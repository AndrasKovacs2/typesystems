LAGDAS := \
  src/NatBoolAST.lagda \
  src/NatBoolWT.lagda \
  src/NatBool.lagda \
  src/Int.lagda \
  src/DefABT.lagda \
  src/DefWT.lagda \
  src/Def.lagda \
  src/STT.lagda \
  src/Fin.lagda 
# \
#  src/Fun/Algebra.lagda \
#  src/Fun/Standard.lagda \
#  src/Fun/Completeness.lagda \
#  src/Fun/Stability.lagda \
#  src/Fin/Algebra.lagda \
#  src/Fin/Standard.lagda \
#  src/Fin/LogicalPredicate.lagda \
#  src/Fin/Examples.lagda \
#  src/SystemT/Algebra.lagda \
#  src/SystemT/Standard.lagda \
#  src/SystemT/Examples.lagda \
#  src/SystemT/LogicalPredicate.lagda \
#  src/Tree/Algebra.lagda \
#  src/Tree/Standard.lagda \
#  src/Tree/Examples.lagda \
#  src/Tree/LogicalPredicate.lagda \
#  src/Fixpoints.lagda \
#  src/Lib.lagda \
#  src/Nat/Algebra.lagda \
#  src/Nat/Addition.lagda \
#  src/AlgebraicStructures.lagda \
#  src/SKI/Algebra.lagda \
#  src/ULC/Algebra.lagda

TEXS := $(addsuffix .tex,$(basename $(LAGDAS)))

all: main.pdf

%.tex: %.lagda
	@echo "Checking lagda files.."
	agda --latex $< --latex-dir=src

main.pdf : main.tex ${TEXS}
	echo ${TEXS} | sed 's/ /\n/g' | sed 's/.*/\\input\{\0\}/' >inputs.tex
	@echo "Compiling tex..."
	xelatex main
	bibtex main
	xelatex main
	xelatex main

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.nav *.snm *.toc *~ *.ptb *.idx *.pdf
	rm -f inputs.tex src/*.tex src/*/*.tex src/agda.sty
