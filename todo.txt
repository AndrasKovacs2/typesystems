 * egységes nevek obj/metaelméletnek, 0,1,+,*,ℕ,recℕ,Bool,→,Maybe,...
 * a régi jegyzetet végigolvasni, és minden definíciót, példát átírni az újba
 * változó neves szintaxis és CwF szintaxis sokszor. talán az előadáson a szabályokat is változónevekkel leírni
 * két probléma: nem lehet egyenlőségre matchelni (általában; bár ez javítható lenne pl. Σ helyett ∃ használatával) és nem lehet szintaxisra matchelni
 * I csak postulalva legyen egybe, ne komponensenkent
 * variable hasznalata
 * app helyett $ legyen a szintaxisban
 * hamarabb bevezetni sCwF-et, hogy szokjak
 * Agda links in pdf
 * Makefile depency-k azok egy Agda fajlban legyenek, ami szinten dependency; ha megváltoztatsz valamit Lib.lagda-ban, akkor NatBoolAST.lagda-t is újrabuildelje
 * Fix: fixpont kombinator, es utana Nat, first order function space strictness nelkul (Mark), utana opcionalisan https://www.cs.nott.ac.uk/~psztxa/g53pop/
 * Stream: Nat gluing streamekre mi van? (Ambrus)
 * altalanos induktiv tipusok ("A Syntax for Mutual Inductive Families" cikk lebutitasa one-sorted-ra, nem fuggore es csak rekurzor kell) (Balint+Mark)
 * ⊥η, +η-t hozzaadni Sum-hoz, standard modell, kanonicitas
 * van-e lambda alatt redukcio canonicity bizonyitasban?
 * beirni, hogy sty-t altalaban nem irjuk ki
 * kotott valtozo neve nem szamit, de csak akkor ha ugyanugy hasznaljuk. pl. λx.x+x ≠ λy.y+3
 * logika: think CwF. cartesian cat/Heyting algebra: Con=Ty,Sub=Tm
 * watch https://vimeo.com/428161108
 * (x+0)≠x

 * (f = g)   ((x : A) → f x = g x)
           → trivi
           ← axiomatizalni kell
           ↔

 * A ↔ B, A,B:Prop, A ≅ B

 * speciális algebrák; operátorok diszjunktak, injektívek

   szintaxisban: ∙ ≠ Γ▷A
   Con := Set
   Ty,Tm,Sub := 𝟙
   ∙ := 𝟙
   Γ▷A := 𝟘

   mutass olyan algebrát, melyben ∙=Γ▷A!

   true≠false hogy lehet bizonyitani? uj algebra nelkul

   true = false → ⟦true⟧=⟦false⟧ → 0 = 1

   mutass olyan algebrát, melyben true=false!

   ∙ ≠ Γ▷A
   true = v0[ε,true,false] = v1[ε,true,false] = false

   isZero nem injektiv! lassatok be!

   u,v:Tm∙Nat, suc u = suc v → u = v

   ha van pred, akkor kijön: suc u = suc v → pred (suc u) = pred (suc v) → u = v

   pred : Tm Γ Nat → Tm Γ Nat, challenge

   P nevű ℕ-algebra:
   ℕ := I.ℕ ⊎ ⊤
   zero := inj₂ tt
   suc (inj₁ n) := inj₁ (I.suc n)
   suc (inj₂ _) := inj₁ I.zero


   Con := Con
   Ty  := Ty
   Sub := Sub
   Tm Γ A := Tm Γ A ⊎ ⊤
   zero  := inj₂ tt
   suc (inj₂ _) := inj₁ zero
   suc (inj₁ t) := inj₁ (suc t)

   ⟦_⟧ : I.Tm Γ I.Nat → I.Tm Γ I.Nat ⊎ ⊤

   admissible, derivable

   pred : I.Tm Γ I.Nat → I.Tm Γ I.Nat ⊎ ⊤


   lehetne még ilyen szabályunk:

   Boolη : (t : Tm (Γ ▹ Bool) A) →
     t = if q then (t[id,true]) else (t[id,false])
    ⇒ Boolη' : (t : Tm Γ Bool) → t = if t then true else false

   Con := Con × Con
   Ty  := Ty × Ty
   Tm (Γ,Γ') (A,A') := Tm Γ A × Tm Γ' A'
   Sub (Γ,Γ') (Δ,Δ') := Sub Γ Δ × Sub Γ' Δ'

   Bool := (Bool, Bool)
   true := (true, true)
   false := (false, false)
   ite (b,b') (u,u') (v,v') := (if b then u else v,  if b' then u' else v')

   (true,true), (true,false), (false,true), (false,false)

   nincs kanonicitas

   miben terhet el egy modell a szintaxistol?
   -> meg tobb egyenloseg (ha nincs tobb dolog, kanonicitas igaz)
      => STT az maximalis egyenleseg szempontjabol
   -> tobb dolog, amik pl. nem egyenloek

 * System F:
 
   forall (x :: *) . x -> x


   Ty : Con → Set

    * universes a la Russell
    
      Uᵢ  : Ty (i+1) Γ
      Russell : Tm Γ Uᵢ = Ty i Γ

    * universes a la Tarski

      Uᵢ : Ty (i+1) Γ
      El : Tm Γ Uᵢ → Ty i Γ
      π  : (a : Tm Γ (U i))(b : Tm (Γ ▷ El a) (U j)) → Tm Γ (U (max i j))
      ...

    * universes a lo Coquand

      Uᵢ  : Ty (i+1) Γ
      Russell : Tm Γ Uᵢ ≅ Ty i Γ

      c  : Ty Γ → Tm Γ U
      Uβ : El (c A) = A
      Uη : c (El t) = t

   U[] : U [σ] = U
   Π  : (A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ
   A ⇒ B := Π A (B[p])
   _$_ : Tm Γ (Π A B) → (u : Tm Γ A) → Tm Γ (B[id,u])


   System F:
     U  : Ty Γ
     El : Tm Γ U → Ty Γ
     c  : Tm Γ U ← Ty Γ

     B := {X|X∌X}
     B∈B→B∌B

     B := Σ U (q∈q ⇒ ⊥)
      
               a : Tm Γ U
               El a : Ty Γ
               a : El a <- ertelmetlen

               U : Ty Γ
               ------------
               c U : Tm Γ U

     ∀A.(A→¬A) → (¬A→A) → ⊥


   Π U (El 0 ⇒ El 0)
       ^ : Ty (Γ ▷ U)
       0 : Tm (Γ ▷ U) U
       El 0 : Ty (Γ▷U)

   t : Tm Γ (Π U (El 0 ⇒ El 0))
   t $ (c Nat) : Tm Γ (El 0 ⇒ El 0 [id, cNat])
       : Tm Γ U       (El (c Nat) ⇒ El (c Nat))
                      (Nat ⇒ Nat)


   Ty : ℕ → Set   ℕ := Tycon, ∙, _▷Ty
   Con : ℕ → Set
   Tm : Con n → Ty n → Set

   _⇒_ : Ty n → Ty n → Ty n
   lam : Tm (Γ ▷ A) B → Tm Γ (A ⇒ B)
   _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B

   ∀   : Ty (Ψ,α:*) → Ty Ψ
   λ   : Tm (Ψ,α:*,Γ) A → Tm (Ψ,Γ) (∀α.A)
   _$_ : Tm (Ψ,Γ) (∀α.A) → (B : Ty Ψ) → Tm (Ψ,Γ) (A[α↦B])

   ∀   : Ty (suc n) → Ty n
   lam : Tm {suc n} (Γ[p]) A → Tm {n} Γ (∀ A)
   app : Tm {n} Γ (∀ A) → (B : Ty n) → Tm {n} Γ (A[id,B])


   _[_] : Ty n → Ty n → Ty n
   ∙ : Con n
   _▷_ : (Γ : Con n) → Ty n → Con n
   _[p] : Con n → Con (suc n)
   q  : Ty (suc n)
   _[p] : Ty n → Ty (suc n)
   (α:*,β:*,x:ℕ,y:α→α,z:∀γ.α→β→γ,)
   2, ∙ ▷ Nat ▷ q[p] ⇒ q[p] ▷ ∀ q[p][p] ⇒ q[p] ⇒ q

   ∀α.α→ ∀β.β→α   <- System F
   ∀αβ.α→β→α      <- ez is eleg
