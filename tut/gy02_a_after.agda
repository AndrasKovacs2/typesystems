{-# OPTIONS --prop #-}
module gy02_a_after where


data 𝟚 : Set where
  O I : 𝟚

data ℕ : Set where
  O : ℕ
  S : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

data Id (A : Set)(a : A) : A → Prop where
  refl : Id A a a


-- Nat equality check

-- isEq : ℕ → ℕ → 𝟚
-- isEq a b = {!!}

-- ≟ \?=
-- ℕ \bN
-- 𝟚 \b2
-- → \to
-- Write the equality check using the ≟ operator.

_≟_ : ℕ → ℕ → 𝟚
O ≟ O = I
O ≟ S b = O
S a ≟ O = O
S a ≟ S b = a ≟ b

-- Nat operations

_+_ : ℕ → ℕ → ℕ
O + b = b
S a + b = S (a + b)

_*_ : ℕ → ℕ → ℕ
O * b = O
S a * b = b + (a * b) -- (1 + a) * b = b + (a * b)

_^_ : ℕ → ℕ → ℕ
a ^ O = 1
a ^ S b = a * (a ^ b) -- a ^ (1 + b) = a * (a ^ b)


-- Proofs

idl-plus : (a : ℕ) → Id ℕ (0 + a) a
idl-plus a = refl

cong-ℕ : (f : ℕ → ℕ) → (x y : ℕ) →
       Id ℕ x y → Id ℕ (f x) (f y)
cong-ℕ f x .x refl = refl

-- cong : (A B : Set) → (f : A → B) → (x y : A) →
--        Id A x y → Id B (f x) (f y)
-- cong A B f x y eq = ?

idr-plus : (a : ℕ) → Id ℕ (a + 0) a
idr-plus O = refl
idr-plus (S a) = cong-ℕ S (a + 0) a (idr-plus a) -- (S a + 0) = S (a + 0)
