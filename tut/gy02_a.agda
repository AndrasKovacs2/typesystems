{-# OPTIONS --prop #-}
module gy02_a where


data 𝟚 : Set where
  O I : 𝟚

data ℕ : Set where
  O : ℕ
  S : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

data Id (A : Set)(a : A) : A → Prop where
  refl : Id A a a


-- Nat equality check

isEq : ℕ → ℕ → 𝟚
isEq a b = ?

-- ≟ \?=
-- ℕ \bN
-- 𝟚 \b2
-- → \to
-- Write the equality check using the ≟ operator.



-- Nat operations

_+_ : ℕ → ℕ → ℕ
a + b = ?

_*_ : ℕ → ℕ → ℕ
a * b = ?

_^_ : ℕ → ℕ → ℕ
a ^ b = ?


-- Proofs

idl-plus : (a : ℕ) → Id ℕ (0 + a) a
idl-plus = ?

cong-ℕ : (f : ℕ → ℕ) → (x y : ℕ) →
       Id ℕ x y → Id ℕ (f x) (f y)
cong-ℕ f x y eq = ?

-- cong : (A B : Set) → (f : A → B) → (x y : A) →
--        Id A x y → Id B (f x) (f y)
-- cong A B f x y eq = ?

idr-plus : (a : ℕ) → Id ℕ (a + 0) a
idr-plus = ?
