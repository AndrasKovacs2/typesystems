-- {-# OPTIONS --prop --rewriting #-}
{-# OPTIONS --prop #-}
module gy04_after where

open import Lib

import NatBoolAST as AST
import NatBoolWT as WT
import NatBool as NB

module WT' where
  open WT
  open WT.I

  -- Normalization in the well typed standard algebra

  p : Tm Bool
  p =
    isZero (
      ite
        (isZero (suc zero))
        (zero + zero)
        (ite true zero (suc (suc zero)))
      )

  q : Tm Bool
  q = ite (isZero (zero + suc zero)) false (isZero (suc zero))

  r : Tm Nat
  r = (zero + (suc zero)) + (zero + (suc (suc zero) + zero))

  s : Tm Nat
  s = (zero + zero) + ((zero + zero) + zero)

  canBeNormalized : ∀ {ty : Ty} → Tm ty → Set
  canBeNormalized {ty = Nat} tm =
    ↑p (St.⟦ tm ⟧t ≡ O) ⊎ Σ ℕ (λ n → ↑p (St.⟦ tm ⟧t ≡ S n))
  canBeNormalized {ty = Bool} tm =
    ↑p (St.⟦ tm ⟧t ≡ O) ⊎ ↑p (St.⟦ tm ⟧t ≡ I)

  np : canBeNormalized p
  np = ι₂ ↑[ refl ]↑ -- \Gi

  nq : canBeNormalized q
  nq = ι₁ ↑[ refl ]↑

  nr : canBeNormalized r
  nr = ι₂ (2 , ↑[ refl ]↑)

  ns : canBeNormalized s
  ns = ι₁ ↑[ refl ]↑

  -- nn : canBeNormalized {!!}
  -- nn = {!!}

module NB' where

  open NB.I

  -- Many previously different terms are now equal

  eq-1 : isZero (suc zero) ≡ false
  eq-1 = isZeroβ₂

  eq-2 : ite true (isZero zero) false ≡ true
  eq-2 = iteβ₁ ◾ isZeroβ₁ -- \sqb -> ->

  eq-2' : ite true (isZero zero) false ≡ true
  eq-2' =
    ite true (isZero zero) false
      ≡⟨ iteβ₁ ⟩ -- \== \< \>
    isZero zero
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎ -- \qed

  eq-3 : ite (isZero zero) (suc zero + suc zero) zero
         ≡
         suc (suc zero)
  eq-3 =
    ite (isZero zero) (suc zero + suc zero) zero
      ≡⟨ cong-3 ite isZeroβ₁ refl refl ⟩
    ite true (suc zero + suc zero) zero
      ≡⟨ iteβ₁ ⟩
    suc zero + suc zero
      ≡⟨ +β₂ ⟩
    suc (zero + suc zero)
      ≡⟨ cong suc +β₁ ⟩
    suc (suc zero)
      ∎

  eq-4 : (ite false (suc zero + suc zero) zero + zero) ≡ zero
  eq-4 =
    (ite false (suc zero + suc zero) zero) + zero
      ≡⟨ cong-2 _+_ iteβ₂ refl ⟩
      -- alternative solution using lambda function:
      -- ≡⟨ cong (λ x → x + zero) iteβ₂ ⟩
      -- alternative solution using partially applied operator:
      -- ≡⟨ cong (_+ zero) iteβ₂ ⟩
    (zero + zero)
      ≡⟨ +β₁ ⟩
    zero
      ∎

