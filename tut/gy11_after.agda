{-# OPTIONS --prop --rewriting #-}
module gy11_after where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

open import Fun.Algebra
open I

St : Algebra
St = record
       { Con      = Set
       ; STy      = Set
       ; Ty       = Set
       ; Sub      = λ Γ Δ → Γ → Δ
       ; Tm       = λ Γ A → Γ → A
       ; ∙        = ↑p 𝟙
       ; _▹_      = _×_
       ; Nat      = ℕ
       ; Bool     = 𝟚
       ; sty      = idf
       ; _⇒_      = λ A B → A → B
       ; _∘_      = _∘f_
       ; id       = idf
       ; ε        = const *↑
       ; _,_      = λ σ t γ → σ γ ,Σ t γ
       ; p        = π₁
       ; q        = π₂
       ; _[_]     = _∘f_
       ; lam      = λ t γ x → t (γ ,Σ x)
       ; app      = λ t γx → t (π₁ γx) (π₂ γx)
       ; zero     = const O
       ; suc      = S ∘f_
       ; isZero   = λ t γ → 0 ≟ℕ t γ
       ; _+_      = λ m n γ → m γ +ℕ n γ
       ; true     = const I
       ; false    = const O
       ; ite      = λ b u v γ → if b γ then u γ else v γ
       ; ass      = refl
       ; idl      = refl
       ; idr      = refl
       ; ∙η       = refl
       ; ▹β₁      = refl
       ; ▹β₂      = refl
       ; ▹η       = refl
       ; [id]     = refl
       ; [∘]      = refl
       ; ⇒β       = refl
       ; ⇒η       = refl
       ; lam[]    = refl
       ; zero[]   = refl
       ; suc[]    = refl
       ; isZero[] = refl
       ; +[]      = refl
       ; true[]   = refl
       ; false[]  = refl
       ; ite[]    = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁      = refl
       ; +β₂      = refl
       ; iteβ₁    = refl
       ; iteβ₂    = refl
       }
open Algebra St using (⟦_⟧ST ; ⟦_⟧T ; ⟦_⟧t)

norm : {A : Ty} → Tm ∙ A → ⟦ A ⟧T
norm t = ⟦ t ⟧t *↑

⌜_⌝ : {A : STy} → ⟦ A ⟧ST → Tm ∙ (sty A)
⌜_⌝ {Nat} O = zero
⌜_⌝ {Nat} (S n) = suc ⌜ n ⌝
⌜_⌝ {Bool} O = false
⌜_⌝ {Bool} I = true

-- lemmas

isZero-⌜⌝ : {n : ℕ} → ⌜ 0 ≟ℕ n ⌝ ≡ isZero ⌜ n ⌝
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

+-⌜⌝ : {m n : ℕ} → ⌜ m +ℕ n ⌝ ≡ ⌜ m ⌝ + ⌜ n ⌝
+-⌜⌝ {O} = refl
+-⌜⌝ {S m} = ap suc (+-⌜⌝ {m})

-- ite-⌜⌝ : ∀ {A b t f} → ⌜ if b then t else f ⌝ ≡ ite {A = sty A} ⌜ b ⌝ ⌜ t ⌝ ⌜ f ⌝
-- ite-⌜⌝ {b = O} = refl
-- ite-⌜⌝ {b = I} = refl

P-ite : {A : Ty} → {P : Tm ∙ A → Set} →
           {b : Tm ∙ (sty Bool)} → {t f : Tm ∙ A} →
           ⌜ norm b ⌝ ≡ b → P t → P f →
           P (ite b t f)
P-ite {A} {P} {b} {t} {f} eq Pt Pf with (norm b)
... | O = transport (λ a → P (ite a t f)) eq Pf
... | I = transport (λ a → P (ite a t f)) eq Pt

Comp : DepAlgebra
Comp = record
         { Con      = {!!}
         ; STy      = {!!}
         ; Ty       = {!!}
         ; Sub      = {!!}
         ; Tm       = {!!}
         ; ∙        = {!!}
         ; _▹_      = {!!}
         ; Nat      = {!!}
         ; Bool     = {!!}
         ; sty      = {!!}
         ; _⇒_      = {!!}
         ; _∘_      = {!!}
         ; id       = {!!}
         ; ε        = {!!}
         ; _,_      = {!!}
         ; p        = {!!}
         ; q        = {!!}
         ; _[_]     = {!!}
         ; lam      = {!!}
         ; app      = {!!}
         ; zero     = {!!}
         ; suc      = {!!}
         ; isZero   = {!!}
         ; _+_      = {!!}
         ; true     = {!!}
         ; false    = {!!}
         ; ite      = {!!}
         ; ass      = refl
         ; idl      = refl
         ; idr      = refl
         ; ∙η       = refl
         ; ▹β₁      = refl
         ; ▹β₂      = refl
         ; ▹η       = refl
         ; [id]     = refl
         ; [∘]      = refl
         ; ⇒β       = refl
         ; ⇒η       = refl
         ; lam[]    = refl
         ; zero[]   = refl
         ; suc[]    = refl
         ; isZero[] = refl
         ; +[]      = refl
         ; true[]   = refl
         ; false[]  = refl
         ; ite[]    = refl
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁      = refl
         ; +β₂      = refl
         ; iteβ₁    = refl
         ; iteβ₂    = refl
         }
module Comp = DepAlgebra Comp

completeness : {A : STy} → {t : Tm ∙ (sty A)} → ⌜ norm t ⌝ ≡ t
completeness {A} {t} = {!!}

stability : {A : STy} → {t : ⟦ sty A ⟧T} → norm {sty A} ⌜ t ⌝ ≡ t
stability {Nat} {O} = refl
stability {Nat} {S t} = cong S (stability {Nat})
stability {Bool} {O} = refl
stability {Bool} {I} = refl
