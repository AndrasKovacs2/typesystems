\chapter{An expression language}\label{ch:natbool}

\begin{tcolorbox}[title=Learning goals of this chapter]
  \begin{itemize}
  \item Levels of abstraction when defining a programming language: strings, sequences of lexical elements, abstract syntax trees, well typed syntax, well-typed syntax with equations
  \item Programs as trees: algebras, syntax, defining functions by recursion, dependent algebras, proving properties by induction
  \item Well-typed syntax, type inference, standard algebra and the metacircular interpreter
  \item Well-typed syntax with equations, normalisation, completeness and stability of normalisation
  \end{itemize}
\end{tcolorbox}

In this chapter we study a very simple programming language called
NatBool which contains e.g.\ the following program.
\begin{verbatim}
if isZero (zero + suc zero) then false else isZero zero
\end{verbatim}
Every program is either a numeric or a boolean expression. Numbers can
be formed using \verb$zero$ and \verb$suc$ (successor), e.g.\ $3$ is
given by \verb$suc (suc (suc zero))$. We have a usual if-then-else
operator, addition and an isZero operator which says whether a number
is zero. The above program evaluates in the following steps.
\begin{verbatim}
if isZero (suc zero) then false else isZero zero
if false then false else isZero zero
isZero zero
true
\end{verbatim}

We will describe this language in several iterations. In each
iteration we will make a more precise description of the language:
strings, sequences of lexical elements, abstract syntax trees,
well-typed syntax and algebraic syntax. See Figures \ref{fig:levels_gen}, \ref{fig:levels}.

\begin{figure}
\begin{center}
\begin{tikzpicture}
\node            (lab1) at (0,10.3) {\textbf{level}};
\node[left,red]  (lab2) at (-5.6,9.8) {exclude\vphantom{g}};
\node[gray]      (lab3) at (-1.2,9.8) {abstracting};
\node[gray]      (lab4) at (1.4,9.8) {concretising};
\node[left]      (lab5) at (-3,9.8) {quotient by};
\node (str) at (0,0) {\textbf{(1) string}};
\node (lex) at (0,1.5) {\textbf{(2) sequence of lexical elements}};
\node (ast) at (0,3) {\textbf{(3) abstract syntax tree}};
\node (abt) at (0,4.5)  {\textbf{abstract binding tree}};
\node (wt)  at (0,6) {\textbf{(4) well typed syntax}};
\node (alg) at (0,7.5) {\textbf{(5) well typed syntax with equations}};
\node (hoas) at (0,9) {\textbf{higher order abstract syntax}};
\path[->] (str.north) edge [bend left] node[gray, left, pos=0.7] {lexical analysis} (lex.south);
\path[->] (lex.north) edge [bend left] node[gray, left, pos=0.7] {parsing} (ast.south);
\path[->] (ast.north) edge [bend left] node[gray, left, pos=0.7] {scope checking} (abt.south);
\path[->] (abt.north) edge [bend left] node[gray, left, pos=0.7] {type checking} (wt.south);
\path[->] (wt.north)  edge [bend left] node[gray, left, pos=0.7] {} (alg.south);
\path[->] (alg.north)  edge [bend left] node[gray, left, pos=0.7] {internalisation} (hoas.south);
\node (lex1) at (-5.6,0.2) {};
\node (ast1) at (-5.6,1.7) {};
\node (abt1) at (-5.6,3.2) {};
\node (wt1)  at (-5.6,4.7) {};
\node (alg1) at (-5.6,6.2) {};
\node (hoas1) at (-5.6,7.7) {};
\path[->] (str.north) edge [bend right=15] node[red, left, pos=1] {invalid lexical elements} (lex1.west);
\path[->] (lex.north) edge [bend right=15] node[red, left, pos=1] {wrong number of parameters/brackets} (ast1.west);
\path[->] (ast.north) edge [bend right=15] node[red, left, pos=1] {variable not in scope} (abt1.west);
\path[->] (abt.north) edge [bend right=15] node[red, left, pos=1] {non-matching types} (wt1.west);
\path[->] (alg.north) edge [bend right=15] node[red, left, pos=1] {operations not respecting substitution} (hoas1.west);
\draw[->] (lex) edge [bend left] node[gray, right] {add spaces} (str);
\draw[->] (ast) edge [bend left] node[gray, right] {add brackets} (lex);
\draw[->] (abt) edge [bend left] node[gray, right] {pick variable names} (ast);
\draw[->] (wt)  edge [bend left] node[gray, right] {} (abt);
\draw[->] (alg) edge [bend left] node[gray, right] {convert to normal form} (wt);
\draw[->] (hoas) edge [bend left] node[gray, right] {externalisation} (alg);
\node[left] (qstr) at (-3,0.94) {removal of spaces};
\node[left] (qlex) at (-3,2.49) {removal of extra brackets};
\node[left] (qast) at (-3,3.94) {renaming of bound variables};
\node[left] (qabt) at (-3,5.59) {};
\node[left] (qwt)  at (-3,6.94) {operational equivalence};
\end{tikzpicture}
\end{center}
\caption{Different levels of abstraction when defining a programming
language and transformations between levels. The NatBool expression
language only illustrates levels (1)--(5) out of the seven levels, see Figure
\ref{fig:levels} for examples. Abstract binding trees are sometimes called
well-scoped syntax trees, see Chapter \ref{ch:def}.}
\label{fig:levels_gen} \end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=7cm]{levels.png}
\end{center}
\caption{Example programs at different levels of abstractions in the
NatBool expression language. Each bubble represents a separate program.
At higher levels certain programs are excluded and others identified.
The levels: (1) string, (2) sequence of lexical elements, (3) abstract
syntax tree, (4) well-typed syntax tree, (5) well-typed syntax with
equations.}
\label{fig:levels}
\end{figure}

\section{Strings}

As a first approximation, we say that a program is a string, that is,
a sequence of (say ASCII) characters. We could be even cruder and
following Gödel say that a program is a natural number (strings can be encoded
as natural numbers), but we would like to avoid encodings and use descriptions
which are as direct as possible.

Many strings do not correspond to meaningful programs in our language
such as \verb$suc zero - zero$ as we don't have subtraction. Also,
there are different strings which represent the same program. For
example, \verb$isZero (suc zero)$ and \verb$isZero   (suc zero)$: the extra
spaces after \verb$isZero$ shouldn't matter.

Instead of describing which strings are meaningful programs and
defining an equivelence relation for identifying strings that
represent the same program, we will describe programs using more
abstract structures.

\section{Sequence of lexical elements}

We describe NatBool by the following lexical elements.
\[
\verb$($, \verb$)$, \verb$true$, \verb$false$, \verb$if$, \verb$then$,
\verb$else$, \verb$zero$, \verb$suc$, \verb$isZero$, \verb$+$
\]
A program is an arbitrary sequence of these.

Our example program is the following sequence.

[\verb$if$, \verb$isZero$, \verb$($, \verb$zero$, \verb$+$,
\verb$suc$, \verb$zero$, \verb$)$, \verb$then$, \verb$false$,
\verb$else$, \verb$isZero$, \verb$($, \verb$zero$, \verb$)$]

Now we have much fewer programs and our previous counterexamples are
not programs anymore. Any two programs given as strings which differ
only in the number of spaces will end up as the same program at this
level: \verb$isZero (suc zero)$ and \verb$isZero   (suc zero)$ are both
given by the sequence [\verb$isZero$, \verb$($, \verb$suc$, \verb$zero$, \verb$)$].

However, we still have meaningless programs, e.g. [\verb$($,
\verb$true$] (parentheses have to be balanced) or [\verb$suc$,
\verb$zero$, \verb$+$] (\verb$+$ needs two arguments), and so
on. Also, there are programs which could be identified, e.g.\
[\verb$($, \verb$true$, \verb$)$] and [\verb$true$] (the redundant
parentheses don't matter).

Again, to solve these issues, we move to a higher level representation
of programs.

Note that we can always obtain a string from a sequence of lexical
elements by simply printing the sequence. In the other direction, we
can implement a lexical analyser (lexer) which turns a string into a
sequence of lexical elements or returns an error.

\begin{exe}
  Write a lexical analyser for NatBool!
\end{exe}

\section{Abstract syntax tree}

We define our language by the following BNF grammar:
\begin{verbatim}
  T ::= true | false | if T then T else T | zero | suc T | isZero T | T + T
\end{verbatim}
In Haskell we would write this as follows.
\begin{verbatim}
  data Tm = True | False | Ite Tm Tm Tm | Zero | Suc Tm | IsZero Tm | Tm :+: Tm
\end{verbatim}
We will use a different notation for the same definition where we
write the arity of each operator. We call elements of \verb$Tm$
terms. We write \verb$ite$ instead of if-then-else.
\begin{code}[hide]
{-# OPTIONS --prop #-}
module NatBoolAST where

open import Lib
module I where
\end{code}
\begin{code}
  data Tm   : Set where
    true    : Tm
    false   : Tm
    ite     : Tm → Tm → Tm → Tm
    zero    : Tm
    suc     : Tm → Tm
    isZero  : Tm → Tm
    _+_     : Tm → Tm → Tm
\end{code}
In contrast with the sequence of lexical elements approach, in this
description we added the information that if-then-else expects three,
suc and isZero expect one and addition expects two
arguments. Programs are now trees which have \verb$true$, \verb$false$
or \verb$zero$ at the leaves and they can have ternary branching with
\verb$ite$ at the branching node, unary branching with \verb$suc$ or
\verb$isZero$ at the node or binary branching with \verb$+$ at the
node.

Our example program is depicted as follows.

\begin{tikzpicture}
  \node (x10) at (0,0) {\verb$ite$};
  \node (x20) at (-1,-1) {\verb$isZero$};
  \node (x21) at (0,-1) {\verb$false$};
  \node (x22) at (1,-1) {\verb$isZero$};
  \node (x30) at (-1,-2) {\verb$+$};
  \node (x31) at (1,-2) {\verb$zero$};
  \node (x40) at (-1.5,-3) {\verb$zero$};
  \node (x41) at (-0.5,-3) {\verb$suc$};
  \node (x50) at (-0.5,-4) {\verb$zero$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x10) edge node {} (x22);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x22) edge node {} (x31);
  \draw[-] (x30) edge node {} (x40);
  \draw[-] (x30) edge node {} (x41);
  \draw[-] (x41) edge node {} (x50);
\end{tikzpicture}

Instead of drawing trees we usually use linear notation to save space:
\begin{code}
ex = ite (isZero (zero + suc zero)) false (isZero zero)
\end{code}
\begin{code}[hide]
  where open I
\end{code}
The previously meaningless programs \verb$(true$ and \verb$suc zero +$
do not correspond to any tree, and the programs \verb$(true)$ and
\verb$true$ correspond to the same tree.

Note that \verb$(zero + zero) + zero$ and
\verb$zero + (zero + zero)$ are different trees.

\begin{tikzpicture}
  \node (x10) at (0,0) {\verb$+$};
  \node (x20) at (-0.5,-1) {\verb$+$};
  \node (x21) at (0.5,-1) {\verb$zero$};
  \node (x30) at (-1,-2) {\verb$zero$};
  \node (x31) at (0,-2) {\verb$zero$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x20) edge node {} (x31);
  \node (y10) at (3,0) {\verb$+$};
  \node (y20) at (2.5,-1) {\verb$zero$};
  \node (y21) at (3.5,-1) {$+$};
  \node (y30) at (3,-2) {\verb$zero$};
  \node (y31) at (4,-2) {\verb$zero$};
  \draw[-] (y10) edge node {} (y20);
  \draw[-] (y10) edge node {} (y21);
  \draw[-] (y21) edge node {} (y30);
  \draw[-] (y21) edge node {} (y31);
\end{tikzpicture}

Even if their intuitive meaning is the same, as trees, they are
different. An example of parentheses which even changes the intuitive
meaning of an expression is $1+(2*3) ≠ (1+2)*3$.

\begin{exe}
  Draw the trees depicting the following programs:
\begin{verbatim}
(true + true) + true
((true + true) + true) + true
(true + (true + true)) + true
true + ((true + true) + true)
ite (true + true) ((true + true) + true) (true + (true + true))
\end{verbatim}
\end{exe}

\begin{exe}
  Write down the following tree with linear notation: \\
  \begin{tikzpicture}
  \node (x10) at (0,0) {\verb$ite$};
  \node (x20) at (-2,-1) {\verb$+$};
  \node (x21) at (0,-1) {\verb$zero$};
  \node (x22) at (2,-1) {\verb$ite$};
  \node (x30) at (-2.5,-2) {\verb$zero$};
  \node (x31) at (-1.5,-2) {\verb$isZero$};
  \node (x32) at (1,-2) {\verb$+$};
  \node (x33) at (2,-2) {\verb$suc$};
  \node (x34) at (3,-2) {\verb$true$};
  \node (x40) at (-1.5,-3) {\verb$suc$};
  \node (x41) at (0.5,-3) {\verb$true$};
  \node (x42) at (1.25,-3) {\verb$false$};
  \node (x43) at (2,-3) {\verb$suc$};
  \node (x50) at (-1.5,-4) {\verb$suc$};
  \node (x51) at (2,-4) {\verb$suc$};
  \node (x60) at (-1.5,-5) {\verb$zero$};
  \node (x61) at (2,-5) {\verb$zero$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x10) edge node {} (x22);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x20) edge node {} (x31);
  \draw[-] (x22) edge node {} (x32);
  \draw[-] (x22) edge node {} (x33);
  \draw[-] (x22) edge node {} (x34);
  \draw[-] (x31) edge node {} (x40);
  \draw[-] (x32) edge node {} (x41);
  \draw[-] (x32) edge node {} (x42);
  \draw[-] (x33) edge node {} (x43);
  \draw[-] (x40) edge node {} (x50);
  \draw[-] (x43) edge node {} (x51);
  \draw[-] (x50) edge node {} (x60);
  \draw[-] (x51) edge node {} (x61);
\end{tikzpicture}
\end{exe}

\begin{exe}
  Write down the following BNF notations in Agda using \verb$data$ (as we did for \verb$Tm$).
\begin{verbatim}
  T ::= op0 | op1 T | op2 T T | op3 T T T | op4 T T T T
  
  A ::= a | fb B
  B ::= fa A

  V ::= vzero | vsuc V
  E ::= zero | suc E | E < E | E = E | var V
  C ::= V := E | while E S | if E then S else S
  S ::= empty | C colon S 
\end{verbatim}
\end{exe}

\begin{exe}
  Write a parser for NatBool: a program which given a sequence of
  lexical elements, outputs an element of $\mathsf{Tm}$ or an error.
\end{exe}

\begin{exe}
  Which of the following strings correspond to elements of
  \verb$Tm$?
\begin{verbatim}
if true then true else zero
if true then zero else zero
if zero then zero else zero
if if then zero else zero
true + false
true + suc zero
true + suc
suc + zero
\end{verbatim}
\end{exe}

\begin{exe}
  After parsing them into trees, which of the following strings becomes
  equal to \verb$zero + (suc zero + suc zero)$?
\begin{verbatim}
zero + ((suc zero) + (suc zero))
(zero + ((suc zero) + (suc zero)))
(zero + suc zero) + suc zero
((zero + suc zero) + suc zero)
suc zero + suc zero
suc (suc zero)
(zero + zero) + (suc zero + suc zero)
ite true (zero + (suc zero + suc zero)) zero
ite true (zero + (suc zero + suc zero)) (zero + (suc zero + suc zero))
ite true (zero + (suc zero + suc zero)) false
\end{verbatim}
\end{exe}

\subsection{Recursion}

We call a set \verb$A$ with three elements, two endofunctions (\verb$A → A$
functions), one binary operation on \verb$A$ (\verb$A → A → A$ function) and a
ternary operation on \verb$A$ (\verb$A → A → A → A$ function) a
\emph{NatBoolAST algebra} (sometimes called a \emph{model} of NatBoolAST). We fix a notation for
this. A NatBoolAST algebra (or simply algebra) is a record with the following fields.
\begin{code}
record Algebra {i} : Set (lsuc i) where
  field
    Tm      : Set i
    true    : Tm
    false   : Tm
    ite     : Tm → Tm → Tm → Tm
    zero    : Tm
    suc     : Tm → Tm
    isZero  : Tm → Tm
    _+_     : Tm → Tm → Tm
\end{code}
The previously mentioned trees are called the \emph{syntax} of NatBool. The syntax is
also an algebra, we denote it by \verb$I$, its components are \verb$I.Tm$,
\verb$I.true$, \verb$I.false$, and so on. The syntax has the property that it can
be interpreted into any other algebra (it is an initial algebra, hence the name \verb$I$):
given an algebra \verb$M$, there is a homomorphism (a function that commutes with all operators)
called the recursor (other names: fold, catamorphism, nondependent eliminator)
from \verb$I$ to \verb$M$ which we denote \verb$M.⟦_⟧$:
\begin{code}
  ⟦_⟧ : I.Tm → Tm
  ⟦ I.true          ⟧ = true
  ⟦ I.false         ⟧ = false
  ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
  ⟦ I.zero          ⟧ = zero
  ⟦ I.suc t         ⟧ = suc ⟦ t ⟧
  ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
  ⟦ t I.+ t'        ⟧ = ⟦ t ⟧ + ⟦ t' ⟧
\end{code}
The syntax is also called the free algebra: \verb$I.true$, \verb$I.false$,
\verb$I.zero$ are syntactic terms and then we can freely apply operators on these
and then on those terms produced by the operators, and so on, and these constitute all terms.

For example, the NatBoolAST algebra \verb$H$ (height) is the following.
\begin{code}
H : Algebra
H = record
  { Tm      = ℕ
  ; true    = 0
  ; false   = 0
  ; ite     = λ n n' n'' → 1 +ℕ max n (max n' n'')
  ; zero    = 0
  ; suc     = λ n → 1 +ℕ n
  ; isZero  = λ n → 1 +ℕ n
  ; _+_     = λ n n' → 1 +ℕ max n n'
  }
\end{code}
Note that we distinguish two addition operations:
\verb$_+_$ is the addition operator in the algebra \verb$H$, while \verb$+ℕ$ is
just addition of natural numbers. We call the latter metatheoretic addition. The
\emph{meta theory} (meta language) is
the language that we use to speak about the \emph{object theory}
(object language) which is NatBool in our case.

If our example program \verb$ite (isZero (zero + suc zero)) false (isZero zero)$
is given in the algebra \verb$H$, we obtain the height of the tree. 
\begin{code}[hide]
module H = Algebra H
evalH : H.ite (H.isZero (H.zero H.+ H.suc H.zero)) (H.false) (H.isZero H.zero) ≡ 4
evalH =
\end{code}
\begin{code}
  H.ite     (H.isZero  (H.zero  H.+  H.suc  H.zero)) (H.false) (H.isZero H.zero)          ≡≡
  1 +ℕ max  (H.isZero  (H.zero  H.+  H.suc  H.zero))  (max H.false  (H.isZero H.zero))    ≡≡
  1 +ℕ max  (H.isZero  (0       H.+  H.suc  0))       (max 0        (H.isZero 0))         ≡≡
  1 +ℕ max  (H.isZero  (0       H.+  (1 +ℕ  0)))      (max 0        (1 +ℕ 0))             ≡≡
  1 +ℕ max  (H.isZero  (0       H.+  1))              (max 0        1)                    ≡≡
  1 +ℕ max  (H.isZero  (1 +ℕ max 0 1))                1                                   ≡≡
  1 +ℕ max  (H.isZero  (1 +ℕ 1))                      1                                   ≡≡
  1 +ℕ max  (H.isZero  2)                             1                                   ≡≡
  1 +ℕ max  (1 +ℕ 2)                                  1                                   ≡≡
  1 +ℕ max  3                                         1                                   ≡≡
  1 +ℕ 3                                                                                  ≡≡
  4                                                                                       ∎
\end{code}

Another example algebra $T$ calculates the number of \verb$true$s
in a term.
\begin{code}
T : Algebra
T = record
  { Tm      = ℕ
  ; true    = 1
  ; false   = 0
  ; ite     = λ n n' n'' → n +ℕ n' +ℕ n''
  ; zero    = 0
  ; suc     = λ n → n
  ; isZero  = λ n → n
  ; _+_     = λ n n' → n +ℕ n'
  }
\end{code}
Now
\begin{code}[hide]
module T = Algebra T
evalT : T.ite (T.isZero (T.zero T.+ T.suc T.zero)) (T.false) (T.isZero T.zero) ≡ 0
evalT = 
\end{code}
\begin{code}
  T.ite (T.isZero (T.zero T.+ T.suc T.zero)) (T.false) (T.isZero T.zero) ≡≡ (0 +ℕ 0) +ℕ 0 +ℕ 0 ≡≡ 0
\end{code}
\begin{code}[hide]
  ∎
\end{code}
and e.g.
\begin{code}[hide]
evalT' : T.ite T.true T.zero T.true ≡ 2
evalT' = 
\end{code}
\begin{code}
  T.ite T.true T.zero T.true ≡≡ 1 +ℕ 0 +ℕ 1 ≡≡ 2
\end{code}
\begin{code}[hide]
  ∎
\end{code}

\begin{exe}\label{exe:natbool-size}
  Define an algebra which calculates the number of nodes in a tree, e.g.\
  \verb$zero = 1$, \verb$suc zero = 2$,
  \verb$suc zero + zero = 4$.
\end{exe}

In particular we have that
\begin{code}[hide]
evalM : ∀{i}{M : Algebra {i}} → let module M = Algebra M in
\end{code}
\begin{code}
  M.⟦  I.ite  (I.isZero  (I.zero  I.+  I.suc  I.zero))  (I.false)  (I.isZero  I.zero) ⟧ ≡
       M.ite  (M.isZero  (M.zero  M.+  M.suc  M.zero))  (M.false)  (M.isZero  M.zero)
\end{code}
\begin{code}[hide]
evalM = refl
\end{code}
for any algebra \verb$M$, and similarly for any other syntactic term.

In functional languages like Haskell or Agda, using the recursor on an
algebra is a special case of a recursive definition using pattern
matching. For example, \verb$H.⟦_⟧$ corresponds to a \verb$height$
function defined by pattern matching:
\begin{AgdaAlign}
\begin{code}
height : I.Tm → ℕ
height I.true            = 0
height I.false           = 0
height (I.ite t t' t'')  = 1 +ℕ max (height t) (max (height t') (height t''))
height I.zero            = 0
height (I.suc t)         = 1 +ℕ height t
height (I.isZero t)      = 1 +ℕ height t
height (t I.+ t')        = 1 +ℕ max (height t) (height t')
\end{code}

\AgdaNoSpaceAroundCode{}
\begin{verbatim}
H.⟦_⟧ : I.Tm → ℕ
\end{verbatim}
\begin{code}[hide]
evalHtrue : 
\end{code}
\begin{code}
  H.⟦ I.true ⟧           ≡ 0
\end{code}
\begin{code}[hide]
evalHfalse : 
\end{code}
\begin{code}
  H.⟦ I.false ⟧          ≡ 0
\end{code}
\begin{code}[hide]
evalHite : ∀{t t' t''} →
\end{code}
\begin{code}
  H.⟦ I.ite t t' t'' ⟧   ≡ 1 +ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)
\end{code}
\begin{code}[hide]
evalHzero : 
\end{code}
\begin{code}
  H.⟦ I.zero ⟧           ≡ 0
\end{code}
\begin{code}[hide]
evalHsuc : ∀{t} →
\end{code}
\begin{code}
  H.⟦ I.suc t ⟧          ≡ 1 +ℕ H.⟦ t ⟧
\end{code}
\begin{code}[hide]
evalHisZero : ∀{t} →
\end{code}
\begin{code}
  H.⟦ I.isZero t ⟧       ≡ 1 +ℕ H.⟦ t ⟧
\end{code}
\begin{code}[hide]
evalH+ : ∀{t t'} →
\end{code}
\begin{code}
  H.⟦ t I.+ t' ⟧         ≡ 1 +ℕ max H.⟦ t ⟧ H.⟦ t' ⟧
\end{code}
\begin{code}[hide]
evalHtrue = refl
evalHfalse = refl
evalHite = refl
evalHzero = refl
evalHsuc = refl
evalHisZero = refl
evalH+ = refl
\end{code}
\end{AgdaAlign}
\AgdaSpaceAroundCode{}

Not all Haskell functions by pattern matching are expressible using an
algebra and a recursor. Haskell allows nonterminating recursion or
recursion where some cases are not handled, see the following
examples.
\begin{verbatim}
f : I.Tm → ℕ
f t = f t

g : I.Tm → ℕ
g I.true = 3
\end{verbatim}
In Agda, all functions defined by pattern matching can also be expressed
using the recursor (or induction, see below), for details see
\cite{conorthesis,andreasfoetus}.

The syntactic operators are \emph{disjoint}: for example,
\verb$I.true$ is not equal to \verb$I.false$. We can show this using
an algebra where terms are metatheoretic booleans (\verb$𝟚$ is the
two-element set), \verb$true$ is metatheoretic true, \verb$false$ is
metatheoretic false. The other components don't matter, here we just
set them to constant false.
\begin{code}
TF : Algebra
TF = record
  { Tm      = 𝟚
  ; true    = I
  ; false   = O
  ; ite     = λ _ _ _ → O
  ; zero    = O
  ; suc     = λ _ → O
  ; isZero  = λ _ → O
  ; _+_     = λ _ _ → O
  }
module TF = Algebra TF
\end{code}
Now we can use the recursor and \verb$congruence$ of equality to
obtain a contradiction from \verb$I.true ≡ I.false$:
\begin{code}
I=O : I.true ≡ I.false → I ≡ O
I=O e = congruency TF.⟦_⟧ e

I≠O : ¬ (I ≡ O)
I≠O ()

true≠false : ¬ (I.true ≡ I.false)
true≠false e = I≠O (I=O e)
\end{code}

\begin{exe}
  Prove that \verb$I.isZero$ is not equal \verb$I.suc$!
\end{exe}

However there are algebras where \verb$true = false$, e.g.\ the
trivial algebra where terms are given by the set with one element
\verb$*$ and all operators return this element.
\begin{code}
Triv : Algebra
Triv = record
  { Tm      = ↑p 𝟙
  ; true    = _
  ; false   = _
  ; ite     = _
  ; zero    = _
  ; suc     = _
  ; isZero  = _
  ; _+_     = _
  }
\end{code}

\begin{exe}
  Create an algebra where \verb$true = false$, but \verb$true ≠ zero$.
\end{exe}

Note that the height of every syntactic term (element of \verb$I.Tm$)
is finite, e.g.\ there is no tree \verb$I.suc (I.suc (I.suc ⋯))$ (an
infinitely deep tree with \verb$I.suc$ at each node) because it would
be mapped to an infinite natural number by \verb$H.⟦_⟧$ which does not
exist. This is not true for arbitrary algebras: there can be algebras
where \verb$Tm$ has infinitely large elements, for example \verb$Tm = ℕ → 𝟚$
(a term is an infinite sequence of booleans).

% \begin{exe}
%   Define a NatBoolAST algebra where terms are \verb$ℕ → 𝟚$ sequences,
%   \verb$true$ is constant \verb$I$, \verb$false$ is constant \verb$O$ and
%   (following C) every sequence which is not constant \verb$O$ is also interpreted
%   as \verb$true$. A number is the number of \verb$I$s in a sequence. Addition
%   creates a sequence where the number of \verb$I$s is the sum of
%   the number of \verb$I$s in the two numbers.
% \end{exe}

\begin{exe}
Define a NatBoolAST algebra where terms are natural numbers, booleans
are interpreted in the C style: false is $0$, true is $1$ and for
if-then-else, anything that is not $0$ is interpreted as true. We can call
this algebra the degenerate standard algebra.
\end{exe}

\begin{exe}
  Is there an algebra where \verb$Tm = ↑p 𝟘$?
\end{exe}

\begin{exe}
Show that for any two algebras \verb$M$ and \verb$N$ there is an algebra
where \verb$Tm = M.Tm × N.Tm$.
\end{exe}

\begin{exe}
  Create an algebra where \verb$Tm = 𝟚$, \verb$zero = I$ and all other operators are
  $O$. With the help of this, create a function \verb$isZero : I.Tm → 𝟚$. Then
  create an optimisation \verb$I.Tm → I.Tm$ which maps \verb$I.zero + t$ to
  \verb$t$ and \verb$t + I.zero$ to \verb$t$.
\end{exe}

\begin{exe}
  Consider a subset of NatBool, the programming language Nat: 
  \begin{verbatim}
    Tm : Set
    zero : Tm
    suc : Tm → Tm
    _+_ : Tm → Tm → Tm
  \end{verbatim} Define an algebra \verb$St$ such that \verb$St.⟦_⟧ : I.Tm → ℕ$ is an
  interpreter for this language (where \verb$I$ is the syntax for this language).
  E.g.\ \verb$St.⟦suc zero + suc (suc (suc zero))⟧ = 4$.
\end{exe}

\subsection{Induction}

In addition to recursion, the syntax supports induction which is a
generalisation (dependent variant) of recursion. Induction can be
stated by saying that for any \emph{dependent algebra} \verb$D$ there
is a \emph{dependent homomorphism} from \verb$I$ to \verb$D$. A
\emph{dependent algebra} (dependent model, displayed algebra/model) is
given by the following components.
\begin{code}
record DepAlgebra {i} : Set (lsuc i) where
  field
    Tm      : I.Tm → Set i
    true    : Tm I.true
    false   : Tm I.false
    ite     : ∀{t t' t''} → Tm t → Tm t' → Tm t'' → Tm (I.ite t t' t'')
    zero    : Tm I.zero
    suc     : ∀{t} → Tm t → Tm (I.suc t)
    isZero  : ∀{t} → Tm t → Tm (I.isZero t)
    _+_     : ∀{t t'} → Tm t → Tm t' → Tm (t I.+ t')
\end{code}
In a dependent algebra, instead of a set of terms, there is a family
of sets indexed over \verb$I.Tm$, that is, there is a set \verb$Tm t$
for each element \verb$t : I.Tm$ (See Figure \ref{fig:depalg}). Then we have elements of these
families at each syntactic operator as index. So, we have \verb$Tm I.true$,
\verb$Tm I.false$, $\dots$, \verb$Tm (t I.+ t')$. For those
operators with parameters, we also have induction hypotheses, for
example we have \verb$Tm t$ and \verb$Tm t'$ as inputs for \verb$_+_$.

\begin{figure}
\begin{center}
\begin{tikzpicture}
\draw (0,1) circle (0.9cm);
\draw (2,1) circle (0.9cm);
\draw (4,1) circle (0.9cm);
\draw (6,1) circle (0.9cm);
\node (d1) at (0,2.25) {\verb$D.Tm t$};
\node (d2) at (2,2.25) {\verb$D.Tm t'$};
\node (d3) at (4,2.25) {\verb$D.Tm (t I.+ t')$};
\node (d4) at (6,2.25) {\verb$D.Tm I.zero$};
\draw[rounded corners=12pt] (-1,-1) rectangle ++(10,0.9);
\node (i) at (-1.5,-0.5) {\verb$I.Tm$};
\node (i1) at (0,-0.5) {\verb$t$};
\node (i2) at (2,-0.5) {\verb$t'$};
\node (i3) at (4,-0.5) {\verb$(t I.+ t')$};
\node (i4) at (6,-0.5) {\verb$I.zero$};
\node (i4) at (8,-0.5) {$\dots$};
\node (d5) at (0,1) {\verb$u$};
\node (d6) at (2,1) {\verb$u'$};
\node (d7) at (4,1) {\verb$(u D.+ u')$};
\node (d8) at (6,1) {\verb$D.zero$};
\node (d8) at (8,1) {$\dots$};
\end{tikzpicture}
\end{center}
\cprotect\caption{Depiction of a dependent algebra \verb$D$.
There is a separate set \verb$D.Tm t$ for every
syntactic term \verb$t$.}
\label{fig:depalg}
\end{figure}

The dependent homomorphism from the syntax is called \emph{induction}
(induction principle, eliminator). This is a dependent function
which for an input \verb$t : I.TmI$ outputs a \verb$Tm t$. Moreover, as in
the non-dependent case, it maps each syntactic operator to its
counterpart in the dependent algebra.
\begin{code}
  ⟦_⟧ : (t : I.Tm) → Tm t
  ⟦ I.true          ⟧ = true
  ⟦ I.false         ⟧ = false
  ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
  ⟦ I.zero          ⟧ = zero
  ⟦ I.suc t         ⟧ = suc ⟦ t ⟧
  ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
  ⟦ t I.+ t'        ⟧ = ⟦ t ⟧ + ⟦ t' ⟧
\end{code}

A special case of the dependent algebra is when \verb$Tm t = ↑p (P t)$
for some \verb$P : I.Tm → Prop$, that is, \verb$Tm$ is a predicate (unary
relation) on syntactic terms. In this case the other components say
that each syntactic operator respects the predicate. For example, if
the predicate holds for \verb$t$ and \verb$t'$, then it also holds for
\verb$t I.+ t'$. Cf.\ natural number algebras and induction on natural
numbers. All inductively defined sets come with a notion
of algebra, recursion, dependent algebra and induction. Other well-known
examples are lists, binary trees, see Chapter TODO.

An example is the dependent algebra expressing that the number of
\verb$true$s in a syntactic term is less than or equal to \verb$3$ to
the power the height of the tree:
\begin{code}[hide]
import Lemmas

D-1 : {t : I.Tm} →
  ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧) →
  ↑p (T.⟦ I.suc t ⟧ ≤ 3 ^ℕ H.⟦ I.suc t ⟧)
D-1 le = ↑[ Lemmas.≤-+ℕ-r ↓[ le ]↓ ]↑

D-2 : {t t' : I.Tm} →
  ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧) →
  ↑p (T.⟦ t' ⟧ ≤ 3 ^ℕ H.⟦ t' ⟧) →
  ↑p (T.⟦ t I.+ t' ⟧ ≤ 3 ^ℕ H.⟦ t I.+ t' ⟧)
D-2 {t} {t'} le1 le2 = ↑[
      (Lemmas.+ℕ-≤-mono ↓[ le1 ]↓ ↓[ le2 ]↓)
    Lemmas.≤-◾
      Lemmas.≤-+ℕ-l {c = 3 ^ℕ max H.⟦ t ⟧ H.⟦ t' ⟧}
        (Lemmas.+ℕ-≤-mono
          (Lemmas.^ℕ-≤-mono-r {b = H.⟦ t ⟧} (s≤s z≤n) Lemmas.≤-max-l)
          (Lemmas.≤-+ℕ-r
            (Lemmas.^ℕ-≤-mono-r
              {b = H.⟦ t' ⟧} {b' = max H.⟦ t ⟧ H.⟦ t' ⟧}
              (s≤s z≤n) Lemmas.≤-max-r
            )
          )
        )
  ]↑

D-3 : {t t' t'' : I.Tm} →
  ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧) →
  ↑p (T.⟦ t' ⟧ ≤ 3 ^ℕ H.⟦ t' ⟧) →
  ↑p (T.⟦ t'' ⟧ ≤ 3 ^ℕ H.⟦ t'' ⟧) →
  ↑p (T.⟦ I.ite t t' t'' ⟧ ≤ 3 ^ℕ H.⟦ I.ite t t' t'' ⟧)
D-3 {t} {t'} {t''} le1 le2 le3 = ↑[
      Lemmas.+ℕ-≤-mono (Lemmas.+ℕ-≤-mono ↓[ le1 ]↓ ↓[ le2 ]↓) ↓[ le3 ]↓
    Lemmas.≤-◾
      Lemmas.+ℕ-≤-mono
        {a = 3 ^ℕ H.⟦ t ⟧ +ℕ 3 ^ℕ H.⟦ t' ⟧}
        {b = 3 ^ℕ H.⟦ t'' ⟧}
        {b' = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
        (Lemmas.+ℕ-≤-mono
          {a = 3 ^ℕ H.⟦ t ⟧}
          {a' = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
          (Lemmas.^ℕ-≤-mono-r
            {b = H.⟦ t ⟧}
            {b' = max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
            (s≤s z≤n) (Lemmas.≤-max-3a {b = H.⟦ t' ⟧})
          )
          (Lemmas.≤-+ℕ-r
            {b = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
            (Lemmas.^ℕ-≤-mono-r {b = H.⟦ t' ⟧} (s≤s z≤n) (Lemmas.≤-max-3b {a = H.⟦ t ⟧}))
          )
        )
        (Lemmas.^ℕ-≤-mono-r
          {b = H.⟦ t'' ⟧}
          -- {b' = max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)}
          (s≤s z≤n) (Lemmas.≤-max-3c {a = H.⟦ t ⟧})
        )
    Lemmas.≤-◾
      Lemmas.≤-≡ (Lemmas.+ℕ-comm {b = 3 ^ℕ max H.⟦ t ⟧ (max H.⟦ t' ⟧ H.⟦ t'' ⟧)})
  ]↑
\end{code}
\begin{code}
D : DepAlgebra
D = record
  { Tm      = λ t → ↑p (T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧)
  ; true    = ↑[ s≤s z≤n ]↑
  ; false   = ↑[ z≤n ]↑
  ; ite     = λ {t}{t'}{t''} → D-3 {t}{t'}{t''}
  ; zero    = ↑[ z≤n ]↑
  ; suc     = λ {t} → D-1 {t}
  ; isZero  = λ {t} → D-1 {t}
  ; _+_     = λ {t}{t'} → D-2 {t}{t'}
  }
\end{code}
\verb$D.Tm t$ has an inhabitant exactly when \verb$T.⟦t⟧$ is less than or
equal to \verb$3^{H.⟦t⟧}$.

TODO: rewrite this into Agda code:
\begin{alignat*}{10}
  & \mathsf{true}_D && :\,\, && ⟦\mathsf{true}_\I⟧^T = 1 ≤ 1 = 3^0 = 3^{⟦\mathsf{true}_\I⟧^H} \\
  & \mathsf{false}_D && : && ⟦\mathsf{false}_\I⟧^T = 0 ≤ 1 = 3^0 = 3^{⟦\mathsf{false}_\I⟧^H} \\
  & \mathsf{if}_D\ t_D\ \mathsf{then}\ t_D'\ \mathsf{else}\ t_D'' && : && ⟦\mathsf{if}_\I\ t\ \mathsf{then}\ t'\ \mathsf{else}\ t''⟧^T = \\
  & && && ⟦t⟧^T+ ⟦t'⟧^T+ ⟦t''⟧^T \overset{t_D, t_D', t_D''}{≤} \\
  & && && 3^{⟦t⟧^H}+3^{⟦t'⟧^H}+3^{⟦t''⟧^H} ≤ \\
  & && && 3^{\mathsf{max}\ \{⟦t⟧^H, ⟦t'⟧^H,⟦t''⟧^H\}}+3^{\mathsf{max}\ \{⟦t⟧^H, ⟦t'⟧^H,⟦t''⟧^H\}}+3^{\mathsf{max}\ \{⟦t⟧^H, ⟦t'⟧^H,⟦t''⟧^H\}} = \\
  & && && 3^{1+\mathsf{max}\ \{⟦t⟧^H, ⟦t'⟧^H,⟦t''⟧^H\}} = \\
  & && && 3^{⟦\mathsf{if}_\I\ t\ \mathsf{then}\ t'\ \mathsf{else}\ t''⟧^H} \\
  & \mathsf{zero}_D && : && ⟦\mathsf{false}_\I⟧^T = 0 ≤ 1 = 3^0 = 3^{⟦\mathsf{zero}_\I⟧^H} \\
  & \mathsf{suc}_D\ t_D && : && ⟦\mathsf{zero}_\I\ t⟧^T = ⟦t⟧^T \overset{t_D}{≤} 3^{⟦t⟧^H} ≤ 3^{1+⟦t⟧^H} = 3^{⟦\mathsf{suc}_\I\ t⟧^H} \\
  & \mathsf{isZero}_D\ t_D && : && ⟦\mathsf{isZero}_\I\ t⟧^T = ⟦t⟧^T \overset{t_D}{≤} 3^{⟦t⟧^H} ≤ 3^{1+⟦t⟧^H} = 3^{⟦\mathsf{isZero}_\I\ t⟧^H} \\
  & t_D +_D t_D' && : && ⟦t+_\I t'⟧^T = \\
  & && && ⟦t⟧^T+⟦t'⟧^T \overset{t_D,t_D'}{≤} \\
  & && && 3^{⟦t⟧^H}+3^{⟦t'⟧^H} ≤ \\
  & && && 3^{\mathsf{max}\ \{⟦t⟧^H,⟦t'⟧^H\}}+3^{\mathsf{max}\ \{⟦t⟧^H,⟦t'⟧^H\}} \\
  & && && ≤ 3^{1+\mathsf{max}\ \{⟦t⟧^H,⟦t'⟧^H\}} = \\
  & && && 3^{⟦t+_\I t'⟧^H}
\end{alignat*}

Applying the induction principle on the dependent algebra $D$, we
obtain a function
\begin{verbatim}
  D.⟦_⟧ : (t : I.Tm) → T.⟦ t ⟧ ≤ 3 ^ℕ H.⟦ t ⟧
\end{verbatim}

\begin{exe}
  Show that every algebra can be turned into a dependent algebra, and
  derive the recursor using the induction principle.
\end{exe}

\begin{exe}
  Prove that \verb$H.⟦ t ⟧ ≤ S.⟦ t ⟧$ where \verb$S$ is the algebra defined in
  exercise \ref{exe:natbool-size}.
\end{exe}

\begin{exe}
  Prove that \verb$I.suc$ is injective: if \verb$I.suc t ≡ I.suc t'$, then \verb$t ≡ t'$.
  Define an algebra \verb$M$ where \verb$M.suc$ is not injective.
\end{exe}

\begin{exe}
Define an algebra which counts the number of \verb$true$s and \verb$false$s in a term.
Define an algebra which counts the number of \verb$true$s in a term. Show
that interpreting a syntactic term in the first one always gives a greater
or equal number than interpreting a term in the second one.
\end{exe}
