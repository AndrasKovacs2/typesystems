\begin{code}[hide]
{-# OPTIONS --prop #-}

open import Lib hiding (¬_)
\end{code}
\begin{code}
¬_ : ∀ {i} (A : Set i) → Prop i
¬ A = A → 𝟘

_$_ : ∀ {i j} {A : Set i}{B : Set j} → (A → B) → A → B
f $ x = f x

!_ : 𝟚 → 𝟚
! O = I
! I = O

!≠id : ∀ {b} → ¬ ↑p (! b ≡ b)
!≠id {O} ()
!≠id {I} ()

card : ¬ Σ (ℕ → ℕ → 𝟚) λ φ → (f : ℕ → 𝟚) → Σ ℕ λ k → ↑p (f ≡ φ k)
card t = 
  let φ = π₁ t
      f n = ! φ n n
      u = π₂ t f
      k = π₁ u
  in  !≠id ↑[ ap (_$ k) ↓[ π₂ u ]↓ ]↑
\end{code}
