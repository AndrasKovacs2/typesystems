\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module SystemT.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import SystemT.Algebra

open I
\end{code}
\begin{code}
St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; _⇒_ = λ A B → A → B
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ,Σ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; zero = λ _ → O
       ; suc = λ n γ → S (n γ)
       ; rec = λ u v n γ → indℕ _ (u γ) (λ a → v (γ ,Σ a)) (n γ)
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; Natβ₁ = refl
       ; Natβ₂ = refl
       ; lam[] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; rec[] = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑

⌜_⌝ : ℕ → Tm ∙ Nat
⌜ O ⌝ = zero
⌜ S n ⌝ = suc ⌜ n ⌝
\end{code}
