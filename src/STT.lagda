\chapter{Function space}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Rules for function space. Constructor and destructor operators, computation and uniqueness rules.
\end{tcolorbox}

TODO: bidirectional type checking (for this, we need an ABT version of STT).

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module STT where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

module I where
  data Ty : Set where
    Nat : Ty
    Bool : Ty
    _⇒_ : Ty → Ty → Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    true : ∀ {Γ} → Tm Γ Bool
    false : ∀ {Γ} → Tm Γ Bool
    ite : ∀ {Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[] : ∀ {Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    iteβ₁ : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v

    zero : ∀ {Γ} → Tm Γ Nat
    suc : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat
    isZero : ∀ {Γ} → Tm Γ Nat → Tm Γ Bool
    _+_ : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁ : ∀ {Γ} → isZero (zero {Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ} {n : Tm Γ Nat} → isZero (suc n) ≡ false
    +β₁ : ∀ {Γ} {n : Tm Γ Nat} → zero + n ≡ n
    +β₂ : ∀ {Γ} {m n : Tm Γ Nat} → (suc m) + n ≡ suc (m + n)
    zero[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[] : ∀ {Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    +[] : ∀ {Γ Δ} {m n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (m + n) [ σ ] ≡ (m [ σ ]) + (n [ σ ])


    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE iteβ₁ iteβ₂ true[] false[] ite[] #-}
  {-# REWRITE isZeroβ₁ isZeroβ₂ +β₁ +β₂ zero[] suc[] isZero[] +[] #-}
  {-# REWRITE ⇒β ⇒η lam[] app[] #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con       : Set i
    Sub       : Con → Con → Set k
    Ty        : Set j
    Tm        : Con → Ty → Set l

    _∘_       : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id        : ∀{Γ} → Sub Γ Γ
    ass       : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl       : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr       : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]      : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]      : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]       : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    _▹_       : Con → Ty → Con
    _,_       : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η        : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    Bool      : Ty
    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁     : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂     : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]   : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]     : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
                
    Nat       : Ty
    zero      : ∀{Γ} → Tm Γ Nat
    suc       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+_       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁  : ∀{Γ} → isZero (zero {Γ}) ≡ true
    isZeroβ₂  : ∀{Γ} {n : Tm Γ Nat} → isZero (suc n) ≡ false
    +β₁       : ∀{Γ} {n : Tm Γ Nat} → zero + n ≡ n
    +β₂       : ∀{Γ} {m n : Tm Γ Nat} → (suc m) + n ≡ suc (m + n)

    zero[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]     : ∀{Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
                (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[]  : ∀{Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
                (isZero n) [ σ ] ≡ isZero (n [ σ ])
    +[]       : ∀{Γ Δ} {m n : Tm Δ Nat}{σ : Sub Γ Δ} →
                (m + n) [ σ ] ≡ (m [ σ ]) + (n [ σ ])
\end{code}
An STT algebra extends a Def algebra with the following operations:
\begin{code}
    _⇒_       : Ty → Ty → Ty
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]     : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
\end{code}
For every type former, operators can be grouped into the following categories:
\begin{itemize}
\item type introduction: the operator which constructs the given type (\verb$_⇒_$ for function space),
\item constructors (introductory operators): operators which introduce elements of the type (\verb$lam$),
\item destructors (eliminators): operators that can be used to eliminate an element of the type (\verb$app$),
\item computation (\verb$β$) rules: explain what happens if a destructor is applied to a constructor (\verb$⇒β$),
\item uniqueness (\verb$η$) rules: explain what happens if a constructor is applied to a destructor (\verb$⇒η$),
\item substitution rules: explain how instantiation of substitution \verb$_[_]$ interacts with the operators (we only
  have the equation \verb$lam[]$ as the one for \verb$app$ can be proven, see below).
\end{itemize}
\begin{code}[hide]
  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]
\end{code}
We can define the usual binary application operator:
\begin{code}
  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]
\end{code}
\begin{code}[hide]
  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦+⟧ : ∀ {Γ} {m n : I.Tm Γ I.Nat} →
      ⟦ m I.+ n ⟧t ≡ ⟦ m ⟧t + ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}
  
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    Bool : Ty I.Bool
    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
    ite : ∀ {Γ' A' b' u' v'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Bool b' → Tm Γ A u' → Tm Γ A v' → Tm Γ A (I.ite b' u' v')
    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' u' v' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ Bool b'}{u : Tm Δ A u'}{v : Tm Δ A v'}
      {σ : Sub Γ Δ σ'} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    iteβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite false u v ≡ v

    Nat : Ty I.Nat
    zero : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Nat I.zero
    suc : ∀ {Γ' n'} {Γ : Con Γ'} →
      Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    isZero : ∀ {Γ' n'} {Γ : Con Γ'} →
      Tm Γ Nat n' → Tm Γ Bool (I.isZero n')
    _+_ : ∀ {Γ' m' n'} {Γ : Con Γ'} →
      Tm Γ Nat m' → Tm Γ Nat n' → Tm Γ Nat (m' I.+ n')
    isZeroβ₁ : ∀ {Γ'} {Γ : Con Γ'} → isZero (zero {Γ = Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ' n'} {Γ : Con Γ'}{n : Tm Γ Nat n'} →
      isZero (suc n) ≡ false
    +β₁ : ∀ {Γ' n'} {Γ : Con Γ'}{n : Tm Γ Nat n'} → zero + n ≡ n
    +β₂ : ∀ {Γ' m' n'} {Γ : Con Γ'}{m : Tm Γ Nat m'}{n : Tm Γ Nat n'} →
      (suc m) + n ≡ suc (m + n)
    zero[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      zero [ σ ] ≡ zero
    suc[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    +[] : ∀ {Γ' Δ' m' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {m : Tm Δ Nat m'}{n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (m + n) [ σ ] ≡ (m [ σ ]) + (n [ σ ])
    
    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')
    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦+⟧ : ∀ {Γ} {m n : I.Tm Γ I.Nat} →
      ⟦ m I.+ n ⟧t ≡ ⟦ m ⟧t + ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

open I

-- Church encodings
BoolCh : Ty → Ty
BoolCh A = A ⇒ A ⇒ A

NatCh : Ty → Ty
NatCh A = (A ⇒ A) ⇒ A ⇒ A

trueCh : ∀ {Γ A} → Tm Γ (BoolCh A)
trueCh = lam (lam v1)

falseCh : ∀ {Γ A} → Tm Γ (BoolCh A)
falseCh = lam (lam q)

iteCh : ∀ {Γ A} → Tm Γ (BoolCh A ⇒ A ⇒ A ⇒ A)
iteCh = lam q

zeroCh : ∀ {Γ A} → Tm Γ (NatCh A)
zeroCh = lam (lam q)

sucCh : ∀ {Γ A} → Tm Γ (NatCh A ⇒ NatCh A)
sucCh = lam (lam (lam (v1 $ (v2 $ v1 $ q))))

plusCh : ∀ {Γ A} → Tm Γ (NatCh A ⇒ NatCh A ⇒ NatCh A)
plusCh = lam (lam (lam (lam (v3 $ v1 $ (v2 $ v1 $ q)))))
\end{code}
Examples. With variable names, we write \verb$ex1 = λx.isZero x$, \verb$ex3 = λx.true$.
\begin{code}
ex1 : Tm ∙ (Nat ⇒ Bool)
ex1 = lam (isZero v0)

ex2 : ex1 $ suc zero ≡ false
ex2 =
  ex1 $ suc zero
                                                         ≡≡
  lam (isZero v0) $ suc zero
                                                         ≡≡
  app (lam (isZero v0)) [ id , suc zero ]
                                                         ≡≡
  isZero v0 [ id , suc zero ]
                                                         ≡≡
  isZero (suc zero)
                                                         ≡≡
  false
                                                         ∎

ex3 : Tm ∙ (Nat ⇒ Bool)
ex3 = lam true
\end{code}
A constant function is really constant:
\begin{code}
ex4 : ∀{Γ A B}{t : Tm Γ B}{u : Tm Γ A} → lam (t [ p ]) $ u ≡ t
ex4 = refl
\end{code}
More examples:
\begin{code}
ex5 : Tm ∙ (Nat ⇒ (Bool ⇒ Nat))
ex5 = lam (lam (v1 + ite v0 (suc zero) (suc (suc zero))))

ex6 : Tm ∙ ((Bool ⇒ Nat) ⇒ Nat)
ex6 = lam (v0 $ isZero (v0 $ true))
\end{code}

\begin{code}[hide]
isO : ℕ → 𝟚
isO O = I
isO (S n) = O

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; zero = const O
       ; suc = S ∘f_
       ; isZero = isO ∘f_
       ; _+_ = λ m n γ → m γ +ℕ n γ
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; zero[] = refl
       ; suc[] = refl
       ; isZero[] = refl
       ; +[] = refl
       ; true = const I
       ; false = const O
       ; ite = λ b u v γ → if b γ then u γ else v γ
       ; iteβ₁ = refl
       ; iteβ₂ = refl
       ; true[] = refl
       ; false[] = refl
       ; ite[] = refl
\end{code}
The new components in the standard algebra:
\begin{code}
       ; _⇒_ = λ A B → A → B
       ; lam = λ t γ x → t (γ ,Σ x)
       ; app = λ t γx → t (π₁ γx) (π₂ γx)
       ; ⇒β = refl
       ; ⇒η = refl
       ; lam[] = refl
\end{code}       
\begin{code}[hide]
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)
\end{code}
Normalisation function:
\begin{code}
norm : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
norm t = ⟦ t ⟧t *↑
\end{code}
We have quote for types \verb$Nat$ and \verb$Bool$ but not for function space.
\begin{code}
⌜_⌝N : ℕ → Tm ∙ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝B : 𝟚 → Tm ∙ Bool
⌜ O ⌝B = false
⌜ I ⌝B = true
\end{code}
Completeness of normalisation. We don't directly prove completeness as for
Def, because we don't even have a function \verb$⌜_⌝ : St.⟦ A ⟧ → Tm ∙ A$. Instead,
for each type \verb$A$ we have a predicate on \verb$Tm ∙ A$ and these predicates
say completeness in the cases \verb$A = Nat$ and \verb$A = Bool$. For the function
space \verb$A ⇒ B$, the predicate says that if the predicate for type \verb$A$ holds for a term,
then the predicate for type \verb$B$ holds for the output of the function. For terms, we prove
that they preserve predicates.
\begin{code}
isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝B ≡ isZero ⌜ n ⌝N
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

+-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝N ≡ ⌜ m ⌝N + ⌜ n ⌝N
+-⌜⌝ {O} = refl
+-⌜⌝ {S m} = ap suc (+-⌜⌝ {m})

Comp : DepAlgebra
Comp = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; Nat = λ n → ↑p (⌜ norm n ⌝N ≡ n)
         ; zero = λ _ → refl↑
         ; suc = λ n x → ↑[ ap suc ↓[ n x ]↓ ]↑
         ; isZero = λ n x → ↑[ isZero-⌜⌝
                             ◾ ap isZero ↓[ n x ]↓ ]↑
         ; _+_ = λ {_ m'} m n {ν'} x →
                   ↑[ +-⌜⌝ {norm (m' [ ν' ])}
                    ◾ ap2 _+_ ↓[ m x ]↓ ↓[ n x ]↓ ]↑
         ; zero[] = refl
         ; suc[] = refl
         ; isZero[] = refl
         ; +[] = refl
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁ = refl
         ; +β₂ = refl
         ; Bool = λ b → ↑p (⌜ norm b ⌝B ≡ b)
         ; true = λ _ → refl↑
         ; false = λ _ → refl↑
         ; ite = λ {_ _ b' u' v' _ A} b u v {ν'} x →
                   let P t = A (ite t (u' [ ν' ]) (v' [ ν' ]))
                   in  ind𝟚 (λ b → ⌜ b ⌝B ≡ b' [ ν' ] → P (b' [ ν' ]))
                            (λ e → transport P e (v x))
                            (λ e → transport P e (u x))
                            (norm (b' [ ν' ]))
                            ↓[ b x ]↓
         ; iteβ₁ = refl
         ; iteβ₂ = refl
         ; true[] = refl
         ; false[] = refl
         ; ite[] = refl
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; ⇒β = refl
         ; ⇒η = refl
         ; lam[] = refl
         }
module Comp = DepAlgebra Comp
\end{code}
We obtain completeness for the cases \verb$A = Nat$ and \verb$A = Bool$.
\begin{code}
compB : {b : Tm ∙ Bool} → ⌜ norm b ⌝B ≡ b
compB {b} = ↓[ Comp.⟦ b ⟧t {id} *↑ ]↓

compN : {n : Tm ∙ Nat} → ⌜ norm n ⌝N ≡ n
compN {n} = ↓[ Comp.⟦ n ⟧t {id} *↑ ]↓
\end{code}
Stability is proved as for Def.
\begin{code}
stabilityN : ∀ {n} → norm ⌜ n ⌝N ≡ n
stabilityN {O} = refl
stabilityN {S n} = ap S stabilityN

stabilityB : ∀ {b} → norm ⌜ b ⌝B ≡ b
stabilityB {O} = refl
stabilityB {I} = refl
\end{code}
