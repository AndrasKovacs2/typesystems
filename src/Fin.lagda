\chapter{Product and sum types}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Nullary and binary products.
  Nullary and binary sums, enumerations, booleans encoded as sums, option type.
\end{tcolorbox}

TODO: n-ary products, records, curry, uncurry, isomorphisms of finite types.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fin where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _⊗_ ; _,_ to _,Σ_)

module I where
  data Ty : Set where
    Unit : Ty
    Empty : Ty
    _⇒_ : Ty → Ty → Ty
    _×_ : Ty → Ty → Ty
    _+_ : Ty → Ty → Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    Unitη : ∀ {Γ} {t : Tm Γ Unit} → t ≡ tt
    ×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ A B C} {t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inr t) u v ≡ v [ id , t ]
    
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ Δ A B} {t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B} {t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A + B)}
      {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β
                
  tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
  tt[] = Unitη

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η #-}
  {-# REWRITE ×β₁ ×β₂ ×η #-}
  {-# REWRITE +β₁ +β₂ #-}
  {-# REWRITE lam[] app[] #-}
  {-# REWRITE tt[] ⟨,⟩[] proj₁[] proj₂[] #-}
  {-# REWRITE absurd[] inl[] inr[] case[] #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : Set i
    Ty : Set j
    Sub : Con → Con → Set k
    Tm : Con → Ty → Set l

    ∙ : Con
    _▹_ : Con → Ty → Con

    _⇒_ : Ty → Ty → Ty

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    
    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
\end{code}
A Fin algebra is an STT algebra extended with nullary and binary sums
and products.

Rules for the nullary product:
\begin{code}
    Unit     : Ty
    tt       : ∀{Γ} → Tm Γ Unit
    Unitη    : ∀{Γ}{t : Tm Γ Unit} → t ≡ tt
\end{code}
Rules for binary products:
\begin{code}
    _×_      : Ty → Ty → Ty
    ⟨_,_⟩    : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁    : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂    : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ B
    ×β₁      : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂      : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η       : ∀{Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]    : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
               ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩
\end{code}
The nullary product is the unit (top, \verb$()$) type with only one
constructor and no destructor: this shows that there is no information
in a term of type \verb$Unit$, there is no way to use such a
term. This is why in some programing languages this type is called
\verb$void$ -- when a function does not return a value, its return type
is unit.

A term of the binary product type \verb$A × B$ is an ordered pair of a
term of type \verb$A$ and another of type \verb$B$. The constructor is
pairing \verb$⟨_,_⟩$, and there are two destructors: first and second
projections. The computation rules say what happens if we project our
the first or second element of a pair, the uniqueness rule says that
any pair is composed from its two projections. We only need a
substitution rule for the constructor, the substitution rules for the
destructors can be proven, see below.

Rules for the nullary sum:
\begin{code}
    Empty    : Ty
    absurd   : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
               (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
\end{code}
Rules for binary sums:
\begin{code}
    _+_     : Ty → Ty → Ty
    inl     : ∀{Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr     : ∀{Γ A B} → Tm Γ B → Tm Γ (A + B)
    case    : ∀{Γ A B C} → Tm Γ (A + B) →
              Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
    +β₁     : ∀{Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
              case (inl t) u v ≡ u [ id , t ]
    +β₂     : ∀{Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
              case (inr t) u v ≡ v [ id , t ]
    inl[]   : ∀{Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
              (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[]   : ∀{Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} →
              (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[]  : ∀{Γ Δ A B C}{t : Tm Δ (A + B)}
              {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
              (case t u v) [ σ ] ≡
                case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])
\end{code}
\begin{code}[hide]
  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

\end{code}
The following are provable in any algebra:
\begin{code}
  tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
  tt[] = Unitη

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η
\end{code}
\begin{code}[hide]
  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Unit ⟧T = Unit
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦tt⟧ ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦absurd⟧ ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    Unit : Ty I.Unit
    Empty : Ty I.Empty
    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    _×_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.× B')
    _+_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.+ B')

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')

    tt : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Unit I.tt
    ⟨_,_⟩ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A u' → Tm Γ B v' → Tm Γ (A × B) I.⟨ u' , v' ⟩
    proj₁ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ A (I.proj₁ t')
    proj₂ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ B (I.proj₂ t')

    absurd : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Empty t' → Tm Γ A (I.absurd t')
    inl : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A t' → Tm Γ (A + B) (I.inl t')
    inr : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ B t' → Tm Γ (A + B) (I.inr t')
    case : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'} →
      Tm Γ (A + B) t' → Tm (Γ ▹ A) C u' → Tm (Γ ▹ B) C v' →
      Tm Γ C (I.case t' u' v')

    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t

    Unitη : ∀ {Γ' t'} {Γ : Con Γ'} {t : Tm Γ Unit t'} →
      t =[ ap (Tm Γ Unit) I.Unitη ]= tt
    ×β₁ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A × B) t'} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Γ A t'}{u : Tm (Γ ▹ A) C u'}{v : Tm (Γ ▹ B) C v'} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Γ B t'}{u : Tm (Γ ▹ A) C u'}{v : Tm (Γ ▹ B) C v'} →
      case (inr t) u v ≡ v [ id , t ]
    
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    ⟨,⟩[] : ∀ {Γ' Δ' A' B' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {t : Tm Δ Empty t'}{σ : Sub Γ Δ σ'} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Δ A t'}{σ : Sub Γ Δ σ'} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Δ B t'}{σ : Sub Γ Δ σ'} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ' Δ' A' B' C' t' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Δ (A + B) t'}{u : Tm (Δ ▹ A) C u'}{v : Tm (Δ ▹ B) C v'}
      {σ : Sub Γ Δ σ'} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------
  
  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Unit ⟧T = Unit
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦tt⟧ ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦absurd⟧ ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}
\begin{code}[hide]
module Examples where
  open I
\end{code}
Enumerations can be defined using the unit and sum types. For example, the type with three elements:
\begin{code}
  Three : Ty
  Three = Unit + (Unit + Unit)
\end{code}
Its three elements:
\begin{code}
  three0 three1 three2 : ∀{Γ} → Tm Γ Three
  three0 = inl tt
  three1 = inr (inl tt)
  three2 = inr (inr tt)
\end{code}
Elimination principle of \verb$Three$:
\begin{code}
  caseThree : ∀{Γ A} → Tm Γ Three → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
  caseThree t u v w = case t (u [ p ]) (case v0 (v [ p ∘ p ]) (w [ p ∘ p ]))
  
  Threeβ0 : ∀{Γ A}{u v w : Tm Γ A} → caseThree three0 u v w ≡ u
  Threeβ1 : ∀{Γ A}{u v w : Tm Γ A} → caseThree three1 u v w ≡ v
  Threeβ2 : ∀{Γ A}{u v w : Tm Γ A} → caseThree three2 u v w ≡ w
  Threeβ0 = refl
  Threeβ1 = refl
  Threeβ2 = refl
\end{code}
Addition modulo 3:
\begin{code}
  plus3 : Tm ∙ (Three ⇒ Three ⇒ Three)
  plus3 = lam (lam (caseThree v1
    {- v1=0 -} v0
    {- v1=1 -} (caseThree v0 three1 three2 three0)
    {- v1=2 -} (caseThree v0 three2 three0 three1)))

  plus3test00 : plus3 $ three0 $ three0 ≡ three0
  plus3test11 : plus3 $ three1 $ three1 ≡ three2
  plus3test12 : plus3 $ three1 $ three2 ≡ three0
  plus3test00 = refl
  plus3test11 = refl
  plus3test12 = refl

\end{code}
We can also define booleans. The usual eliminator (if-then-else) can
be defined using case.
\begin{code}
  Bool : Ty
  Bool = Unit + Unit

  true : ∀{Γ} → Tm Γ Bool
  true = inl tt

  false : ∀{Γ} → Tm Γ Bool
  false = inr tt

  ite : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  ite b u v = case b (u [ p ]) (v [ p ])
  
  Boolβ₁ : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
  Boolβ₁ = refl
  Boolβ₂ : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v
  Boolβ₂ = refl
\end{code}
With \verb$Bool$ and binary products we can simulate homogeneous binary sums:
\begin{code}
  2*_ : Ty → Ty
  2* A = Bool × A
  inl' inr' : ∀{Γ A} → Tm Γ A → Tm Γ (2* A)
  inl' t = ⟨ true , t ⟩
  inr' t = ⟨ false , t ⟩
  case' : ∀{Γ A C} → Tm Γ (2* A) → Tm (Γ ▹ A) C → Tm (Γ ▹ A) C → Tm Γ C
  case' t u v = ite (proj₁ t) (u [ id , proj₂ t ]) (v [ id , proj₂ t ])
  2*β₁ : ∀{Γ A C}{t : Tm Γ A}{u v : Tm (Γ ▹ A) C} → case' (inl' t) u v ≡ u [ id , t ]
  2*β₁ = refl
  2*β₂ : ∀{Γ A C}{t : Tm Γ A}{u v : Tm (Γ ▹ A) C} → case' (inr' t) u v ≡ v [ id , t ]
  2*β₂ = refl
\end{code}
\begin{exe}
Define the type \verb$Fin : ℕ → Ty$ where \verb$Fin n$ has $n$ elements.
\end{exe}
The option type former (sometimes called maybe) adds an extra element to a type:
\begin{code}
  Opt : Ty → Ty
  Opt A = A + Unit
  none : ∀{Γ A} → Tm Γ (Opt A)
  none = inr tt
  some : ∀{Γ A} → Tm Γ A → Tm Γ (Opt A)
  some t = inl t
\end{code}

\begin{code}[hide]
St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _⊗_
       ; _⇒_ = λ A B → A → B
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ x → t (γ ,Σ x)
       ; app = λ t γx → t (π₁ γx) (π₂ γx)
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; lam[] = refl
\end{code}
The new components in the standard algebra:
\begin{code}
       ; Unit = ↑p 𝟙
       ; tt = const *↑
       ; Unitη = refl
       
       ; _×_ = _⊗_
       ; ⟨_,_⟩ = λ u v γ → u γ ,Σ v γ
       ; proj₁ = π₁ ∘f_
       ; proj₂ = π₂ ∘f_
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; ⟨,⟩[] = refl
       
       ; Empty = ↑p 𝟘
       ; absurd = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; absurd[] = refl
       
       ; _+_ = _⊎_
       ; inl = ι₁ ∘f_
       ; inr = ι₂ ∘f_
       ; case = λ t u v γ →
                  case⊎ (t γ) (λ x → u (γ ,Σ x)) (λ y → v (γ ,Σ y))
       ; +β₁ = refl
       ; +β₂ = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
\end{code}
\begin{code}[hide]
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → I.Tm I.∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑
\end{code}

The standard algebra some equalities not present in the syntax, for example:
\begin{code}
module EqualitiesInSt where
  module St = Algebra St
  open St
\end{code}
\begin{code}
  e1 : Con ≡ Ty
  e2 : ∀{Γ Δ} → Sub Γ Δ ≡ Tm Γ Δ
  e3 : ∀{Γ Δ A} → _≡_ {A = Sub Γ (Δ ▹ A) → Sub Γ Δ} (p ∘_)  proj₁
  e4 : ∀{Γ Δ A} → _≡_ {A = Sub Γ (Δ ▹ A) → Tm  Γ A} (q [_]) proj₂
  e5 : ∀{A} → Tm ∙ (Unit × A) ≡ Sub ∙ (∙ ▹ A)
\end{code}

\begin{code}[hide]
  e1 = refl
  e2 = refl
  e3 = refl
  e4 = refl
  e5 = refl

open I
\end{code}
We prove canonicity using the logical predicate method as for STT.

The predicate that we will use for sums and case:
\begin{code}
data _P+_ {A' B'}(A : Tm ∙ A' → Set)(B : Tm ∙ B' → Set) : Tm ∙ (A' + B') → Set where
  Pinl : ∀ {a'} → A a' → (A P+ B) (inl a')
  Pinr : ∀ {b'} → B b' → (A P+ B) (inr b')

Pcase : ∀{A' B' C'}{t' : Tm ∙ (A' + B')}{u' : Tm (∙ ▹ A') C'}{v' : Tm (∙ ▹ B') C'}
  {A : Tm ∙ A' → Set}{B : Tm ∙ B' → Set}(C : Tm ∙ C' → Set)
  (t : (A P+ B) t')(u : ∀ {a'} → A a' → C (u' [ id , a' ]))
  (v : ∀ {b'} → B b' → C (v' [ id , b' ])) →
  C (case t' u' v')
Pcase {u' = u'} {v'} C (Pinl t) u v  = u t
Pcase {u' = u'} {v'} C (Pinr t) u v = v t
\end{code}
\begin{code}[hide]
Can : DepAlgebra
Can = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; ⇒β = refl
         ; ⇒η = refl
         ; lam[] = refl
\end{code}
Finite product and sum types in the logical predicate dependent algebra:
\begin{code}
         ; Unit      = λ _ → ↑p 𝟙
         ; tt        = λ _ → *↑
         ; Unitη     = refl
         
         ; _×_       = λ A B t' → A (proj₁ t') ⊗ B (proj₂ t')
         ; ⟨_,_⟩     = λ u v x → u x ,Σ v x
         ; proj₁     = λ t x → π₁ (t x)
         ; proj₂     = λ t x → π₂ (t x)
         ; ×β₁       = refl
         ; ×β₂       = refl
         ; ×η        = refl
         ; ⟨,⟩[]     = refl
         
         ; Empty     = λ _ → ↑p 𝟘
         ; absurd    = λ t x → ⟦ ↓[ t x ]↓ ⟧𝟘
         ; absurd[]  = refl
         
         ; _+_       = _P+_
         ; inl       = λ t x → Pinl (t x)
         ; inr       = λ t x → Pinr (t x)
         ; case      = λ { {C = C} t u v {ν'} x → Pcase C (t x) (λ a → u (x ,Σ a)) (λ b → v (x ,Σ b)) }
         ; +β₁       = refl
         ; +β₂       = refl
         ; inl[]     = refl
         ; inr[]     = refl
         ; case[]    = refl
\end{code}
\begin{code}[hide]
         }
module Can = DepAlgebra Can
\end{code}
The canonicity statements for unit and product follow from their \verb$η$ rules:
\begin{code}
canUnit : (t : Tm ∙ Unit) → ↑p (t ≡ tt)
canUnit t = ↑[ Unitη ]↑

can× : ∀{A B}(t : Tm ∙ (A × B)) → Σ (Tm ∙ A) λ u → Σ (Tm ∙ B) λ v → ↑p (t ≡ ⟨ u , v ⟩)
can× t = proj₁ t ,Σ (proj₂ t ,Σ ↑[ refl ]↑)
\end{code}
For the empty type and sums we use interpretation into the canonicity dependent algebra:
\begin{code}
canEmpty : (t : Tm ∙ Empty) → 𝟘
canEmpty t = ↓[ Can.⟦ t ⟧t {id} ↑[ * ]↑ ]↓

can+ : ∀{A B}(t : Tm ∙ (A + B)) →
  Σ (Tm ∙ A) (λ a → ↑p (t ≡ inl a)) ⊎ Σ (Tm ∙ B) (λ b → ↑p (t ≡ inr b))
can+ t with Can.⟦ t ⟧t {id} ↑[ * ]↑
... | Pinl {u'} u = ι₁ (u' ,Σ ↑[ refl ]↑)
... | Pinr {v'} v = ι₂ (v' ,Σ ↑[ refl ]↑)
\end{code}
