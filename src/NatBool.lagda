\section{Well-typed syntax with equations}

In the well-typed syntax, we still had too many terms in a sense: for
example, the terms \verb$I.true$ and
\verb$I.isZero I.zero$ were different, even if they
should be equal in a sensible semantics. The information which terms
should be the same (usually this is called operational semantics) was
missing. Now we add this information in the form of equalities.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module NatBool where

open import Lib

module I where
  data Ty     : Set where
    Nat       : Ty
    Bool      : Ty
  
  postulate
    Tm        : Ty → Set
    
    zero      : Tm Nat
    suc       : Tm Nat → Tm Nat
    isZero    : Tm Nat → Tm Bool
    _+_       : Tm Nat → Tm Nat → Tm Nat
    
    true      : Tm Bool
    false     : Tm Bool
    ite       : ∀ {A} → Tm Bool → Tm A → Tm A → Tm A
    
    isZeroβ₁  : isZero zero ≡ true
    isZeroβ₂  : ∀ {n} → isZero (suc n) ≡ false
    +β₁       : ∀ {n} → zero + n ≡ n
    +β₂       : ∀ {m n} → suc m + n ≡ suc (m + n)

    iteβ₁     : ∀ {A} {u v : Tm A} → ite true u v ≡ u
    iteβ₂     : ∀ {A} {u v : Tm A} → ite false u v ≡ v
    
  {-# REWRITE isZeroβ₁ isZeroβ₂ +β₁ +β₂ iteβ₁ iteβ₂ #-}
\end{code}

A NatBool algebra (without any qualifiers) consists of the following
components. The only difference from NatBoolWT is the addition of the
six equations.
\begin{code}
record Algebra {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty        : Set i
    Tm        : Ty → Set j
    Nat       : Ty
    Bool      : Ty
    zero      : Tm Nat
    suc       : Tm Nat → Tm Nat
    isZero    : Tm Nat → Tm Bool
    _+_       : Tm Nat → Tm Nat → Tm Nat
    true      : Tm Bool
    false     : Tm Bool
    ite       : ∀ {A} → Tm Bool → Tm A → Tm A → Tm A
    isZeroβ₁  :          isZero zero     ≡ true
    isZeroβ₂  : ∀ {n} →  isZero (suc n)  ≡ false
    +β₁       : ∀ {n}    → zero   + n ≡ n
    +β₂       : ∀ {m n}  → suc m  + n ≡ suc (m + n)
    iteβ₁     : ∀ {A} {u v : Tm A} → ite true   u v ≡ u
    iteβ₂     : ∀ {A} {u v : Tm A} → ite false  u v ≡ v
\end{code}
The recursor is given as before.
\begin{code}
  ⟦_⟧T         : I.Ty → Ty
  ⟦ I.Nat ⟧T   = Nat
  ⟦ I.Bool ⟧T  = Bool

  postulate
    ⟦_⟧t       : ∀ {A} → I.Tm A → Tm ⟦ A ⟧T
    ⟦zero⟧     : ⟦ I.zero ⟧t ≡ zero
    ⟦suc⟧      : ∀ {n} → ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧   : ∀ {n} → ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦+⟧        : ∀ {m n} → ⟦ m I.+ n ⟧t ≡ ⟦ m ⟧t + ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦+⟧ #-}
    ⟦true⟧     : ⟦ I.true ⟧t ≡ true
    ⟦false⟧    : ⟦ I.false ⟧t ≡ false
    ⟦ite⟧      : ∀ {A} {b : I.Tm I.Bool}{u v : I.Tm A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}
\end{code}
We also have dependent algebras and an eliminator.
\begin{code}
record DepAlgebra {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty     : I.Ty → Set i
    Tm     : ∀ {A'} → Ty A' → I.Tm A' → Set j
    
    Nat : Ty I.Nat
    Bool : Ty I.Bool

    zero : Tm Nat I.zero
    suc : ∀ {n'} → Tm Nat n' → Tm Nat (I.suc n')
    isZero : ∀ {n'} → Tm Nat n' → Tm Bool (I.isZero n')
    _+_ : ∀ {m' n'} → Tm Nat m' → Tm Nat n' → Tm Nat (m' I.+ n')

    true : Tm Bool I.true
    false : Tm Bool I.false
    ite : ∀ {A' b' u' v'} {A : Ty A'} →
      Tm Bool b' → Tm A u' → Tm A v' → Tm A (I.ite b' u' v')
    
    isZeroβ₁ : isZero zero ≡ true
    isZeroβ₂ : ∀ {n'} {n : Tm Nat n'} → isZero (suc n) ≡ false
    +β₁ : ∀ {n'} {n : Tm Nat n'} → zero + n ≡ n
    +β₂ : ∀ {m' n'} {m : Tm Nat m'}{n : Tm Nat n'} →
      suc m + n ≡ suc (m + n)

    iteβ₁ : ∀ {A' u' v'} {A : Ty A'}{u : Tm A u'}{v : Tm A v'} →
      ite true u v ≡ u
    iteβ₂ : ∀ {A' u' v'} {A : Ty A'}{u : Tm A u'}{v : Tm A v'} →
      ite false u v ≡ v

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool

  postulate
    ⟦_⟧t : ∀ {A} (t : I.Tm A) → Tm ⟦ A ⟧T t
    ⟦zero⟧ :  ⟦ I.zero ⟧t ≡ zero
    ⟦suc⟧ : ∀ {n} → ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {n} → ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦+⟧ : ∀ {m n} → ⟦ m I.+ n ⟧t ≡ ⟦ m ⟧t + ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦true⟧ : ⟦ I.true ⟧t ≡ true
    ⟦false⟧ : ⟦ I.false ⟧t ≡ false
    ⟦ite⟧ : ∀ {A} {b : I.Tm I.Bool}{u v : I.Tm A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}
\end{code}

The type inference algorithm given for NatBoolWT also works for
NatBool. We can't calculate the height of a term anymore as before
because of the equalities. Every function has to preserve the
equalities. Try to define height!

\subsection{Normalisation}

Standard semantics (set algebra, metacircular interpreter).
\begin{code}
open import NatBoolWT using (isO)

St : Algebra
St = record
  { Ty        = Set
  ; Tm        = idf
  ; Nat       = ℕ
  ; Bool      = 𝟚
  ; zero      = O
  ; suc       = S
  ; isZero    = isO
  ; _+_       = _+ℕ_
  ; true      = I
  ; false     = O
  ; ite       = if_then_else_
  ; isZeroβ₁  = refl
  ; isZeroβ₂  = refl
  ; +β₁       = refl
  ; +β₂       = refl
  ; iteβ₁     = refl
  ; iteβ₂     = refl
  }
module St = Algebra St
\end{code}
Note the difference between \verb$_+_$ and \verb$_+ℕ_$, \verb$zero$ and \verb$O$, \verb$suc$ and \verb$S$.
An object theoretic term is mapped to its metatheoretic equivalent by \verb$⟦_⟧t$.

For this algebra, we call \verb$⟦ A ⟧T$ the set of normal forms of type
\verb$A$. For \verb$I.Tm I.Nat$, normal forms are metatheoretic natural numbers, for \verb$I.Tm I.Bool$, normal forms are metatheoretic booleans.  The interpretation of terms in \verb$St$ computes the normal form of a
term:
\begin{code}
norm : ∀{A} → I.Tm A → St.⟦ A ⟧T
norm = St.⟦_⟧t 
\end{code}
Normal forms can be quoted back to terms:
\begin{code}
⌜_⌝N : ℕ → I.Tm I.Nat
⌜ O ⌝N = I.zero
⌜ S n ⌝N = I.suc ⌜ n ⌝N

⌜_⌝B : 𝟚 → I.Tm I.Bool
⌜ O ⌝B = I.false
⌜ I ⌝B = I.true

⌜_⌝ : ∀ {A} → St.⟦ A ⟧T → I.Tm A
⌜_⌝ {I.Nat} = ⌜_⌝N
⌜_⌝ {I.Bool} = ⌜_⌝B
\end{code}

Completeness of normalisation says that every term is equal
to its normal form followed by quote. This means that every boolean term
(element of \verb$I.Tm I.Bool$) is either true or false and every
natural number term is a finite application of \verb$I.suc$s on
\verb$I.zero$.

\begin{code}
isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝ ≡ I.isZero ⌜ n ⌝
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

add-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝ ≡ ⌜ m ⌝ I.+ ⌜ n ⌝
add-⌜⌝ {O} = refl
add-⌜⌝ {S m} = ap I.suc (add-⌜⌝ {m})

ite-⌜⌝ : ∀ {A b} {u v : St.⟦ A ⟧T} →
  ⌜_⌝ {A} (if b then u else v) ≡ I.ite ⌜ b ⌝ ⌜ u ⌝ ⌜ v ⌝
ite-⌜⌝ {b = O} = refl
ite-⌜⌝ {b = I} = refl

Comp : DepAlgebra
Comp = record
         { Ty = const (↑p 𝟙)
         ; Tm = λ _ t → ↑p (⌜ St.⟦ t ⟧t ⌝ ≡ t)
         ; Nat = _
         ; Bool = _
         ; zero = refl↑
         ; suc = λ n → ↑[ ap I.suc ↓[ n ]↓ ]↑
         ; isZero = λ n → ↑[ isZero-⌜⌝ ◾ ap I.isZero ↓[ n ]↓ ]↑
         ; _+_ = λ {m'} m n → ↑[ add-⌜⌝ {St.⟦ m' ⟧t}
                               ◾ ap2 I._+_ ↓[ m ]↓ ↓[ n ]↓ ]↑
         ; true = refl↑
         ; false = refl↑
         ; ite = λ {_ b'} b u v → ↑[ ite-⌜⌝ {b = St.⟦ b' ⟧t}
                                   ◾ ap3 I.ite ↓[ b ]↓ ↓[ u ]↓ ↓[ v ]↓ ]↑
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁ = refl
         ; +β₂ = refl
         ; iteβ₁ = refl
         ; iteβ₂ = refl
         }
module Comp = DepAlgebra Comp

completeness : ∀ {A} {t : I.Tm A} → ⌜ norm t ⌝ ≡ t
completeness {t = t} = ↓[ Comp.⟦ t ⟧t ]↓

completenessB : ∀{t : I.Tm I.Bool} → t ≡ I.true ⊎p t ≡ I.false
completenessB {t} = ind𝟚p
  (λ b → ⌜ b ⌝ ≡ t → t ≡ I.true ⊎p t ≡ I.false)
  (λ e → ι₂ (e ⁻¹))
  (λ e → ι₁ (e ⁻¹))
  (norm t)
  completeness
\end{code}

Stability (no junk in normal forms, every normal form is the image of
normalisation of a term):
\begin{code}
stabilityN : ∀ {n} → St.⟦ ⌜ n ⌝N ⟧t ≡ n
stabilityN {O} = refl
stabilityN {S n} = ap S stabilityN

stabilityB : ∀ {b} → St.⟦ ⌜ b ⌝B ⟧t ≡ b
stabilityB {O} = refl
stabilityB {I} = refl

stability : ∀ {A} {t : St.⟦ A ⟧T} → St.⟦ ⌜_⌝ {A} t ⟧t ≡ t
stability {I.Nat} = stabilityN
stability {I.Bool} = stabilityB
\end{code}

We can define normalisation and prove completeness using a variant of
the standard algebra where \verb$Bool$ has three elements (\verb$Bool = Maybe 𝟚$), but not stability.

\begin{exe}
  Define normalisation and its completeness using a modified standard algebra where \verb$Bool = Maybe 𝟚$ and show that stability does not hold.
\end{exe}

We used all the equations of NatBool in the completeness proof. If we
take out e.g.\ the rule \verb$isZeroβ₂$ from the definition of the
language, we cannot prove compeleteness anymore because we have
elements of \verb$I.Tm I.Bool$ which are neither \verb$I.true$, nor
\verb$I.false$. The following (NatBool without \verb$isZeroβ₂$) algebra distinguishes \verb$true$,
\verb$false$ and \verb$isZero (suc zero)$.
\begin{code}
module NatBoolIncomplete where
  Ty        : Set₁
  Tm        : Ty → Set
  Nat       : Ty
  Bool      : Ty
  zero      : Tm Nat
  suc       : Tm Nat → Tm Nat
  isZero    : Tm Nat → Tm Bool
  _+_       : Tm Nat → Tm Nat → Tm Nat
  true      : Tm Bool
  false     : Tm Bool
  ite       : ∀ {A} → Tm Bool → Tm A → Tm A → Tm A
  isZeroβ₁  :          isZero zero     ≡ true
  +β₁       : ∀ {n}    → zero   + n ≡ n
  +β₂       : ∀ {m n}  → suc m  + n ≡ suc (m + n)
  iteβ₁     : ∀ {A} {u v : Tm A} → ite true   u v ≡ u
  iteβ₂     : ∀ {A} {u v : Tm A} → ite false  u v ≡ v

  Ty               = Set
  Tm A             = A
  Nat              = ℕ
  Bool             = Maybe 𝟚
  zero             = O
  suc              = S
  isZero O         = Just I
  isZero (S t)     = Nothing
  O + v            = v
  S u + v          = S (u + v)
  true             = Just I
  false            = Just O
  ite Nothing u v  = u
  ite (Just O) u v = v
  ite (Just I) u v = u
  isZeroβ₁         = refl
  +β₁              = refl
  +β₂              = refl
  iteβ₁            = refl
  iteβ₂            = refl

  true≠false : ¬ (true ≡ false)
  true≠false ()
  true≠isZero1 : ¬ (true ≡ isZero (suc zero))
  true≠isZero1 ()
  false≠isZero1 : ¬ (false ≡ isZero (suc zero))
  false≠isZero1 ()
\end{code}


\begin{exe}
  Prove that the normal forms for the initial monoid with an injection
  from a set \verb$A$ is lists of \verb$A$.
\end{exe}

\subsection{Transition relation}

The traditional way of defining equality of syntactic terms is using
transition relations on syntactic NatBoolAST terms.
\begin{code}
import NatBoolAST
module I' = NatBoolAST.I
data _↦_ : I'.Tm → I'.Tm → Prop where
  isZeroβ₁  :             I'.isZero I'.zero ↦ I'.true
  isZeroβ₂  : ∀{t}     →  I'.isZero (I'.suc t) ↦ I'.false
  +β₁       : ∀{t}     →  (I'.zero I'.+ t) ↦ t
  +β₂       : ∀{t t'}  →  (I'.suc t I'.+ t') ↦ I'.suc (t I'.+ t')
  iteβ₁     : ∀{t t'}  →  I'.ite I'.true t t' ↦ t
  iteβ₂     : ∀{t t'}  →  I'.ite I'.false t t' ↦ t'

  isZero-cong : ∀{t t'} → t ↦ t' → I'.isZero t ↦ I'.isZero t'
  +-cong₁     : ∀{t₁ t₂ t₁'} → t₁ ↦ t₁' → (t₁ I'.+ t₂) ↦ (t₁' I'.+ t₂)
  +-cong₂     : ∀{t₁ t₂ t₂'} → t₂ ↦ t₂' → (t₁ I'.+ t₂) ↦ (t₁ I'.+ t₂')
  ite-cong    : ∀{t₁ t₂ t₃ t₁'} → t₁ ↦ t₁' → (I'.ite t₁ t₂ t₃) ↦ (I'.ite t₁' t₂ t₃)
\end{code}
The idea is that if there is a \verb$p : t ↦ t'$, this means that the program
\verb$t$ evaluates to \verb$t'$ in one step. Running a program means that we have a
sequence of evaluation steps: \verb$t ↦ t' ↦ t'' ↦ ⋯$ 

There is a rewriting rule corresponding to each equation in our
algebraic structure. Then there are congruence rules (order rules)
which express which parameters and when should be evaluated. In our
example rewriting system above we said that rewriting can happen
anywhere except for \verb$I'.ite$ it only happens in the first
parameter.

Taking the reflexive transitive closure of \verb$↦$ we obtain the
multi-step rewriting relation \verb$↦*$, taking the symmetric closure
of \verb$↦*$ we get the conversion relation (definitial equality)
\verb$∼$ which in the case of NatBool, corresponds to equality of
syntactic terms.
\begin{exe}
  Prove that \verb$t ≡ t'$ is logically equivalent to \verb$t ~ t'$ for all \verb$t$ and \verb$t'$.
\end{exe}

For other languages this is not necessarily the case. The transition
system contains more information than the algebraic description with
equations. Transition is directed and is not necessarily a
congruence. We are not interested in this additional information, this
is why we describe languages using well-typed syntax and equations.
