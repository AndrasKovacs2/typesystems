\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind2.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _,Σ_)
open import Ind2.Algebra
\end{code}
\begin{code}
module M where
  data Ar (C : Set) : Set where
    X : Ar C
    X⇒_ : Ar C → Ar C
    C⇒_ : Ar C → Ar C

  data Sign (C : Set) : Set where
    ◆ : Sign C
    _▷_ : Sign C → Ar C → Sign C
    
  infixr 6 X⇒_
  infixr 6 C⇒_
  infixl 5 _▷_

  _ᴬA : ∀ {C} → Ar C → Set → Set
  (X ᴬA) A = A
  ((X⇒ R) ᴬA) A = A → (R ᴬA) A
  _ᴬA {C} (C⇒ R) A = C → (R ᴬA) A

  _ᴬ : ∀ {C} → Sign C → Set → Set
  (◆ ᴬ) A = ↑p 𝟙
  ((Ω ▷ R) ᴬ) A = (Ω ᴬ) A ×m (R ᴬA) A

  data VarI (C : Set) : Sign C → Ar C → Set where
    vz : ∀ {Ω R} → VarI C (Ω ▷ R) R
    vs : ∀ {Ω R Q} → VarI C Ω R → VarI C (Ω ▷ Q) R

  data TmI (C : Set) : Sign C → Ar C → Set where
    var : ∀ {Ω R} → VarI C Ω R → TmI C Ω R
    _$_ : ∀ {Ω R} → TmI C Ω (X⇒ R) → TmI C Ω X → TmI C Ω R
    _$̂_ : ∀ {Ω R} → TmI C Ω (C⇒ R) → C → TmI C Ω R

  infixl 6 _$_
  infixl 6 _$̂_

  ind : ∀ {C} → Sign C → Set
  ind {C} Ω = TmI C Ω X

  _ⱽ : ∀ {C} → Sign C → Sign C → Set
  (◆ ⱽ) Ω' = ↑p 𝟙
  _ⱽ {C} (Ω ▷ R) Ω' = (Ω ⱽ) Ω' ×m VarI C Ω' R

  shift : ∀ {C} {Ω Ω' : Sign C}{Q : Ar C} → (Ω ⱽ) Ω' → (Ω ⱽ) (Ω' ▷ Q)
  shift {Ω = ◆} .*↑ = *↑
  shift {Ω = Ω ▷ R} (α ,Σ v) = shift α ,Σ vs v

  conVar : ∀ {C} (Ω : Sign C) → (Ω ⱽ) Ω
  conVar ◆ = *↑
  conVar (Ω ▷ R) = shift (conVar Ω) ,Σ vz

  conA : ∀ {C} {R : Ar C}{Ω' : Sign C} → TmI C Ω' R → (R ᴬA) (ind Ω')
  conA {R = X} t = t
  conA {R = X⇒ R} f = λ u → conA (f $ u)
  conA {R = C⇒ R} f = λ a → conA (f $̂ a)

  con' : ∀ {C} {Ω Ω' : Sign C}→ (Ω ⱽ) Ω' → (Ω ᴬ) (ind Ω')
  con' {Ω = ◆} .*↑ = *↑
  con' {Ω = Ω ▷ R} (α ,Σ v) = con' α ,Σ conA (var v)

  con : ∀ {C} (Ω : Sign C) → (Ω ᴬ) (ind Ω)
  con Ω = con' (conVar Ω)

  recVar : ∀ {A C} {R : Ar C}{Ω : Sign C} → (Ω ᴬ) A → VarI C Ω R → (R ᴬA) A
  recVar (ω ,Σ c) vz = c
  recVar (ω ,Σ c) (vs v) = recVar ω v

  rec' : ∀ {A C} {R : Ar C}{Ω : Sign C} → (Ω ᴬ) A → TmI C Ω R → (R ᴬA) A
  rec' ω (var v) = recVar ω v
  rec' ω (f $ u) = rec' ω f (rec' ω u)
  rec' ω (f $̂ a) = rec' ω f a

  rec : ∀ {A C} (Ω : Sign C) → (Ω ᴬ) A → ind Ω → A
  rec Ω ω t = rec' ω t
  
  _ᴹA : ∀ {Γ : Set} {A B C} (R : Ar C) → (Γ → (R ᴬA) A) → (Γ → (R ᴬA) B) →
    ((Γ → A) → (Γ → B)) → Prop
  (X ᴹA) t u F = F t ≡ u
  ((X⇒ R) ᴹA) f g F = ∀ {t} → (R ᴹA) (f ∘' t) (g ∘' F t) F
  ((C⇒ R) ᴹA) f g F = ∀ {t} → (R ᴹA) (f ∘' t) (g ∘' t) F

  _ᴹ : ∀ {Γ : Set} {A B C} (Ω : Sign C) → (Γ → (Ω ᴬ) A) → (Γ → (Ω ᴬ) B) →
    ((Γ → A) → (Γ → B)) → Prop
  (◆ ᴹ) t u F = 𝟙'
  ((Ω ▷ R) ᴹ) t u F =
    (Ω ᴹ) (π₁ ∘f t) (π₁ ∘f u) F ×p (R ᴹA) (π₂ ∘f t) (π₂ ∘f u) F

  postulate
    indβ : ∀ {Γ : Set} {A C} {Ω : Sign C} {ω : Γ → (Ω ᴬ) A} →
      (Ω ᴹ) (λ _ → con Ω) ω (λ t γ → rec Ω (ω γ) (t γ))

  N : Sign (↑p 𝟘)
  N = ◆ ▷ X ▷ X⇒ X

  Nat : Set
  Nat = ind N

  zero : Nat
  zero = π₂ (π₁ (con N))

  suc : Nat → Nat
  suc = π₂ (con N)

  Nβ : {Γ A : Set} {ω : Γ → (N ᴬ) A} →
    (N ᴹ) (λ _ → con N) ω (λ t γ → rec N (ω γ) (t γ))
  Nβ = *' ,p refl ,p refl

St : Algebra
St = record
       { Ar = M.Ar
       ; Sign = M.Sign
       ; Ty = Set
       ; Con = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; X = M.X
       ; X⇒_ = M.X⇒_
       ; C⇒_ = M.C⇒_
       ; ◆ = M.◆
       ; _▷_ = M._▷_
       ; Unit = ↑p 𝟙
       ; Empty = ↑p 𝟘
       ; _⇒_ = λ A B → A → B
       ; _×_ = _×m_
       ; _+_ = _⊎_
       ; ind = M.ind
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×m_
       ; _ᴬA = M._ᴬA
       ; _ᴬ = M._ᴬ
       ; _ᴹA = M._ᴹA
       ; _ᴹ = M._ᴹ
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ,Σ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; tt = const *↑
       ; ⟨_,_⟩ = λ u v γ → u γ ,Σ v γ
       ; proj₁ = λ t γ → π₁ (t γ)
       ; proj₂ = λ t γ → π₂ (t γ)
       ; absurd = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; inl = λ t γ → ι₁ (t γ)
       ; inr = λ t γ → ι₂ (t γ)
       ; case = λ t u v γ →
                  case⊎ (t γ) (λ a → u (γ ,Σ a)) (λ b → v (γ ,Σ b))
       ; con = λ Ω _ → M.con Ω
       ; rec = λ Ω u t γ → M.rec Ω (u γ) (t γ)
       ; ᴬAX = refl
       ; ᴬAX⇒ = refl
       ; ᴬAA⇒ = refl
       ; ᴬ◆ = refl
       ; ᴬ▷ = refl
       ; ᴹAX = refl
       ; ᴹAX⇒ = refl
       ; ᴹAA⇒ = refl
       ; ᴹ◆ = refl
       ; ᴹ▷ = refl
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; uη = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; indβ = M.indβ
       ; lam[] = refl
       ; tt[] = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       ; con[] = refl
       ; rec[] = refl
       }
module St = Algebra St
\end{code}
