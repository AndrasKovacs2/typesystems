\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Stream.LogicalPredicate where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import Stream.Algebra
open import Stream.Standard

open I
open Algebra St using (⟦_⟧C ; ⟦_⟧T ; ⟦_⟧S ; ⟦_⟧t)
\end{code}
\begin{code}
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀ {n'} → PNat n' → PNat (suc n')

indPNat : ∀ {i} (P : ∀ {n'} → PNat n' → Set i) →
  P Pzero → (∀ {n'} {n : PNat n'} → P n → P (Psuc n)) →
  ∀ {n'} (n : PNat n') → P n
indPNat P u v Pzero = u
indPNat P u v (Psuc n) = v (indPNat P u v n)

recPNat : ∀ {i} (A : Tm ∙ Nat → Set i) →
  A zero → (∀ {n'} → A n' → A (suc n')) →
  ∀ {n'} → PNat n' → A n'
recPNat A u v n = indPNat (λ {n'} _ → A n') u v n

Nat-Prec : ∀ {i A'} (A : Tm ∙ A' → Set i)
  {u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'} →
  A u' → (∀ {t'} → A t' → A (v' [ id , t' ])) →
  ∀ {n'} → PNat n' → A (rec u' v' n')
Nat-Prec A {u'}{v'} u v n = recPNat (λ n' → A (rec u' v' n')) u v n

record PStream (s' : Tm ∙ Stream) : Set where
  coinductive
  field
    Phead : PNat (head s')
    Ptail : PStream (tail s')
open PStream public 

{-
genPStream : ∀ {i} (A : Tm ∙ Stream → Set i) →
  (∀ {s'} → A s' → PNat (head s')) →
  (∀ {s'} → A s' → A (tail s')) →
  ∀ {s'} → A s' → PStream s'
Phead (genPStream A u v t) = u t
Ptail (genPStream A u v t) = genPStream A u v (v t)
-}

Stream-Pgen : ∀ {i A'} (A : Tm ∙ A' → Set i)
  {u' : Tm (∙ ▹ A') Nat}{v' : Tm (∙ ▹ A') A'} →
  (∀ {t'} → A t' → PNat (u' [ id , t' ])) →
  (∀ {t'} → A t' → A (v' [ id , t' ])) →
  ∀ {t'} → A t' → PStream (gen u' v' t')
Phead (Stream-Pgen A u v t) = u t
Ptail (Stream-Pgen A u v t) = Stream-Pgen A u v (v t)

{-
genPStream : ∀ {i A'} (A : Tm ∙ Stream → Set i)
  {u' : Tm (∙ ▹ A') Nat}{v' : Tm (∙ ▹ A') A'} →
  (∀ {t'} → A (gen u' v' t') → PNat (u' [ id , t' ])) →
  (∀ {t'} → A (gen u' v' t') → A (gen u' v' (v' [ id , t' ]))) →
  ∀ {t'} → A (gen u' v' t') → PStream (gen u' v' t')
genPStream A {u'}{v'} u v t = Stream-Pgen (λ t' → A (gen u' v' t')) u v t
-}

LP : DepAlgebra
LP = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; Nat = PNat
         ; Stream = PStream
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; zero = λ _ → Pzero
         ; suc = λ n x → Psuc (n x)
         ; rec = λ { {A = A} u v n x →
                   Nat-Prec A (u x) (λ y → v (x ,Σ y)) (n x) }
         ; head = λ s x → Phead (s x)
         ; tail = λ s x → Ptail (s x)
         ; gen = λ { {A = A} u v t x →
                   Stream-Pgen A (λ y → u (x ,Σ y))
                                 (λ y → v (x ,Σ y))
                                 (t x) }
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; ⇒β = refl
         ; ⇒η = refl
         ; Natβ₁ = refl
         ; Natβ₂ = refl
         ; Streamβ₁ = refl
         ; Streamβ₂ = refl
         ; lam[] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; rec[] = refl
         ; head[] = refl
         ; tail[] = refl
         ; gen[] = refl
         }
module LP = DepAlgebra LP

pNat : ∀ n → PNat n
pNat n = LP.⟦ n ⟧t {id} *↑

pStream : ∀ s → PStream s
pStream s = LP.⟦ s ⟧t {id} *↑

completenessN : ∀ {n} → ⌜ eval n ⌝N ≡ n
completenessN {n} = 
  let A n = ↑p (⌜ eval n ⌝N ≡ n)
      p = LP.⟦ n ⟧t {id} *↑
  in  ↓[ recPNat A refl↑ (λ n → ↑[ ap suc ↓[ n ]↓ ]↑) p ]↓

{-
open import Stream.Examples

s : Tm ∙ Stream
s = gen q (app (app times) [ id , q , q ]) (suc (suc zero))

ps : PStream s
ps = Stream-Pgen PNat idf (λ {t'} n → Nat-Prec PNat {zero} {rec q (suc q) (t' [ p ])} {t'} Pzero (λ {t''} n' → Nat-Prec PNat {t''} {suc q} {t'} n' Psuc n) n) (Psuc (Psuc Pzero))
-}
\end{code}
