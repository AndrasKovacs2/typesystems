\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Stream.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import Stream.Algebra

open I
\end{code}
\begin{code}
record 𝕊 : Set where
  coinductive
  field
    Hd : ℕ
    Tl : 𝕊
open 𝕊 public

gen𝕊 : ∀ {i} {A : Set i} → (A → ℕ) → (A → A) → A → 𝕊
Hd (gen𝕊 u v t) = u t
Tl (gen𝕊 u v t) = gen𝕊 u v (v t)

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; Stream = 𝕊
       ; _⇒_ = λ A B → A → B
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ,Σ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; zero = λ _ → O
       ; suc = λ n γ → S (n γ)
       ; rec = λ u v n γ → indℕ _ (u γ) (λ a → v (γ ,Σ a)) (n γ)
       ; head = λ s γ → Hd (s γ)
       ; tail = λ s γ → Tl (s γ)
       ; gen = λ u v t γ → gen𝕊 (λ a → u (γ ,Σ a)) (λ a → v (γ ,Σ a)) (t γ)
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; Natβ₁ = refl
       ; Natβ₂ = refl
       ; Streamβ₁ = refl
       ; Streamβ₂ = refl
       ; lam[] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; rec[] = refl
       ; head[] = refl
       ; tail[] = refl
       ; gen[] = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑

⌜_⌝N : ℕ → Tm ∙ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N
\end{code}
