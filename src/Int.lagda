\subsection{Another example for normalisation: integers}

In this subsection we define integers as an initial algebra. The representation of integers is from \cite{10.1145/3373718.3394760}.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Int where

open import Lib

module I where
  postulate
    Z     : Set
    zero  : Z
    suc   : Z → Z
    pred  : Z → Z
    sucpred : (i : Z) → suc (pred i) ≡ i
    predsuc : (i : Z) → pred (suc i) ≡ i
\end{code}

An integer algebra has five components, a set, a zero element, a successor operation, a predecessor operation and two equalities saying that successor after or before predecessor is the identity.
\begin{code}
record Algebra {i} : Set (lsuc i) where
  field
    Z        : Set i
    zero     : Z
    suc      : Z → Z
    pred     : Z → Z
    sucpred  : (i : Z) → suc (pred i) ≡ i
    predsuc  : (i : Z) → pred (suc i) ≡ i
\end{code}
We have a recursor.
\begin{code}
  postulate
    ⟦_⟧      : I.Z → Z
    ⟦zero⟧   : ⟦ I.zero ⟧ ≡ zero
    ⟦suc⟧    : {i : I.Z} → ⟦ I.suc i ⟧ ≡ suc ⟦ i ⟧
    ⟦pred⟧   : {i : I.Z} → ⟦ I.pred i ⟧ ≡ pred ⟦ i ⟧
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦pred⟧ #-}
\end{code}
Integers themselves are given by the initial integer algebra.
\begin{code}
ℤ = I.Z
\end{code}

Examples of equalities which hold in any integer algebra.
\begin{code}
module examples {i}(M : Algebra {i}) where
  open Algebra M
  
  eq1 : pred (suc (pred zero)) ≡ pred zero
  eq1 = predsuc (pred zero)

  eq2 : pred (suc (suc (pred zero))) ≡ zero
  eq2 = pred (suc (suc (pred zero)))
                                     ≡⟨ ap (λ i → pred (suc i)) (sucpred _) ⟩
        pred (suc zero)
                                     ≡⟨ predsuc _ ⟩
        zero
                                     ∎
\end{code}

Dependent algebras and the eliminator.
\begin{code}
record DepAlgebra {i} : Set (lsuc i) where
  field
    Z        : I.Z → Set i
    zero     : Z I.zero
    suc      : {i' : I.Z} → Z i' → Z (I.suc  i')
    pred     : {i' : I.Z} → Z i' → Z (I.pred i')
    sucpred  : {i' : I.Z}(i : Z i') → transport Z (I.sucpred i') (suc (pred i)) ≡ i
    predsuc  : {i' : I.Z}(i : Z i') → transport Z (I.predsuc i') (pred (suc i)) ≡ i
  postulate
    ⟦_⟧      : (n : I.Z) → Z n
    ⟦zero⟧   : ⟦ I.zero ⟧ ≡ zero
    ⟦suc⟧    : {i : I.Z} → ⟦ I.suc i ⟧ ≡ suc ⟦ i ⟧
    ⟦pred⟧   : {i : I.Z} → ⟦ I.pred i ⟧ ≡ pred ⟦ i ⟧
    {-# REWRITE ⟦zero⟧ #-}
\end{code}

The algebra `M i` where `zero` is `i`, everything else comes from the syntax.
\begin{code}
M : I.Z → Algebra
M i = record
  { Z        = I.Z
  ; zero     = i
  ; suc      = I.suc
  ; pred     = I.pred
  ; sucpred  = I.sucpred
  ; predsuc  = I.predsuc
  }
module M i = Algebra (M i)
\end{code}
Now we have addition `j +ℤ i` which replaces `I.zero` inside `j` by `i`:
\begin{code}
_+ℤ_ : I.Z → I.Z → I.Z
j +ℤ i = M.⟦_⟧ i j

test+ : {i : I.Z} → I.suc (I.suc (I.pred I.zero)) +ℤ i ≡ I.suc (I.suc (I.pred i))
test+ = refl

test+' : {i : I.Z} → I.pred (I.suc (I.suc (I.pred I.zero))) +ℤ i ≡ I.pred (I.suc (I.suc (I.pred i)))
test+' = refl
\end{code}

Normal forms and examples.
\begin{code}
data Nf  : Set where
  -suc   : ℕ → Nf
  zero   : Nf
  +suc   : ℕ → Nf

minusThree minusFive plusSix : Nf
minusThree  = -suc 2
minusFive   = +suc 4
plusSix     = +suc 5

sucNf : Nf → Nf
sucNf (-suc O)       = zero
sucNf (-suc (S n))   = -suc n
sucNf zero           = +suc 0
sucNf (+suc n)       = +suc (S n)

predNf : Nf → Nf
predNf (-suc n)      = -suc (S n)
predNf zero          = -suc 0
predNf (+suc O)      = zero
predNf (+suc (S n))  = +suc n
\end{code}

Normalisation.
\begin{code}
N : Algebra
N = record
  { Z       = Nf
  ; zero    = zero
  ; suc     = sucNf
  ; pred    = predNf
  ; sucpred = λ { (-suc O) → refl ; (-suc (S n)) → refl ; zero → refl ; (+suc O) → refl ; (+suc (S n)) → refl }
  ; predsuc = λ { (-suc O) → refl ; (-suc (S n)) → refl ; zero → refl ; (+suc O) → refl ; (+suc (S n)) → refl }
  }
module N = Algebra N

norm : I.Z → Nf
norm = N.⟦_⟧
\end{code}
Quoting back normal forms to integers:
\begin{code}
⌜_⌝ : Nf → I.Z
⌜ -suc O ⌝      = I.pred I.zero
⌜ -suc (S n) ⌝  = I.pred ⌜ -suc n ⌝
⌜ zero ⌝        = I.zero
⌜ +suc O ⌝      = I.suc I.zero
⌜ +suc (S n) ⌝  = I.suc ⌜ +suc n ⌝
\end{code}
A unit test for normalisation:
\begin{code}
testnorm : ⌜ norm (I.pred (I.pred (I.suc (I.pred (I.pred (I.pred (I.suc I.zero))))))) ⌝ ≡ I.pred (I.pred (I.pred I.zero))
testnorm = refl
\end{code}
Stability of normalisation:
\begin{code}
stab : (v : Nf) → norm ⌜ v ⌝ ≡ v
stab (-suc O)      = refl
stab (-suc (S n))  = ap predNf (stab (-suc n))
stab zero          = refl
stab (+suc O)      = refl
stab (+suc (S n))  = ap sucNf (stab (+suc n))
\end{code}
Helper lemmas for completeness:
\begin{code}
⌜suc⌝ : (v : Nf) → ⌜ sucNf v ⌝ ≡ I.suc ⌜ v ⌝
⌜suc⌝ (-suc O)      = I.sucpred _ ⁻¹
⌜suc⌝ (-suc (S n))  = I.sucpred _ ⁻¹
⌜suc⌝ zero          = refl
⌜suc⌝ (+suc n)      = refl

⌜pred⌝ : (v : Nf) → ⌜ predNf v ⌝ ≡ I.pred ⌜ v ⌝
⌜pred⌝ (-suc n)      = refl
⌜pred⌝ zero          = refl
⌜pred⌝ (+suc O)      = I.predsuc _ ⁻¹
⌜pred⌝ (+suc (S n))  = I.predsuc _ ⁻¹
\end{code}
Completeness of normalisation:
\begin{code}
Comp : DepAlgebra
Comp = record
  { Z        = λ i → ↑p (⌜ norm i ⌝ ≡ i)
  ; zero     = ↑[ refl ]↑
  ; suc      = λ e → ↑[ ⌜suc⌝ _ ◾ ap I.suc ↓[ e ]↓ ]↑
  ; pred     = λ e → ↑[ ⌜pred⌝ _ ◾ ap I.pred ↓[ e ]↓ ]↑
  ; sucpred  = λ _ → refl
  ; predsuc  = λ _ → refl
  }
module Comp = DepAlgebra Comp

comp : (i : I.Z) → ⌜ norm i ⌝ ≡ i
comp i = ↓[ Comp.⟦ i ⟧ ]↓
\end{code}
