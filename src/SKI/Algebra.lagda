\chapter{SKI combinator calculus}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}

module SKI.Algebra where

open import Lib using (lsuc ; refl ; _≡_ ; _◾_ ; coe ; ap)

module Init where
  infixl 10 _ₒ_
  
  postulate
    Tm : Set
    K : Tm
    S : Tm
    _ₒ_ : Tm → Tm → Tm
    Kβ : ∀ {u v} → K ₒ u ₒ v ≡ u
    Sβ : ∀ {u v t} → S ₒ u ₒ v ₒ t ≡ u ₒ t ₒ (v ₒ t)

  I : Tm
  I = S ₒ K ₒ K

  Iβ : ∀{t} → I ₒ t ≡ t
  Iβ = Sβ ◾ Kβ

  {-# REWRITE Iβ Kβ Sβ #-}

record Algebra {i} : Set (lsuc i) where
  infixl 10 _ₒ_

  field
    Tm : Set i
    K : Tm
    S : Tm
    _ₒ_ : Tm → Tm → Tm
    Kβ : ∀ {u v} → K ₒ u ₒ v ≡ u
    Sβ : ∀ {u v t} → S ₒ u ₒ v ₒ t ≡ u ₒ t ₒ (v ₒ t)

  I : Tm
  I = S ₒ K ₒ K

  Iβ : ∀{t} → I ₒ t ≡ t
  Iβ = Sβ ◾ Kβ

  -------------------------------------------
  -- recursor
  -------------------------------------------

  postulate
    ⟦_⟧ : Init.Tm → Tm
    ⟦K⟧ : ⟦ Init.K ⟧ ≡ K
    ⟦S⟧ : ⟦ Init.S ⟧ ≡ S
    ⟦ₒ⟧ : ∀ {u v} → ⟦ u Init.ₒ v ⟧ ≡ ⟦ u ⟧ ₒ ⟦ v ⟧
    {-# REWRITE ⟦K⟧ ⟦S⟧ ⟦ₒ⟧ #-}

  ⟦I⟧ : ⟦ Init.I ⟧ ≡ I
  ⟦I⟧ = refl
  

record DepAlgebra {i} : Set (lsuc i) where
  infixl 10 _ₒ_

  field
    Tm : Init.Tm → Set i
    K : Tm Init.K
    S : Tm Init.S
    _ₒ_ : ∀{u v} → Tm u → Tm v → Tm (u Init.ₒ v)
    Kβ : ∀ {u' v'} {u : Tm u'} {v : Tm v'} → K ₒ u ₒ v ≡ u
    Sβ : ∀ {u' v' t'} {u : Tm u'} {v : Tm v'} {t : Tm t'} → S ₒ u ₒ v ₒ t ≡ u ₒ t ₒ (v ₒ t)

  I : Tm Init.I
  I = S ₒ K ₒ K

  -- eliminator
  -------------------------------------------

  postulate
    ⟦_⟧ : (t : Init.Tm) → Tm t
    ⟦K⟧ : ⟦ Init.K ⟧ ≡ K
    ⟦S⟧ : ⟦ Init.S ⟧ ≡ S
    ⟦ₒ⟧ : ∀{u v} → ⟦ u Init.ₒ v ⟧ ≡ ⟦ u ⟧ ₒ ⟦ v ⟧
    {-# REWRITE ⟦K⟧ ⟦S⟧ ⟦ₒ⟧ #-}

  ⟦I⟧ : ⟦ Init.I ⟧ ≡ I
  ⟦I⟧ = refl
  
\end{code}
