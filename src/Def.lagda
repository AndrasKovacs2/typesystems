\section{Well-typed syntax with equations}

In addition to the equations such as e.g.\ \verb$isZero zero = true$,
we need equations like the following.
\begin{verbatim}
def t (v0 + v0) = t + t
def t (def t' (v0 + v1)) = t' + t
def t (def t' (v1 + (v0 + (v1 + 3)))) = t + (t' + (t + 3))\end{verbatim}
In general, we would like to have \verb$def t t' = t'[id,t]$ where
\verb$t'[id,t]$ is the same as \verb$t'$, but all \verb$v0$s are replaced
by \verb$t$, all \verb$v1$s are replaced by \verb$v0$s, all \verb$v2$s
are replaced by \verb$v1$s, and so on. The nicest way to express this
is to add a new sort of \emph{substitutions}:
\begin{verbatim}
Sub : Con → Con → Set\end{verbatim}
If \verb$Δ = ∙ ▹ A₁ ▹ A₂ ▹ ... ▹ Aₙ$, then a \verb$σ : Sub Γ Δ$ is a list of terms \verb$σ = (t₁ , t₂ , ... tₙ)$  where
\begin{verbatim}
t₁ : Tm Γ A₁
t₂ : Tm Γ A₂
...
tₙ : Tm Γ Aₙ\end{verbatim}
We call \verb$Sub Γ Δ$ a substitution from \verb$Γ$ to \verb$Δ$.
It \verb$Sub Γ Δ$ contains enough information to turn a term
defined in context \verb$Δ$ into a term defined in context
\verb$Γ$. This is witnessed by a new operator called
\emph{instantiation  of substitution}:
\begin{verbatim}
_[_] : Tm Δ A → Sub Γ Δ → Tm Γ A\end{verbatim}
There is an identity substitution \verb$id : Sub Γ Γ$ which doesn't do anything, i.e.\
\verb$t [ id ] = t$ and with this we can express the equation for let expressions:
\begin{verbatim}
def t t' = t' [ id , t ]\end{verbatim}
Substitutions can be composed: \verb$σ ∘ δ : Sub Γ Δ$ if \verb$δ : Sub Γ Θ$ and \verb$σ : Sub Θ Δ$.
The \verb$v0$ De Bruijn index is now called \verb$q : Tm (Γ ▹ A) A$ and
we have a substition \verb$p : Sub (Γ ▹ A) Γ$ which forgets about the
last element in the context: \verb$p ∘ (σ , t) = p$.
The rest of the variables can be defined: \verb$v1 = q [ p ]$,
\verb$v2 = q [ p ∘ p ]$, \verb$v3 = q [ p ∘ p ∘ p ]$, and so on.
\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Def where

open import Lib renaming (_∘_ to _∘f_; _,_ to _,Σ_)

module I where
  data Ty   : Set where
    Nat     : Ty
    Bool    : Ty

  data Con  : Set where
    ∙       : Con
    _▹_     : Con → Ty → Con

  infixl 5 _▹_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]

  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A

    zero : ∀ {Γ} → Tm Γ Nat
    suc : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat
    isZero : ∀ {Γ} → Tm Γ Nat → Tm Γ Bool
    _+_ : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    
    true : ∀ {Γ} → Tm Γ Bool
    false : ∀ {Γ} → Tm Γ Bool
    ite : ∀ {Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    zero[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[] : ∀ {Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    +[] : ∀ {Γ Δ} {m n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (m + n) [ σ ] ≡ (m [ σ ]) + (n [ σ ])
      
    true[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[] : ∀ {Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    
    isZeroβ₁ : ∀ {Γ} → isZero (zero {Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ} {n : Tm Γ Nat} → isZero (suc n) ≡ false
    +β₁ : ∀ {Γ} {n : Tm Γ Nat} → zero + n ≡ n
    +β₂ : ∀ {Γ} {m n : Tm Γ Nat} → (suc m) + n ≡ suc (m + n)
    
    iteβ₁ : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl


  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE zero[] suc[] isZero[] +[] #-}
  {-# REWRITE true[] false[] ite[] #-}
  {-# REWRITE isZeroβ₁ isZeroβ₂ +β₁ +β₂ #-}
  {-# REWRITE iteβ₁ iteβ₂ #-}
\end{code}
\begin{code}
record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
\end{code}
\begin{code}[hide]
  infixl 5 _▹_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  
  field
\end{code}
A Def-algebra consists of the following three groups of components:
\begin{itemize}
\item The substitution calculus:
\begin{code}
    Con       : Set i
    Sub       : Con → Con → Set k
    Ty        : Set j
    Tm        : Con → Ty → Set l

    _∘_       : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id        : ∀{Γ} → Sub Γ Γ
    ass       : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl       : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr       : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]      : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]      : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]       : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    _▹_       : Con → Ty → Con
    _,_       : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂       : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η        : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
\end{code}
\item Booleans:
\begin{code}
    Bool      : Ty
    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁     : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂     : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]   : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]     : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
\end{code}
\item Natural numbers:
\begin{code}
    Nat       : Ty
    zero      : ∀{Γ} → Tm Γ Nat
    suc       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+_       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁  : ∀{Γ} → isZero (zero {Γ}) ≡ true
    isZeroβ₂  : ∀{Γ} {n : Tm Γ Nat} → isZero (suc n) ≡ false
    +β₁       : ∀{Γ} {n : Tm Γ Nat} → zero + n ≡ n
    +β₂       : ∀{Γ} {m n : Tm Γ Nat} → (suc m) + n ≡ suc (m + n)

    zero[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]     : ∀{Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
                (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[]  : ∀{Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
                (isZero n) [ σ ] ≡ isZero (n [ σ ])
    +[]       : ∀{Γ Δ} {m n : Tm Δ Nat}{σ : Sub Γ Δ} →
                (m + n) [ σ ] ≡ (m [ σ ]) + (n [ σ ])
\end{code}
\end{itemize}
Abbreviations and provable equalities:
\begin{code}
  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
\end{code}
\begin{code}[hide]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)
  
\end{code}
\begin{code}
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
\end{code}
\begin{code}[hide]
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl
\end{code}
\begin{code}
  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]
\end{code}
\begin{code}[hide]
  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦+⟧ : ∀ {Γ} {m n : I.Tm Γ I.Nat} →
      ⟦ m I.+ n ⟧t ≡ ⟦ m ⟧t + ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

module Example where
  open I
  pt : def false (ite q true (isZero (ite q zero (suc zero)))) ≡ false {∙}
  pt =
\end{code}
The term \verb$let x:= false in if x then true else isZero (if x then zero else (suc zero))$ and showing that it is equal to \verb$false$:
\begin{code}
       def false (ite q true (isZero (ite q zero (suc zero))))
                                                                                      ≡≡
       ite q true (isZero (ite q zero (suc zero))) [ id , false ]
                                                                                      ≡≡
       ite (q [ id , false ]) true (isZero (ite q zero (suc zero)) [ id , false ])
                                                                                      ≡≡
       ite false true (isZero (ite q zero (suc zero)  [ id , false ]))
                                                                                      ≡≡
       isZero (ite q zero (suc zero)  [ id , false ])
                                                                                      ≡≡
       isZero (ite false zero (suc zero))
                                                                                      ≡≡
       isZero (suc zero)
                                                                                      ≡≡
       false
\end{code}
\begin{code}[hide]
                                                                                      ∎

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  
  field
    Con : I.Con → Set i
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    Nat : Ty I.Nat
    Bool : Ty I.Bool

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])

    zero : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Nat I.zero
    suc : ∀ {Γ' n'} {Γ : Con Γ'} →
      Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    isZero : ∀ {Γ' n'} {Γ : Con Γ'} →
      Tm Γ Nat n' → Tm Γ Bool (I.isZero n')
    _+_ : ∀ {Γ' m' n'} {Γ : Con Γ'} →
      Tm Γ Nat m' → Tm Γ Nat n' → Tm Γ Nat (m' I.+ n')

    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
    ite : ∀ {Γ' A' b' u' v'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Bool b' → Tm Γ A u' → Tm Γ A v' → Tm Γ A (I.ite b' u' v')

    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    zero[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      zero [ σ ] ≡ zero
    suc[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    isZero[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (isZero n) [ σ ] ≡ isZero (n [ σ ])
    +[] : ∀ {Γ' Δ' m' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {m : Tm Δ Nat m'}{n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (m + n) [ σ ] ≡ (m [ σ ]) + (n [ σ ])

    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' u' v' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ Bool b'}{u : Tm Δ A u'}{v : Tm Δ A v'}
      {σ : Sub Γ Δ σ'} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    
    isZeroβ₁ : ∀ {Γ'} {Γ : Con Γ'} → isZero (zero {Γ = Γ}) ≡ true
    isZeroβ₂ : ∀ {Γ' n'} {Γ : Con Γ'}{n : Tm Γ Nat n'} →
      isZero (suc n) ≡ false
    +β₁ : ∀ {Γ' n'} {Γ : Con Γ'}{n : Tm Γ Nat n'} → zero + n ≡ n
    +β₂ : ∀ {Γ' m' n'} {Γ : Con Γ'}{m : Tm Γ Nat m'}{n : Tm Γ Nat n'} →
      (suc m) + n ≡ suc (m + n)

    iteβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite false u v ≡ v

  def : ∀ {Γ' A' B' t' u'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.isZero n ⟧t ≡ isZero ⟦ n ⟧t
    ⟦+⟧ : ∀ {Γ} {m n : I.Tm Γ I.Nat} →
      ⟦ m I.+ n ⟧t ≡ ⟦ m ⟧t + ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl
\end{code}

\subsection{Normalisation in the empty context}

Normalisation in the empty context is also called canonicity. For
normalisation in arbitrary contexts, see e.g.\ \cite{lmcs:4005}.

The standard algebra:
\begin{code}[hide]
open I
\end{code}
\begin{code}
isO : ℕ → 𝟚
isO O = I
isO (S n) = O

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; Bool = 𝟚
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; zero = const O
       ; suc = S ∘f_
       ; isZero = isO ∘f_
       ; _+_ = λ m n γ → m γ +ℕ n γ
       ; true = const I
       ; false = const O
       ; ite = λ b u v γ → if b γ then u γ else v γ
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; isZero[] = refl
       ; +[] = refl
       ; true[] = refl
       ; false[] = refl
       ; ite[] = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; iteβ₁ = refl
       ; iteβ₂ = refl
       }
open Algebra St using (⟦_⟧C ; ⟦_⟧T ; ⟦_⟧S ; ⟦_⟧t)
\end{code}
Normalisation of terms and substitutions is just interpretation in the standard algebra.
\begin{code}
normₜ : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
normₜ t = ⟦ t ⟧t *↑

normₛ : ∀ {Γ} → Sub ∙ Γ → ⟦ Γ ⟧C
normₛ σ = ⟦ σ ⟧S *↑
\end{code}
Quoting normal forms to terms:
\begin{code}
⌜_⌝N : ℕ → Tm ∙ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc ⌜ n ⌝N

⌜_⌝B : 𝟚 → Tm ∙ Bool
⌜ O ⌝B = false
⌜ I ⌝B = true

⌜_⌝ₜ : ∀ {A} → ⟦ A ⟧T → Tm ∙ A
⌜_⌝ₜ {Nat} = ⌜_⌝N
⌜_⌝ₜ {Bool} = ⌜_⌝B
\end{code}
Quoting commutes with the operations \verb$isZero$, \verb$_+_$ and \verb$ite$:
\begin{code}
isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝ₜ ≡ isZero ⌜ n ⌝ₜ
isZero-⌜⌝ {O} = refl
isZero-⌜⌝ {S n} = refl

+-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝ₜ ≡ ⌜ m ⌝ₜ + ⌜ n ⌝ₜ
+-⌜⌝ {O} = refl
+-⌜⌝ {S m} = ap suc (+-⌜⌝ {m})

ite-⌜⌝ : ∀ {A b} {u v : ⟦ A ⟧T} →
  ⌜_⌝ₜ {A} (if b then u else v) ≡ ite ⌜ b ⌝ₜ ⌜ u ⌝ₜ ⌜ v ⌝ₜ
ite-⌜⌝ {b = O} = refl
ite-⌜⌝ {b = I} = refl
\end{code}
An element of the interpretation of a context can be quoted into
substitution from the empty context:
\begin{code}
⌜_⌝ₛ : ∀ {Γ} → ⟦ Γ ⟧C → Sub ∙ Γ
⌜_⌝ₛ {∙} _ = id
⌜_⌝ₛ {Γ ▹ A} (σ ,Σ t) = ⌜ σ ⌝ₛ , ⌜ t ⌝ₜ
\end{code}

Completeness of normalisation in the empty context is proved by
induction on the syntax.
\begin{code}
Comp : DepAlgebra
Comp = record
         { Con = λ _ → ↑p 𝟙
         ; Ty = λ _ → ↑p 𝟙
         ; Sub = λ _ _ σ' →
                   ∀ {ν'} → ↑p (⌜ normₛ ν' ⌝ₛ ≡ ν') →
                     ↑p (⌜ normₛ (σ' ∘ ν') ⌝ₛ ≡ σ' ∘ ν')
         ; Tm = λ _ _ t' →
                  ∀ {ν'} → ↑p (⌜ normₛ ν' ⌝ₛ ≡ ν') →
                     ↑p (⌜ normₜ (t' [ ν' ]) ⌝ₜ ≡ t' [ ν' ])
         ; ∙ = _
         ; _▹_ = _
         ; Nat = _
         ; Bool = _
         ; _∘_ = λ σ δ → σ ∘f δ
         ; id = idf
         ; ε = λ _ → ↑[ ∙η ◾ ∙η ⁻¹ ]↑
         ; _,_ = λ σ t h → ↑[ ap2 _,_ ↓[ σ h ]↓ ↓[ t h ]↓ ]↑
         ; p = λ h → ↑[ ap (p ∘_) ↓[ h ]↓ ]↑
         ; q = λ h → ↑[ ap (q [_]) ↓[ h ]↓ ]↑
         ; _[_] = λ t σ → t ∘f σ
         ; zero = λ _ → refl↑
         ; suc = λ n h → ↑[ ap suc ↓[ n h ]↓ ]↑
         ; isZero = λ n h → ↑[ isZero-⌜⌝
                             ◾ ap isZero ↓[ n h ]↓ ]↑
         ; _+_ = λ {_ m'} m n {ν'} h →
                   ↑[ +-⌜⌝ {normₜ (m' [ ν' ])}
                    ◾ ap2 _+_ ↓[ m h ]↓ ↓[ n h ]↓ ]↑
         ; true = λ _ → refl↑
         ; false = λ _ → refl↑
         ; ite = λ {_ _ b'} b u v {ν'} h →
                   ↑[ ite-⌜⌝ {b = normₜ (b' [ ν' ])}
                    ◾ ap3 ite ↓[ b h ]↓ ↓[ u h ]↓ ↓[ v h ]↓ ]↑
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; isZero[] = refl
         ; +[] = refl
         ; true[] = refl
         ; false[] = refl
         ; ite[] = refl
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁ = refl
         ; +β₂ = refl
         ; iteβ₁ = refl
         ; iteβ₂ = refl
         }
module Comp = DepAlgebra Comp

completenessₜ : ∀ {A} {t : Tm ∙ A} → ⌜ normₜ t ⌝ₜ ≡ t
completenessₜ {t = t} = ↓[ Comp.⟦ t ⟧t {id} refl↑ ]↓

completenessₛ : ∀ {Γ} {σ : Sub ∙ Γ} → ⌜ normₛ σ ⌝ₛ ≡ σ
completenessₛ {σ = σ} = ↓[ Comp.⟦ σ ⟧S {id} refl↑ ]↓
\end{code}
Stability for terms:
\begin{code}
stabilityN : ∀ {n} → normₜ ⌜ n ⌝N ≡ n
stabilityN {O} = refl
stabilityN {S n} = ap S stabilityN

stabilityB : ∀ {b} → normₜ ⌜ b ⌝B ≡ b
stabilityB {O} = refl
stabilityB {I} = refl

stabilityₜ : ∀ {A} {t : ⟦ A ⟧T} → normₜ (⌜_⌝ₜ {A} t) ≡ t
stabilityₜ {Nat} = stabilityN
stabilityₜ {Bool} = stabilityB
\end{code}
Stability for substitutions:
\begin{code}
stabilityₛ : ∀ {Γ} {σ : ⟦ Γ ⟧C} → normₛ (⌜_⌝ₛ {Γ} σ) ≡ σ
stabilityₛ {∙} = refl
stabilityₛ {Γ ▹ A} = ap2 _,Σ_ (stabilityₛ {Γ}) (stabilityₜ {A})
\end{code}
