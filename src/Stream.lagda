\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module Stream where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

module I where
  data Ty : Set where
    Nat : Ty
    _⇒_ : Ty → Ty → Ty
    Stream : Ty

  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B

    zero : ∀ {Γ} → Tm Γ Nat
    suc : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat
    rec : ∀ {Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    recβ₁ : ∀ {Γ A} {u : Tm Γ A}{v : Tm (Γ ▹ A) A} →
      rec u v zero ≡ u
    recβ₂ : ∀ {Γ A} {u : Tm Γ A}{v : Tm (Γ ▹ A) A}{n : Tm Γ Nat} →
      rec u v (suc n) ≡ v [ id , rec u v n ]
    
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    zero[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[] : ∀ {Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    rec[] : ∀ {Γ Δ A} {u : Tm Δ A}{v : Tm (Δ ▹ A) A}{n : Tm Δ Nat}
      {σ : Sub Γ Δ} →
      (rec u v n) [ σ ] ≡ rec (u [ σ ]) (v [ σ ∘ p , q ]) (n [ σ ])

    head : ∀{Γ} → Tm Γ Stream → Tm Γ Nat
    tail : ∀{Γ} → Tm Γ Stream → Tm Γ Stream
    gen  : ∀{Γ A} → Tm (Γ ▹ A) Nat → Tm (Γ ▹ A) A → Tm Γ A → Tm Γ Stream
    head[] : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Δ Stream} → head t [ σ ] ≡ head (t [ σ ])
    tail[] : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Δ Stream} → tail t [ σ ] ≡ tail (t [ σ ])
    gen[]  : ∀ {Γ Δ} {σ : Sub Γ Δ}{A}{u : Tm (Δ ▹ A) Nat}{v : Tm (Δ ▹ A) A}{w : Tm Δ A} → gen u v w [ σ ] ≡ gen (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ]) (w [ σ ])
    Streamβ₁ : ∀ {Γ}{A}{u : Tm (Γ ▹ A) Nat}{v : Tm (Γ ▹ A) A}{w : Tm Γ A} → head (gen u v w) ≡ u [ id , w ]
    Streamβ₂ : ∀ {Γ}{A}{u : Tm (Γ ▹ A) Nat}{v : Tm (Γ ▹ A) A}{w : Tm Γ A} → tail (gen u v w) ≡ gen u v (v [ id , w ])
    

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η #-}
  {-# REWRITE recβ₁ recβ₂ #-}
  {-# REWRITE lam[] app[] #-}
  {-# REWRITE zero[] suc[] rec[] #-}
  {-# REWRITE head[] tail[] gen[] #-}
  {-# REWRITE Streamβ₁ Streamβ₂ #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : Set i
    Ty : Set j
    Sub : Con → Con → Set k
    Tm : Con → Ty → Set l

    ∙ : Con
    _▹_ : Con → Ty → Con

    Nat : Ty
    _⇒_ : Ty → Ty → Ty
    Stream : Ty

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    
    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B

    zero : ∀ {Γ} → Tm Γ Nat
    suc : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat
    rec : ∀ {Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    recβ₁ : ∀ {Γ A} {u : Tm Γ A}{v : Tm (Γ ▹ A) A} →
      rec u v zero ≡ u
    recβ₂ : ∀ {Γ A} {u : Tm Γ A}{v : Tm (Γ ▹ A) A}{n : Tm Γ Nat} →
      rec u v (suc n) ≡ v [ id , rec u v n ]
    
    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    zero[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[] : ∀ {Γ Δ} {n : Tm Δ Nat}{σ : Sub Γ Δ} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    rec[] : ∀ {Γ Δ A} {u : Tm Δ A}{v : Tm (Δ ▹ A) A}{n : Tm Δ Nat}
      {σ : Sub Γ Δ} →
      (rec u v n) [ σ ] ≡ rec (u [ σ ]) (v [ σ ∘ p , q ]) (n [ σ ])

    head : ∀{Γ} → Tm Γ Stream → Tm Γ Nat
    tail : ∀{Γ} → Tm Γ Stream → Tm Γ Stream
    gen  : ∀{Γ A} → Tm (Γ ▹ A) Nat → Tm (Γ ▹ A) A → Tm Γ A → Tm Γ Stream
    head[] : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Δ Stream} → head t [ σ ] ≡ head (t [ σ ])
    tail[] : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Δ Stream} → tail t [ σ ] ≡ tail (t [ σ ])
    gen[]  : ∀ {Γ Δ} {σ : Sub Γ Δ}{A}{u : Tm (Δ ▹ A) Nat}{v : Tm (Δ ▹ A) A}{w : Tm Δ A} → gen u v w [ σ ] ≡ gen (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ]) (w [ σ ])
    Streamβ₁ : ∀ {Γ}{A}{u : Tm (Γ ▹ A) Nat}{v : Tm (Γ ▹ A) A}{w : Tm Γ A} → head (gen u v w) ≡ u [ id , w ]
    Streamβ₂ : ∀ {Γ}{A}{u : Tm (Γ ▹ A) Nat}{v : Tm (Γ ▹ A) A}{w : Tm Γ A} → tail (gen u v w) ≡ gen u v (v [ id , w ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Stream ⟧T = Stream

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} → ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦rec⟧ : ∀ {Γ A} {u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{n : I.Tm Γ I.Nat} →
      ⟦ I.rec u v n ⟧t ≡ rec ⟦ u ⟧t ⟦ v ⟧t ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦rec⟧ #-}

    ⟦head⟧ : ∀{Γ}{t : I.Tm Γ I.Stream} → ⟦ I.head t ⟧t ≡ head ⟦ t ⟧t
    ⟦tail⟧ : ∀{Γ}{t : I.Tm Γ I.Stream} → ⟦ I.tail t ⟧t ≡ tail ⟦ t ⟧t
    ⟦gen⟧  : ∀{Γ A}{u : I.Tm (Γ I.▹ A) I.Nat}{v : I.Tm (Γ I.▹ A) A}{w : I.Tm Γ A} → ⟦ I.gen u v w ⟧t ≡ gen ⟦ u ⟧t ⟦ v ⟧t ⟦ w ⟧t
    {-# REWRITE ⟦head⟧ ⟦tail⟧ ⟦gen⟧ #-}


  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    Nat : Ty I.Nat
    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    Stream : Ty I.Stream

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')

    zero : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Nat I.zero
    suc : ∀ {Γ' n'} {Γ : Con Γ'} → Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    rec : ∀ {Γ' A' u' v' n'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ A u' → Tm (Γ ▹ A) A v' → Tm Γ Nat n' → Tm Γ A (I.rec u' v' n')

    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t

    recβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm (Γ ▹ A) A v'} → rec u v zero ≡ u
    recβ₂ : ∀ {Γ' A' u' v' n'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm (Γ ▹ A) A v'}{n : Tm Γ Nat n'} →
      rec u v (suc n) ≡ v [ id , rec u v n ]
    
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    zero[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      zero [ σ ] ≡ zero
    suc[] : ∀ {Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
      (suc n) [ σ ] ≡ suc (n [ σ ])
    rec[] : ∀ {Γ' Δ' A' u' v' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {u : Tm Δ A u'}{v : Tm (Δ ▹ A) A v'}{n : Tm Δ Nat n'}
      {σ : Sub Γ Δ σ'} →
      (rec u v n) [ σ ] ≡ rec (u [ σ ]) (v [ σ ∘ p , q ]) (n [ σ ])

    head : ∀{Γ' t'}{Γ : Con Γ'} → Tm Γ Stream t' → Tm Γ Nat (I.head t')
    tail : ∀{Γ' t'}{Γ : Con Γ'} → Tm Γ Stream t' → Tm Γ Stream (I.tail t')
    gen  : ∀{Γ' A' u' v' w'}{Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) Nat u' → Tm (Γ ▹ A) A v' → Tm Γ A w' → Tm Γ Stream (I.gen u' v' w')
    head[] : ∀ {Γ' Δ' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'}{t : Tm Δ Stream t'} → head t [ σ ] ≡ head (t [ σ ])
    tail[] : ∀ {Γ' Δ' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'}{t : Tm Δ Stream t'} → tail t [ σ ] ≡ tail (t [ σ ])
    gen[]  : ∀ {Γ' Δ' A' σ' u' v' w'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{σ : Sub Γ Δ σ'}{u : Tm (Δ ▹ A) Nat u'}{v : Tm (Δ ▹ A) A v'}{w : Tm Δ A w'} →
      gen u v w [ σ ] ≡ gen (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ]) (w [ σ ])
    Streamβ₁ : ∀ {Γ' A' u' v' w'}{Γ : Con Γ'}{A : Ty A'}{u : Tm (Γ ▹ A) Nat u'}{v : Tm (Γ ▹ A) A v'}{w : Tm Γ A w'} → head (gen u v w) ≡ u [ id , w ]
    Streamβ₂ : ∀ {Γ' A' u' v' w'}{Γ : Con Γ'}{A : Ty A'}{u : Tm (Γ ▹ A) Nat u'}{v : Tm (Γ ▹ A) A v'}{w : Tm Γ A w'} → tail (gen u v w) ≡ gen u v (v [ id , w ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------
  
  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Stream ⟧T = Stream

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} → ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦rec⟧ : ∀ {Γ A} {u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{n : I.Tm Γ I.Nat} →
      ⟦ I.rec u v n ⟧t ≡ rec ⟦ u ⟧t ⟦ v ⟧t ⟦ n ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦rec⟧ #-}

    ⟦head⟧ : ∀{Γ}{t : I.Tm Γ I.Stream} → ⟦ I.head t ⟧t ≡ head ⟦ t ⟧t
    ⟦tail⟧ : ∀{Γ}{t : I.Tm Γ I.Stream} → ⟦ I.tail t ⟧t ≡ tail ⟦ t ⟧t
    ⟦gen⟧  : ∀{Γ A}{u : I.Tm (Γ I.▹ A) I.Nat}{v : I.Tm (Γ I.▹ A) A}{w : I.Tm Γ A} → ⟦ I.gen u v w ⟧t ≡ gen ⟦ u ⟧t ⟦ v ⟧t ⟦ w ⟧t
    {-# REWRITE ⟦head⟧ ⟦tail⟧ ⟦gen⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record 𝕊 : Set where
  coinductive
  field
    hd : ℕ
    tl : 𝕊
open 𝕊

gen𝕊 : ∀{i}{A : Set i} → (A → ℕ) → (A → A) → A → 𝕊
hd (gen𝕊 f g a) = f a
tl (gen𝕊 f g a) = gen𝕊 f g (g a)

St : Algebra
St = record
       { Con = Set
       ; Ty = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×_
       ; Nat = ℕ
       ; _⇒_ = λ A B → A → B
       ; Stream = 𝕊
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ x → t (γ ,Σ x)
       ; app = λ t γx → t (π₁ γx) (π₂ γx)
       ; zero = λ _ → O
       ; suc = S ∘f_
       ; rec = λ u v n γ → indℕ _ (u γ) (λ x → v (γ ,Σ x)) (n γ)
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; recβ₁ = refl
       ; recβ₂ = refl
       ; lam[] = refl
       ; zero[] = refl
       ; suc[] = refl
       ; rec[] = refl
       ; head = hd ∘f_
       ; tail = tl ∘f_
       ; gen = λ u v w γ → gen𝕊 (λ α → u (γ ,Σ α)) (λ α → v (γ ,Σ α)) (w γ)
       ; head[] = refl
       ; tail[] = refl
       ; gen[] = refl
       ; Streamβ₁ = refl
       ; Streamβ₂ = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

open I
eval : ∀ {A} → Tm ∙ A → ⟦ A ⟧T
eval t = ⟦ t ⟧t *↑

⌜_⌝ : ℕ → Tm ∙ Nat
⌜ O ⌝ = zero
⌜ S n ⌝ = suc ⌜ n ⌝

open Algebra St using (⟦_⟧C ; ⟦_⟧T ; ⟦_⟧S ; ⟦_⟧t)
\end{code}
\begin{code}

record PStream (t' : Tm ∙ Stream) : Set where
  coinductive
  field
    Phead : ↑p (⌜ eval (head t') ⌝ ≡ head t')
    Ptail : PStream (tail t')
open PStream    

Pgen :
  {A' : Ty}{A : Tm ∙ A' → Set}
  {u' : Tm (∙ ▹ A') Nat}(u : {x' : Tm ∙ A'} → A x' → ↑p (⌜ eval (u' [ id , x' ]) ⌝ ≡ u' [ id , x' ]))
  {v' : Tm (∙ ▹ A') A'}(v : {x' : Tm ∙ A'} → A x' → A (v' [ id , x' ]))
  {w' : Tm ∙ A'}(w : A w')
  → PStream (gen u' v' w')
Phead (Pgen {A'} {A} {u'} u {v'} v {w'} w) = u w
Ptail (Pgen {A'} {A} {u'} u {v'} v {w'} w) = Pgen u v (v w)

Comp : DepAlgebra
Comp = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; Nat = λ n → ↑p (⌜ eval n ⌝ ≡ n)
         ; Stream = PStream
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; zero = λ _ → refl↑
         ; suc = λ n x → ↑[ ap suc ↓[ n x ]↓ ]↑
         ; rec = λ {_ _ u' v' n' _ A} u v n {ν'} x →
                   let P t = A (rec (u' [ ν' ]) (v' [ ν' ∘ p , q ]) t)
                   in  indℕ (λ n → ⌜ n ⌝ ≡ n' [ ν' ] → P (n' [ ν' ]))
                            (λ e → transport P e (u x))
                            (λ {n} h e → {!!})
                            (eval (n' [ ν' ]))
                            ↓[ n x ]↓
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; ⇒β = refl
         ; ⇒η = refl
         ; recβ₁ = {!!}
         ; recβ₂ = {!!}
         ; lam[] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; rec[] = {!!}
         ; head = λ t ν → Phead (t ν)
         ; tail = λ t ν → Ptail (t ν)
         ; gen = λ {Γ'}{A'}{u'}{v'}{w'}{Γ}{A} u v w ν → Pgen {A'}{A}(λ x → u (ν ,Σ x))(λ x → v (ν ,Σ x)) (w ν)
         ; head[] = refl
         ; tail[] = refl
         ; gen[] = refl
         ; Streamβ₁ = refl
         ; Streamβ₂ = refl
         }
module Comp = DepAlgebra Comp

compB : {t : Tm ∙ Stream} → PStream t
compB {t} = Comp.⟦ t ⟧t {id} *↑
\end{code}
