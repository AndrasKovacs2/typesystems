\begin{code}[hide]
{-# OPTIONS --prop #-}

module Nat.Addition where

open import Lib
open import Nat.Algebra

open I
\end{code}
\begin{code}
Plus : Algebra
Plus = record
      { Nat = Nat → Nat
      ; zero = idf
      ; suc = suc ∘_
      }
module Plus = Algebra Plus

_+_ : Nat → Nat → Nat
_+_ = Plus.⟦_⟧

Ass : DepAlgebra
Ass = record
      { Nat = λ x → (y z : Nat) → ↑p ((x + y) + z ≡ x + (y + z))
      ; zero = λ _ _ → refl↑
      ; suc = λ n y z → ↑[ ap suc ↓[ n y z ]↓ ]↑
      }
module Ass = DepAlgebra Ass

ass : ∀ x y z → (x + y) + z ≡ x + (y + z)
ass x y z = ↓[ Ass.⟦ x ⟧ y z ]↓

Idl : DepAlgebra
Idl = record
       { Nat = λ n → ↑p (zero + n ≡ n)
       ; zero = refl↑
       ; suc = const refl↑
       }
module Idl = DepAlgebra Idl

idl : ∀ {n} → zero + n ≡ n
idl {n} = ↓[ Idl.⟦ n ⟧ ]↓

Idr : DepAlgebra
Idr = record
       { Nat = λ n → ↑p (n + zero ≡ n)
       ; zero = refl↑
       ; suc = λ n → ↑[ ap suc ↓[ n ]↓ ]↑
       }
module Idr = DepAlgebra Idr

idr : ∀ {n} → n + zero ≡ n
idr {n} = ↓[ Idr.⟦ n ⟧ ]↓

CommLem : DepAlgebra
CommLem = record
          { Nat = λ x → (y : Nat) → ↑p (suc x + y ≡ x + suc y)
          ; zero = λ _ → refl↑
          ; suc = λ n y → ↑[ ap suc ↓[ n y ]↓ ]↑
          }
module CommLem = DepAlgebra CommLem

comm-lem : ∀ x y → suc x + y ≡ x + suc y
comm-lem x y = ↓[ CommLem.⟦ x ⟧ y ]↓

Comm : DepAlgebra
Comm = record
       { Nat = λ x → (y : Nat) → ↑p (x + y ≡ y + x)
       ; zero = λ _ → ↑[ idr ⁻¹ ]↑
       ; suc = λ {n'} n y → ↑[ ap suc ↓[ n y ]↓ ◾ comm-lem y n' ]↑
       }
module Comm = DepAlgebra Comm

comm : ∀ x y → x + y ≡ y + x
comm x y = ↓[ Comm.⟦ x ⟧ y ]↓
\end{code}
