\chapter{Natural numbers}

\begin{code}[hide]
{-# OPTIONS --prop #-}
module Nat.Algebra where

open import Lib
\end{code}
\begin{code}
module I where
  data Nat : Set where
    zero : Nat
    suc : Nat → Nat
    
record Algebra {i} : Set (lsuc i) where
  field
    Nat : Set i
    zero : Nat
    suc : Nat → Nat
  
  ⟦_⟧ : I.Nat → Nat
  ⟦ I.zero ⟧ = zero
  ⟦ I.suc n ⟧ = suc ⟦ n ⟧

record DepAlgebra {i} : Set (lsuc i) where
  field
    Nat : I.Nat → Set i
    zero : Nat I.zero
    suc : ∀ {n'} → Nat n' → Nat (I.suc n')
  
  ⟦_⟧ : (n : I.Nat) → Nat n
  ⟦ I.zero ⟧ = zero
  ⟦ I.suc n ⟧ = suc ⟦ n ⟧
\end{code}
