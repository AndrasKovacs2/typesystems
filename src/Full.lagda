\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Full where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)

module I where
  data Ty      : Set where
    Bool       : Ty
    Nat        : Ty
    _⇒_        : Ty → Ty → Ty
    Unit       : Ty
    _×_        : Ty → Ty → Ty
    Prod       : Ty → Ty → Ty
    Empty      : Ty
    _+_        : Ty → Ty → Ty
    List       : Ty → Ty
    Tree       : Ty
    Tree1      : Ty → Ty
    Tree2      : Ty → Ty → Ty

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_
  infixl 6 _+_
  
  postulate
    Sub        : Con → Con → Set
    Tm         : Con → Ty → Set

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁      : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂      : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]     : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]      : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                 (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
                 
    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])

    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt         : ∀{Γ} → Tm Γ Unit
    uη         : ∀{Γ}{t : Tm Γ Unit} → t ≡ tt
    tt[]       : ∀{Γ Δ}{σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    
    ⟨_,_⟩      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ B
    ×β₁        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η         : ∀{Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]      : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    pair       : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
    recProd    : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B) C → Tm Γ (Prod A B) → Tm Γ C
    Prodβ      : ∀{Γ A B C}{t : Tm (Γ ▹ A ▹ B) C}{u : Tm Γ A}{v : Tm Γ B} →
                 recProd t (pair u v) ≡ (t [ id , u , v ])
    pair[]     : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 pair u v [ σ ] ≡ pair (u [ σ ]) (v [ σ ])
    recProd[]  : ∀{Γ Δ A B C}{t : Tm (Δ ▹ A ▹ B) C}{u : Tm Δ (Prod A B)}{σ : Sub Γ Δ} →
                 recProd t u [ σ ] ≡ recProd (t [ (σ ∘ p , q) ∘ p , q ]) (u [ σ ])

    absurd     : ∀{Γ A} → Tm Γ Empty → Tm Γ A
    absurd[]   : ∀{Γ Δ A}{t : Tm Δ Empty}{σ : Sub Γ Δ} →
                 (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])

    inl        : ∀{Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr        : ∀{Γ A B} → Tm Γ B → Tm Γ (A + B)
    case       : ∀{Γ A B C} → Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
    +β₁        : ∀{Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} → case (inl t) u v ≡ u [ id , t ]
    +β₂        : ∀{Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} → case (inr t) u v ≡ v [ id , t ]
    inl[]      : ∀{Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} → (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[]      : ∀{Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} → (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[]     : ∀{Γ Δ A B C}{t : Tm Δ (A + B)}{u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
                 (case t u v) [ σ ] ≡ case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    recList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → recList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 recList u v (cons t₁ t) ≡ (v [ id , t₁ , recList u v t ])
    nil[]      : ∀{Γ Δ A}{σ : Sub Γ Δ} → nil {Δ}{A} [ σ ] ≡ nil {Γ}{A}
    cons[]     : ∀{Γ Δ A}{t₁ : Tm Δ A}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 (cons t₁ t) [ σ ] ≡ cons (t₁ [ σ ]) (t [ σ ])
    recList[]  : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ A ▹ B) B}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 recList u v t [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf       : ∀{Γ} → Tm Γ Tree
    node       : ∀{Γ} → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
    recTree    : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A ▹ A) A → Tm Γ Tree → Tm Γ A
    Treeβ₁     : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A ▹ A) A} → recTree u v leaf ≡ u
    Treeβ₂     : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A ▹ A) A}{t t₁ : Tm Γ Tree} →
                 recTree u v (node t t₁) ≡ v [ id , recTree u v t , recTree u v t₁ ]
    leaf[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → leaf [ σ ] ≡ leaf
    node[]     : ∀{Γ Δ}{t t₁ : Tm Δ Tree}{σ : Sub Γ Δ} → (node t t₁) [ σ ] ≡ node (t [ σ ]) (t₁ [ σ ])
    recTree[]  : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A ▹ A) A}{t : Tm Δ Tree}{σ : Sub Γ Δ} →
                 recTree u v t [ σ ] ≡ recTree (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf1      : ∀{Γ A} → Tm Γ (Tree1 A)
    node1      : ∀{Γ A} → Tm Γ (Tree1 A) → Tm Γ A → Tm Γ (Tree1 A) → Tm Γ (Tree1 A)
    recTree1   : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A ▹ B) B → Tm Γ (Tree1 A) → Tm Γ B
    Tree1β₁    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B} → recTree1 u v leaf1 ≡ u
    Tree1β₂    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B}{t₁ t₂ : Tm Γ (Tree1 A)}{w : Tm Γ A} →
                 recTree1 u v (node1 t₁ w t₂) ≡ v [ id , recTree1 u v t₁ , w , recTree1 u v t₂ ]
    leaf1[]    : ∀{Γ Δ A}{σ : Sub Γ Δ} → leaf1 {Δ}{A} [ σ ] ≡ leaf1
    node1[]    : ∀{Γ Δ A}{t₁ t₂ : Tm Δ (Tree1 A)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node1 t₁ w t₂) [ σ ] ≡ node1 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree1[] : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ B ▹ A ▹ B) B}{t : Tm Δ (Tree1 A)}{σ : Sub Γ Δ} →
                 recTree1 u v t [ σ ] ≡ recTree1 (u [ σ ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf2      : ∀{Γ A B} → Tm Γ B → Tm Γ (Tree2 A B)
    node2      : ∀{Γ A B} → Tm Γ (Tree2 A B) → Tm Γ A → Tm Γ (Tree2 A B) → Tm Γ (Tree2 A B)
    recTree2   : ∀{Γ A B C} → Tm (Γ ▹ B) C → Tm (Γ ▹ C ▹ A ▹ C) C → Tm Γ (Tree2 A B) → Tm Γ C
    Tree2β₁    : ∀{Γ A B C}{u : Tm (Γ ▹ B) C}{v : Tm (Γ ▹ C ▹ A ▹ C) C}{t : Tm Γ B} → recTree2 u v (leaf2 t) ≡ u [ id , t ]
    Tree2β₂    : ∀{Γ A B C}{u : Tm (Γ ▹ B) C}{v : Tm (Γ ▹ C ▹ A ▹ C) C}{t₁ t₂ : Tm Γ (Tree2 A B)}{w : Tm Γ A} →
                 recTree2 u v (node2 t₁ w t₂) ≡ v [ id , recTree2 u v t₁ , w , recTree2 u v t₂ ]
    leaf2[]    : ∀{Γ Δ B A}{t : Tm Δ B}{σ : Sub Γ Δ} → leaf2 {A = A} t [ σ ] ≡ leaf2 (t [ σ ])
    node2[]    : ∀{Γ Δ B A}{t₁ t₂ : Tm Δ (Tree2 A B)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node2 t₁ w t₂) [ σ ] ≡ node2 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree2[] : ∀{Γ Δ A B C}{u : Tm (Δ ▹ B) C}{v : Tm (Δ ▹ C ▹ A ▹ C) C}{t : Tm Δ (Tree2 A B)}{σ : Sub Γ Δ} →
                 recTree2 u v t [ σ ] ≡ recTree2 (u [ σ ∘ p , q ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE iteβ₁ iteβ₂ true[] false[] ite[] #-}
  {-# REWRITE Natβ₁ Natβ₂  zero[] suc[] recNat[] #-}
  {-# REWRITE ⇒β ⇒η lam[] app[] #-}
  {-# REWRITE tt[] #-}
  {-# REWRITE ×β₁ ×β₂ ×η ⟨,⟩[] #-}
  {-# REWRITE Prodβ pair[] recProd[] #-}
  {-# REWRITE absurd[] #-}
  {-# REWRITE +β₁ +β₂ inl[] inr[] case[] #-}
  {-# REWRITE Listβ₁ Listβ₂ nil[] cons[] recList[] #-}
  {-# REWRITE Treeβ₁ Treeβ₂ leaf[] node[] recTree[] #-}
  {-# REWRITE Tree1β₁ Tree1β₂ leaf1[] node1[] recTree1[] #-}
  {-# REWRITE Tree2β₁ Tree2β₂ leaf2[] node2[] recTree2[] #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_
  infixl 6 _+_
  
  field
    Con        : Set i
    Sub        : Con → Con → Set k
    Ty         : Set j
    Tm         : Con → Ty → Set l

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙          : Con
    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    _▹_        : Con → Ty → Con
    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    Bool       : Ty
    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁      : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂      : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]     : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]      : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                 (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
                 
    Nat        : Ty
    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])

    _⇒_        : Ty → Ty → Ty
    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    Unit       : Ty
    tt         : ∀{Γ} → Tm Γ Unit
    uη         : ∀{Γ}{t : Tm Γ Unit} → t ≡ tt
    tt[]       : ∀{Γ Δ}{σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    
    _×_        : Ty → Ty → Ty
    ⟨_,_⟩      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ B
    ×β₁        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η         : ∀{Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]      : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    Prod       : Ty → Ty → Ty
    pair       : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
    recProd    : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B) C → Tm Γ (Prod A B) → Tm Γ C
    Prodβ      : ∀{Γ A B C}{t : Tm (Γ ▹ A ▹ B) C}{u : Tm Γ A}{v : Tm Γ B} →
                 recProd t (pair u v) ≡ (t [ id , u , v ])
    pair[]     : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 pair u v [ σ ] ≡ pair (u [ σ ]) (v [ σ ])
    recProd[]  : ∀{Γ Δ A B C}{t : Tm (Δ ▹ A ▹ B) C}{u : Tm Δ (Prod A B)}{σ : Sub Γ Δ} →
                 recProd t u [ σ ] ≡ recProd (t [ (σ ∘ p , q) ∘ p , q ]) (u [ σ ])

    Empty      : Ty
    absurd     : ∀{Γ A} → Tm Γ Empty → Tm Γ A
    absurd[]   : ∀{Γ Δ A}{t : Tm Δ Empty}{σ : Sub Γ Δ} →
                 (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])

    _+_        : Ty → Ty → Ty
    inl        : ∀{Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr        : ∀{Γ A B} → Tm Γ B → Tm Γ (A + B)
    case       : ∀{Γ A B C} → Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
    +β₁        : ∀{Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} → case (inl t) u v ≡ u [ id , t ]
    +β₂        : ∀{Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} → case (inr t) u v ≡ v [ id , t ]
    inl[]      : ∀{Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} → (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[]      : ∀{Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} → (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[]     : ∀{Γ Δ A B C}{t : Tm Δ (A + B)}{u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
                 (case t u v) [ σ ] ≡ case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    List       : Ty → Ty
    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    recList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → recList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 recList u v (cons t₁ t) ≡ (v [ id , t₁ , recList u v t ])
    nil[]      : ∀{Γ Δ A}{σ : Sub Γ Δ} → nil {Δ}{A} [ σ ] ≡ nil {Γ}{A}
    cons[]     : ∀{Γ Δ A}{t₁ : Tm Δ A}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 (cons t₁ t) [ σ ] ≡ cons (t₁ [ σ ]) (t [ σ ])
    recList[]  : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ A ▹ B) B}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 recList u v t [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    Tree       : Ty
    leaf       : ∀{Γ} → Tm Γ Tree
    node       : ∀{Γ} → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
    recTree    : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A ▹ A) A → Tm Γ Tree → Tm Γ A
    Treeβ₁     : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A ▹ A) A} → recTree u v leaf ≡ u
    Treeβ₂     : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A ▹ A) A}{t t₁ : Tm Γ Tree} →
                 recTree u v (node t t₁) ≡ v [ id , recTree u v t , recTree u v t₁ ]
    leaf[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → leaf [ σ ] ≡ leaf
    node[]     : ∀{Γ Δ}{t t₁ : Tm Δ Tree}{σ : Sub Γ Δ} → (node t t₁) [ σ ] ≡ node (t [ σ ]) (t₁ [ σ ])
    recTree[]  : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A ▹ A) A}{t : Tm Δ Tree}{σ : Sub Γ Δ} →
                 recTree u v t [ σ ] ≡ recTree (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    Tree1      : Ty → Ty
    leaf1      : ∀{Γ A} → Tm Γ (Tree1 A)
    node1      : ∀{Γ A} → Tm Γ (Tree1 A) → Tm Γ A → Tm Γ (Tree1 A) → Tm Γ (Tree1 A)
    recTree1   : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A ▹ B) B → Tm Γ (Tree1 A) → Tm Γ B
    Tree1β₁    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B} → recTree1 u v leaf1 ≡ u
    Tree1β₂    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B}{t₁ t₂ : Tm Γ (Tree1 A)}{w : Tm Γ A} →
                 recTree1 u v (node1 t₁ w t₂) ≡ v [ id , recTree1 u v t₁ , w , recTree1 u v t₂ ]
    leaf1[]    : ∀{Γ Δ A}{σ : Sub Γ Δ} → leaf1 {Δ}{A} [ σ ] ≡ leaf1
    node1[]    : ∀{Γ Δ A}{t₁ t₂ : Tm Δ (Tree1 A)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node1 t₁ w t₂) [ σ ] ≡ node1 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree1[] : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ B ▹ A ▹ B) B}{t : Tm Δ (Tree1 A)}{σ : Sub Γ Δ} →
                 recTree1 u v t [ σ ] ≡ recTree1 (u [ σ ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

    Tree2      : Ty → Ty → Ty
    leaf2      : ∀{Γ A B} → Tm Γ B → Tm Γ (Tree2 A B)
    node2      : ∀{Γ A B} → Tm Γ (Tree2 A B) → Tm Γ A → Tm Γ (Tree2 A B) → Tm Γ (Tree2 A B)
    recTree2   : ∀{Γ A B C} → Tm (Γ ▹ B) C → Tm (Γ ▹ C ▹ A ▹ C) C → Tm Γ (Tree2 A B) → Tm Γ C
    Tree2β₁    : ∀{Γ A B C}{u : Tm (Γ ▹ B) C}{v : Tm (Γ ▹ C ▹ A ▹ C) C}{t : Tm Γ B} → recTree2 u v (leaf2 t) ≡ u [ id , t ]
    Tree2β₂    : ∀{Γ A B C}{u : Tm (Γ ▹ B) C}{v : Tm (Γ ▹ C ▹ A ▹ C) C}{t₁ t₂ : Tm Γ (Tree2 A B)}{w : Tm Γ A} →
                 recTree2 u v (node2 t₁ w t₂) ≡ v [ id , recTree2 u v t₁ , w , recTree2 u v t₂ ]
    leaf2[]    : ∀{Γ Δ B A}{t : Tm Δ B}{σ : Sub Γ Δ} → leaf2 {A = A} t [ σ ] ≡ leaf2 (t [ σ ])
    node2[]    : ∀{Γ Δ B A}{t₁ t₂ : Tm Δ (Tree2 A B)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node2 t₁ w t₂) [ σ ] ≡ node2 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree2[] : ∀{Γ Δ A B C}{u : Tm (Δ ▹ B) C}{v : Tm (Δ ▹ C ▹ A ▹ C) C}{t : Tm Δ (Tree2 A B)}{σ : Sub Γ Δ} →
                 recTree2 u v t [ σ ] ≡ recTree2 (u [ σ ∘ p , q ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree ⟧T = Tree
  ⟦ I.Tree1 A ⟧T = Tree1 ⟦ A ⟧T
  ⟦ I.Tree2 A B ⟧T = Tree2 ⟦ A ⟧T ⟦ B ⟧T
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Unit ⟧T = Unit
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ I.Prod A B ⟧T = Prod ⟦ A ⟧T ⟦ B ⟧T
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦recNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.recNat u v t ⟧t ≡ recNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦recNat⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}
  
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    {-# REWRITE ⟦tt⟧ #-}

    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦pair⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.pair u v ⟧t ≡ pair ⟦ u ⟧t ⟦ v ⟧t
    ⟦recProd⟧ : ∀{Γ A B C}{u : I.Tm (Γ I.▹ A I.▹ B) C}{t : I.Tm Γ (I.Prod A B)} →
      ⟦ I.recProd u t ⟧t ≡ recProd ⟦ u ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦pair⟧ ⟦recProd⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    {-# REWRITE ⟦absurd⟧ #-}
    
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}

    ⟦nil⟧ : ∀ {Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≡ nil
    ⟦cons⟧ : ∀ {Γ A} {t₁ : I.Tm Γ A}{t : I.Tm Γ (I.List A)} →
      ⟦ I.cons t₁ t ⟧t ≡ cons ⟦ t₁ ⟧t ⟦ t ⟧t
    ⟦recList⟧ : ∀{Γ A B}{u : I.Tm Γ B}{v : I.Tm (Γ I.▹ A I.▹ B) B}{t : I.Tm Γ (I.List A)} →
      ⟦ I.recList u v t ⟧t ≡ recList ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦recList⟧ #-}

    ⟦leaf⟧ : ∀ {Γ} → ⟦ I.leaf {Γ} ⟧t ≡ leaf
    ⟦node⟧ : ∀ {Γ} {t t' : I.Tm Γ I.Tree} →
      ⟦ I.node t t' ⟧t ≡ node ⟦ t ⟧t ⟦ t' ⟧t
    ⟦recTree⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A I.▹ A) A}{t : I.Tm Γ I.Tree} →
      ⟦ I.recTree u v t ⟧t ≡ recTree ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦recTree⟧ #-}

    ⟦leaf1⟧ : ∀ {Γ A} → ⟦ I.leaf1 {Γ}{A} ⟧t ≡ leaf1
    ⟦node1⟧ : ∀ {Γ A} {t t' : I.Tm Γ (I.Tree1 A)}{t'' : I.Tm Γ A} →
      ⟦ I.node1 t t'' t' ⟧t ≡ node1 ⟦ t ⟧t ⟦ t'' ⟧t ⟦ t' ⟧t
    ⟦recTree1⟧ : ∀{Γ A B}{u : I.Tm Γ B}{v : I.Tm (Γ I.▹ B I.▹ A I.▹ B) B}{t : I.Tm Γ (I.Tree1 A)} →
      ⟦ I.recTree1 u v t ⟧t ≡ recTree1 ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf1⟧ ⟦node1⟧ ⟦recTree1⟧ #-}

    ⟦leaf2⟧ : ∀ {Γ A B}{t : I.Tm Γ B} → ⟦ I.leaf2 {Γ}{A}{B} t ⟧t ≡ leaf2 ⟦ t ⟧t
    ⟦node2⟧ : ∀ {Γ A B} {t t' : I.Tm Γ (I.Tree2 A B)}{t'' : I.Tm Γ A} →
      ⟦ I.node2 t t'' t' ⟧t ≡ node2 ⟦ t ⟧t ⟦ t'' ⟧t ⟦ t' ⟧t
    ⟦recTree2⟧ : ∀{Γ A B C}{u : I.Tm (Γ I.▹ B) C}{v : I.Tm (Γ I.▹ C I.▹ A I.▹ C) C}{t : I.Tm Γ (I.Tree2 A B)} →
      ⟦ I.recTree2 u v t ⟧t ≡ recTree2 ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf2⟧ ⟦node2⟧ ⟦recTree2⟧ #-}

  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_
  infixl 6 _+_
  
  field
    Con : I.Con → Set i
    Ty  : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm  : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ

    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙ : Con I.∙
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p
    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ

    Bool : Ty I.Bool
    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
    ite : ∀ {Γ' A' b' u' v'}  {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Bool b' → Tm Γ A u' → Tm Γ A v' → Tm Γ A (I.ite b' u' v')
    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' u' v' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ Bool b'}{u : Tm Δ A u'}{v : Tm Δ A v'}
      {σ : Sub Γ Δ σ'} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    iteβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite false u v ≡ v

    Nat        : Ty I.Nat
    zero       : ∀{Γ'}{Γ : Con Γ'} → Tm Γ Nat I.zero
    suc        : ∀{Γ' n'}{Γ : Con Γ'} →
                 Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    recNat     : ∀{Γ' A' u' v' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A u' → Tm (Γ ▹ A) A v' → Tm Γ Nat t' → Tm Γ A (I.recNat u' v' t')
    Natβ₁      : ∀{Γ' A' u' v'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm (Γ ▹ A) A v'} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ' A' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm (Γ ▹ A) A v'}{t : Tm Γ Nat t'} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
                 zero [ σ ] ≡ zero
    suc[]      : ∀{Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
                 (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ' Δ' A' u' v' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{u : Tm Δ A u'}{v : Tm (Δ ▹ A) A v'}{t : Tm Δ Nat t'}{σ : Sub Γ Δ σ'} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])

    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')
    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    Unit : Ty I.Unit
    tt : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Unit I.tt
    uη : ∀ {Γ' t'} {Γ : Con Γ'} {t : Tm Γ Unit t'} →
      t =[ ap (Tm Γ Unit) I.uη ]= tt
    tt[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      tt [ σ ] ≡ tt
    
    _×_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.× B')
    ⟨_,_⟩ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A u' → Tm Γ B v' → Tm Γ (A × B) I.⟨ u' , v' ⟩
    proj₁ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ A (I.proj₁ t')
    proj₂ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ B (I.proj₂ t')
    ×β₁ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A × B) t'} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[] : ∀ {Γ' Δ' A' B' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    Prod       : ∀ {A' B'} → Ty A' → Ty B' → Ty (I.Prod A' B')
    pair       : ∀{Γ' A' B' u' v'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ A u' → Tm Γ B v' → Tm Γ (Prod A B) (I.pair u' v')
    recProd    : ∀{Γ' A' B' C' u' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'} → Tm (Γ ▹ A ▹ B) C u' → Tm Γ (Prod A B) t' → Tm Γ C (I.recProd u' t')
    Prodβ      : ∀{Γ' A' B' C' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{t : Tm (Γ ▹ A ▹ B) C t'}{u : Tm Γ A u'}{v : Tm Γ B v'} →
                 recProd t (pair u v) ≡ (t [ id , u , v ])
    pair[]     : ∀{Γ' Δ' A' B' u' v' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
                 pair u v [ σ ] ≡ pair (u [ σ ]) (v [ σ ])
    recProd[]  : ∀{Γ' Δ' A' B' C' u' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{t : Tm (Δ ▹ A ▹ B) C t'}{u : Tm Δ (Prod A B) u'}{σ : Sub Γ Δ σ'} →
                 recProd t u [ σ ] ≡ recProd (t [ (σ ∘ p , q) ∘ p , q ]) (u [ σ ])

    Empty : Ty I.Empty
    absurd : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Empty t' → Tm Γ A (I.absurd t')
    absurd[] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {t : Tm Δ Empty t'}{σ : Sub Γ Δ σ'} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])

    _+_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.+ B')
    inl : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A t' → Tm Γ (A + B) (I.inl t')
    inr : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ B t' → Tm Γ (A + B) (I.inr t')
    case : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'} →
      Tm Γ (A + B) t' → Tm (Γ ▹ A) C u' → Tm (Γ ▹ B) C v' →
      Tm Γ C (I.case t' u' v')
    +β₁ : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Γ A t'}{u : Tm (Γ ▹ A) C u'}{v : Tm (Γ ▹ B) C v'} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Γ B t'}{u : Tm (Γ ▹ A) C u'}{v : Tm (Γ ▹ B) C v'} →
      case (inr t) u v ≡ v [ id , t ]
    inl[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Δ A t'}{σ : Sub Γ Δ σ'} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Δ B t'}{σ : Sub Γ Δ σ'} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ' Δ' A' B' C' t' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Δ (A + B) t'}{u : Tm (Δ ▹ A) C u'}{v : Tm (Δ ▹ B) C v'}
      {σ : Sub Γ Δ σ'} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    List       : ∀{A'} → Ty A' → Ty (I.List A')
    nil        : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (List A) I.nil
    cons       : ∀{Γ' A' t' u'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A u' → Tm Γ (List A) t' → Tm Γ (List A) (I.cons u' t')
    recList    : ∀{Γ' A' B' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ B u' → Tm (Γ ▹ A ▹ B) B v'  → Tm Γ (List A) t' → Tm Γ B (I.recList u' v' t')
    Listβ₁     : ∀{Γ' A' B' u' v'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{u : Tm Γ B u'}{v : Tm (Γ ▹ A ▹ B) B v'} → recList u v nil ≡ u
    Listβ₂     : ∀{Γ' A' B' u' v' t' t₁'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{u : Tm Γ B u'}{v : Tm (Γ ▹ A ▹ B) B v'}{t : Tm Γ (List A) t'}{t₁ : Tm Γ A t₁'} →
                 recList u v (cons t₁ t) ≡ (v [ id , t₁ , recList u v t ])
    nil[]      : ∀{Γ' Δ' A' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{σ : Sub Γ Δ σ'} → nil {Γ = Δ}{A = A} [ σ ] ≡ nil
    cons[]     : ∀{Γ' Δ' A' t' t₁' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{t₁ : Tm Δ A t₁'}{t : Tm Δ (List A) t'}{σ : Sub Γ Δ σ'} →
                 (cons t₁ t) [ σ ] ≡ cons (t₁ [ σ ]) (t [ σ ])
    recList[]  : ∀{Γ' Δ' A' B' u' v' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{u : Tm Δ B u'}{v : Tm (Δ ▹ A ▹ B) B v'}{t : Tm Δ (List A) t'}{σ : Sub Γ Δ σ'} →
                 recList u v t [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    Tree       : Ty I.Tree
    leaf       : ∀{Γ'}{Γ : Con Γ'} → Tm Γ Tree I.leaf
    node       : ∀{Γ' u' v'}{Γ : Con Γ'} → Tm Γ Tree u' → Tm Γ Tree v' → Tm Γ Tree (I.node u' v')
    recTree    : ∀{Γ' A' u' v' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A u' → Tm (Γ ▹ A ▹ A) A v' → Tm Γ Tree t' → Tm Γ A (I.recTree u' v' t')
    Treeβ₁     : ∀{Γ' A' u' v'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm (Γ ▹ A ▹ A) A v'} → recTree u v leaf ≡ u
    Treeβ₂     : ∀{Γ' A' u' v' t' t₁'}{Γ : Con Γ'}{A : Ty A'}{u : Tm Γ A u'}{v : Tm (Γ ▹ A ▹ A) A v'}{t : Tm Γ Tree t'}{t₁ : Tm Γ Tree t₁'} →
                 recTree u v (node t t₁) ≡ v [ id , recTree u v t , recTree u v t₁ ]
    leaf[]     : ∀{Γ' Δ' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} → leaf [ σ ] ≡ leaf
    node[]     : ∀{Γ' Δ' t' t₁' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{t : Tm Δ Tree t'}{t₁ : Tm Δ Tree t₁'}{σ : Sub Γ Δ σ'} →
                 (node t t₁) [ σ ] ≡ node (t [ σ ]) (t₁ [ σ ])
    recTree[]  : ∀{Γ' Δ' A' u' v' t' σ'}{Γ : Con Γ'}{A : Ty A'}{Δ : Con Δ'}{u : Tm Δ A u'}{v : Tm (Δ ▹ A ▹ A) A v'}{t : Tm Δ Tree t'}{σ : Sub Γ Δ σ'} →
                 recTree u v t [ σ ] ≡ recTree (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    Tree1       : ∀{A'} → Ty A' → Ty (I.Tree1 A')
    leaf1       : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (Tree1 A) I.leaf1
    node1       : ∀{Γ' A' u' v' w'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (Tree1 A) u' → Tm Γ A w' → Tm Γ (Tree1 A) v' → Tm Γ (Tree1 A) (I.node1 u' w' v')
    recTree1    : ∀{Γ' A' B' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ B u' → Tm (Γ ▹ B ▹ A ▹ B) B v' → Tm Γ (Tree1 A) t' → Tm Γ B (I.recTree1 u' v' t')
    Tree1β₁     : ∀{Γ' A' B' u' v'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{u : Tm Γ B u'}{v : Tm (Γ ▹ B ▹ A ▹ B) B v'} → recTree1 u v leaf1 ≡ u
    Tree1β₂     : ∀{Γ' A' B' u' v' t₁' t₂' w'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{u : Tm Γ B u'}{v : Tm (Γ ▹ B ▹ A ▹ B) B v'}{t₁ : Tm Γ (Tree1 A) t₁'}{t₂ : Tm Γ (Tree1 A) t₂'}{w : Tm Γ A w'} →
                 recTree1 u v (node1 t₁ w t₂) ≡ v [ id , recTree1 u v t₁ , w , recTree1 u v t₂ ]
    leaf1[]     : ∀{Γ' Δ' A' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{σ : Sub Γ Δ σ'} → leaf1 {A = A} [ σ ] ≡ leaf1
    node1[]     : ∀{Γ' Δ' A' t₁' t₂' w' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{t₁ : Tm Δ (Tree1 A) t₁'}{t₂ : Tm Δ (Tree1 A) t₂'}{w : Tm Δ A w'}{σ : Sub Γ Δ σ'} → 
                 (node1 t₁ w t₂) [ σ ] ≡ node1 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree1[]  : ∀{Γ' Δ' A' B' u' v' t' σ'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{Δ : Con Δ'}{u : Tm Δ B u'}{v : Tm (Δ ▹ B ▹ A ▹ B) B v'}{t : Tm Δ (Tree1 A) t'}{σ : Sub Γ Δ σ'} →
                 recTree1 u v t [ σ ] ≡ recTree1 (u [ σ ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

    Tree2       : ∀{A' B'} → Ty A' → Ty B' → Ty (I.Tree2 A' B')
    leaf2       : ∀{Γ' A' B' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ B t' → Tm Γ (Tree2 A B) (I.leaf2 t')
    node2       : ∀{Γ' A' B' u' v' w'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ (Tree2 A B) u' → Tm Γ A w' → Tm Γ (Tree2 A B) v' → Tm Γ (Tree2 A B) (I.node2 u' w' v')
    recTree2    : ∀{Γ' A' B' C' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'} → Tm (Γ ▹ B) C u' → Tm (Γ ▹ C ▹ A ▹ C) C v' → Tm Γ (Tree2 A B) t' → Tm Γ C (I.recTree2 u' v' t')
    Tree2β₁     : ∀{Γ' A' B' C' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{u : Tm (Γ ▹ B) C u'}{v : Tm (Γ ▹ C ▹ A ▹ C) C v'}{t : Tm Γ B t'} → recTree2 u v (leaf2 t) ≡ u [ id , t ]
    Tree2β₂     : ∀{Γ' A' B' C' u' v' t₁' t₂' w'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{u : Tm (Γ ▹ B) C u'}{v : Tm (Γ ▹ C ▹ A ▹ C) C v'}{t₁ : Tm Γ (Tree2 A B) t₁'}{t₂ : Tm Γ (Tree2 A B) t₂'}{w : Tm Γ A w'} →
                 recTree2 u v (node2 t₁ w t₂) ≡ v [ id , recTree2 u v t₁ , w , recTree2 u v t₂ ]
    leaf2[]     : ∀{Γ' Δ' A' B' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{t : Tm Δ B t'}{σ : Sub Γ Δ σ'} → leaf2 {A = A} t [ σ ] ≡ leaf2 (t [ σ ])
    node2[]     : ∀{Γ' Δ' A' B' t₁' t₂' w' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{t₁ : Tm Δ (Tree2 A B) t₁'}{t₂ : Tm Δ (Tree2 A B) t₂'}{w : Tm Δ A w'}{σ : Sub Γ Δ σ'} → 
                 (node2 t₁ w t₂) [ σ ] ≡ node2 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree2[]  : ∀{Γ' Δ' A' B' C' u' v' t' σ'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{Δ : Con Δ'}{u : Tm (Δ ▹ B) C u'}{v : Tm (Δ ▹ C ▹ A ▹ C) C v'}{t : Tm Δ (Tree2 A B) t'}{σ : Sub Γ Δ σ'} →
                 recTree2 u v t [ σ ] ≡ recTree2 (u [ σ ∘ p , q ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree ⟧T = Tree
  ⟦ I.Tree1 A ⟧T = Tree1 ⟦ A ⟧T
  ⟦ I.Tree2 A B ⟧T = Tree2 ⟦ A ⟧T ⟦ B ⟧T
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Unit ⟧T = Unit
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ I.Prod A B ⟧T = Prod ⟦ A ⟧T ⟦ B ⟧T
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦recNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.recNat u v t ⟧t ≡ recNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦recNat⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    {-# REWRITE ⟦tt⟧ #-}

    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦pair⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.pair u v ⟧t ≡ pair ⟦ u ⟧t ⟦ v ⟧t
    ⟦recProd⟧ : ∀{Γ A B C}{u : I.Tm (Γ I.▹ A I.▹ B) C}{t : I.Tm Γ (I.Prod A B)} →
      ⟦ I.recProd u t ⟧t ≡ recProd ⟦ u ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦pair⟧ ⟦recProd⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    {-# REWRITE ⟦absurd⟧ #-}
    
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}

    ⟦nil⟧ : ∀ {Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≡ nil
    ⟦cons⟧ : ∀ {Γ A}{t₁ : I.Tm Γ A}{t : I.Tm Γ (I.List A)} →
      ⟦ I.cons t₁ t ⟧t ≡ cons ⟦ t₁ ⟧t ⟦ t ⟧t
    ⟦recList⟧ : ∀{Γ A B}{u : I.Tm Γ B}{v : I.Tm (Γ I.▹ A I.▹ B) B}{t : I.Tm Γ (I.List A)} →
      ⟦ I.recList u v t ⟧t ≡ recList ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦recList⟧ #-}

    ⟦leaf⟧ : ∀ {Γ} → ⟦ I.leaf {Γ} ⟧t ≡ leaf
    ⟦node⟧ : ∀ {Γ} {t t' : I.Tm Γ I.Tree} →
      ⟦ I.node t t' ⟧t ≡ node ⟦ t ⟧t ⟦ t' ⟧t
    ⟦recTree⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A I.▹ A) A}{t : I.Tm Γ I.Tree} →
      ⟦ I.recTree u v t ⟧t ≡ recTree ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦recTree⟧ #-}

    ⟦leaf1⟧ : ∀ {Γ A} → ⟦ I.leaf1 {Γ}{A} ⟧t ≡ leaf1
    ⟦node1⟧ : ∀ {Γ A} {t t' : I.Tm Γ (I.Tree1 A)}{t'' : I.Tm Γ A} →
      ⟦ I.node1 t t'' t' ⟧t ≡ node1 ⟦ t ⟧t ⟦ t'' ⟧t ⟦ t' ⟧t
    ⟦recTree1⟧ : ∀{Γ A B}{u : I.Tm Γ B}{v : I.Tm (Γ I.▹ B I.▹ A I.▹ B) B}{t : I.Tm Γ (I.Tree1 A)} →
      ⟦ I.recTree1 u v t ⟧t ≡ recTree1 ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf1⟧ ⟦node1⟧ ⟦recTree1⟧ #-}

    ⟦leaf2⟧ : ∀ {Γ A B}{t : I.Tm Γ B} → ⟦ I.leaf2 {Γ}{A}{B} t ⟧t ≡ leaf2 ⟦ t ⟧t
    ⟦node2⟧ : ∀ {Γ A B} {t t' : I.Tm Γ (I.Tree2 A B)}{t'' : I.Tm Γ A} →
      ⟦ I.node2 t t'' t' ⟧t ≡ node2 ⟦ t ⟧t ⟦ t'' ⟧t ⟦ t' ⟧t
    ⟦recTree2⟧ : ∀{Γ A B C}{u : I.Tm (Γ I.▹ B) C}{v : I.Tm (Γ I.▹ C I.▹ A I.▹ C) C}{t : I.Tm Γ (I.Tree2 A B)} →
      ⟦ I.recTree2 u v t ⟧t ≡ recTree2 ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf2⟧ ⟦node2⟧ ⟦recTree2⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

canBool   : (t : I.Tm I.∙ I.Bool) → ↑p (t ≡ I.true) ⊎ ↑p (t ≡ I.false)
canNat    : (t : I.Tm I.∙ I.Nat) → ↑p (t ≡ I.zero) ⊎ Σ (I.Tm I.∙ I.Nat) λ u → ↑p (t ≡ I.suc u)
can⇒      : ∀{A B}(t : I.Tm I.∙ (A I.⇒ B)) → Σ (I.Tm (I.∙ I.▹ A) B) λ u → ↑p (t ≡ I.lam u)
canUnit   : (t : I.Tm I.∙ I.Unit) → t ≡ I.tt
can×      : ∀{A B}(t : I.Tm I.∙ (A I.× B)) → Σ (I.Tm I.∙ A) λ u → Σ (I.Tm I.∙ B) λ v → ↑p (t ≡ I.⟨ u , v ⟩)
canProd   : ∀{A B}(t : I.Tm I.∙ (I.Prod A B)) → Σ (I.Tm I.∙ A) λ u → Σ (I.Tm I.∙ B) λ v → ↑p (t ≡ I.pair u v)
canEmpty  : ∀{A B}(t : I.Tm I.∙ (A I.⇒ B)) → 𝟘
can+      : ∀{A B}(t : I.Tm I.∙ (A I.+ B)) → (Σ (I.Tm I.∙ A) λ u → ↑p (t ≡ I.inl u)) ⊎ (Σ (I.Tm I.∙ B) λ v → ↑p (t ≡ I.inr v))
canList   : ∀{A}(t : I.Tm I.∙ (I.List A)) → ↑p (t ≡ I.nil) ⊎ Σ (I.Tm I.∙ A) λ u → Σ (I.Tm I.∙ (I.List A)) λ v → ↑p (t ≡ I.cons u v)
canTree   : (t : I.Tm I.∙ I.Tree) → ↑p (t ≡ I.leaf) ⊎ Σ (I.Tm I.∙ I.Tree) λ u → Σ (I.Tm I.∙ I.Tree) λ v → ↑p (t ≡ I.node u v)
canTree1  : ∀{A}(t : I.Tm I.∙ (I.Tree1 A)) → ↑p (t ≡ I.leaf1) ⊎ Σ (I.Tm I.∙ (I.Tree1 A)) λ t₁ → Σ (I.Tm I.∙ A) λ w → Σ (I.Tm I.∙ (I.Tree1 A)) λ t₂ → ↑p (t ≡ I.node1 t₁ w t₂)
canTree2  : ∀{A B}(t : I.Tm I.∙ (I.Tree2 A B)) → (Σ (I.Tm I.∙ B) λ u → ↑p (t ≡ I.leaf2 u)) ⊎ Σ (I.Tm I.∙ (I.Tree2 A B)) λ t₁ → Σ (I.Tm I.∙ A) λ w → Σ (I.Tm I.∙ (I.Tree2 A B )) λ t₂ → ↑p (t ≡ I.node2 t₁ w t₂)
\end{code}
