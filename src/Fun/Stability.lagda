\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module Fun.Stability where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import Fun.Algebra
open import Fun.Standard

open I
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

---------------------------------------------------------
-- stability for Nat and Bool
---------------------------------------------------------

stabilityN : ∀ {n} → eval ⌜ n ⌝N ≡ n
stabilityN {O} = refl
stabilityN {S n} = ap S stabilityN

stabilityB : ∀ {b} → eval ⌜ b ⌝B ≡ b
stabilityB {O} = refl
stabilityB {I} = refl
\end{code}
