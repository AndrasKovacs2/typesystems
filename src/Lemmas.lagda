\begin{code}
{-# OPTIONS --prop --rewriting #-}

module Lemmas where

open import Lib

-- S properties

S⁻¹-≡ : {x y : ℕ} → S x ≡ S y → x ≡ y
S⁻¹-≡ refl = refl

-- +ℕ properties

+ℕ-idl : {a : ℕ} → 0 +ℕ a ≡ a
+ℕ-idl {a} = refl

+ℕ-idr : {a : ℕ} → a +ℕ 0 ≡ a
+ℕ-idr {O} = refl
+ℕ-idr {S a} = congruency S +ℕ-idr

+ℕ-S-r : {a b : ℕ} → a +ℕ S b ≡ S (a +ℕ b)
+ℕ-S-r {O} = refl
+ℕ-S-r {S a} = congruency S +ℕ-S-r

+ℕ-comm : {a b : ℕ} → a +ℕ b ≡ b +ℕ a
+ℕ-comm {O} {b} = +ℕ-idr ⁻¹
+ℕ-comm {S a} {b} = congruency S (+ℕ-comm {a}) ◾ (+ℕ-S-r ⁻¹)

+ℕ-assoc : {a b c : ℕ} → a +ℕ (b +ℕ c) ≡ (a +ℕ b) +ℕ c
+ℕ-assoc {O} = refl
+ℕ-assoc {S a} = congruency S (+ℕ-assoc {a})

-- *ℕ properties

*ℕ-0-l : {a : ℕ} → 0 *ℕ a ≡ 0
*ℕ-0-l = refl

*ℕ-0-r : {a : ℕ} → a *ℕ 0 ≡ 0
*ℕ-0-r {O} = refl
*ℕ-0-r {S a} = *ℕ-0-r {a}

*ℕ-idl : {a : ℕ} → 1 *ℕ a ≡ a
*ℕ-idl = +ℕ-idr

*ℕ-idr : {a : ℕ} → a *ℕ 1 ≡ a
*ℕ-idr {O} = refl
*ℕ-idr {S a} = congruency S *ℕ-idr

*ℕ-S-r : {a b : ℕ} → a *ℕ S b ≡ a +ℕ a *ℕ b
*ℕ-S-r {O} = refl
*ℕ-S-r {S a} {b} = congruency S
  (
      congruency (b +ℕ_) (*ℕ-S-r {a})
    ◾
      +ℕ-assoc {b} {a}
    ◾
      congruency (_+ℕ a *ℕ b) (+ℕ-comm {b})
    ◾
      (+ℕ-assoc {a} {b}) ⁻¹
  )

*ℕ-comm : {a b : ℕ} → a *ℕ b ≡ b *ℕ a
*ℕ-comm {O} {b} = (*ℕ-0-r {b}) ⁻¹
*ℕ-comm {S a} {b} =
  (
      congruency (b +ℕ_) (*ℕ-comm {a})
    ◾
      (*ℕ-S-r {b}) ⁻¹
  )

-- ≤ properties

≤-S : {a b : ℕ} → a ≤ b → S a ≤ S b
≤-S z≤n = s≤s z≤n
≤-S (s≤s a≤b) = s≤s (≤-S a≤b)

≤-S-r : {a b : ℕ} → a ≤ b → a ≤ S b
≤-S-r z≤n = z≤n
≤-S-r (s≤s le) = s≤s (≤-S-r le)

≤-refl : {a : ℕ} → a ≤ a
≤-refl {O} = z≤n
≤-refl {S a} = s≤s ≤-refl

≤-≡ : {a b : ℕ} → a ≡ b → a ≤ b
≤-≡ refl = ≤-refl

infixr 4 _≤-◾_
_≤-◾_ : {a b c : ℕ} → a ≤ b → b ≤ c → a ≤ c
z≤n ≤-◾ b≤c = z≤n
(s≤s a≤b) ≤-◾ (s≤s b≤c) = s≤s (a≤b ≤-◾ b≤c)

-- +ℕ and ≤ combined properties

≤-+ℕ-l : {a b c : ℕ} → a ≤ b → a ≤ c +ℕ b
≤-+ℕ-l {c = O} a≤b = a≤b
≤-+ℕ-l {c = S c} a≤b = ≤-S-r (≤-+ℕ-l a≤b)

≤-+ℕ-r : {a b c : ℕ} → a ≤ b → a ≤ b +ℕ c
≤-+ℕ-r {c = c} a≤b = (≤-+ℕ-l a≤b) ≤-◾ (≤-≡ (+ℕ-comm {a = c}))

-- Should these be compat or mono?
+ℕ-≤-mono-l : {a a' b : ℕ} →
  a ≤ a' → a +ℕ b ≤ a' +ℕ b
+ℕ-≤-mono-l z≤n = ≤-+ℕ-l ≤-refl
+ℕ-≤-mono-l (s≤s a≤a') = s≤s (+ℕ-≤-mono-l a≤a')

+ℕ-≤-mono-r : {a b b' : ℕ} →
  b ≤ b' → a +ℕ b ≤ a +ℕ b'
+ℕ-≤-mono-r {a = a} b≤b' =
    (≤-≡ (+ℕ-comm {a = a}))
  ≤-◾
    (+ℕ-≤-mono-l b≤b')
  ≤-◾
    (≤-≡ (+ℕ-comm {b = a}))

+ℕ-≤-mono : {a a' b b' : ℕ} →
  a ≤ a' → b ≤ b' → a +ℕ b ≤ a' +ℕ b'
+ℕ-≤-mono z≤n b≤b' = ≤-+ℕ-l b≤b'
+ℕ-≤-mono (s≤s a≤a') b≤b' = s≤s (+ℕ-≤-mono a≤a' b≤b')

-- max properties

≤-max-l : {a b : ℕ} → a ≤ (max a b)
≤-max-l {O} = z≤n
≤-max-l {S a} {O} = ≤-refl
≤-max-l {S a} {S b} = s≤s ≤-max-l

≤-max-r : {a b : ℕ} → b ≤ (max a b)
≤-max-r {O} = ≤-refl
≤-max-r {S a} {O} = z≤n
≤-max-r {S a} {S b} = s≤s ≤-max-r

≤-max-3a : {a b c : ℕ} → a ≤ max a (max b c)
≤-max-3a {a = a} = ≤-max-l {a = a}

≤-max-3b : {a b c : ℕ} → b ≤ max a (max b c)
≤-max-3b {a = a} = ≤-max-l ≤-◾ (≤-max-r {a = a})

≤-max-3c : {a b c : ℕ} → c ≤ max a (max b c)
≤-max-3c {a = a} = ≤-max-r ≤-◾ (≤-max-r {a = a})

-- *ℕ and ≤ combined properties

*ℕ-≤-l : {a b c : ℕ} → 1 ≤ c → a ≤ b → a ≤ c *ℕ b
*ℕ-≤-l {c = S c} 1≤c a≤b = ≤-+ℕ-r a≤b

*ℕ-≤-r : {a b c : ℕ} → 1 ≤ c → a ≤ b → a ≤ b *ℕ c
*ℕ-≤-r {b = b} 1≤c a≤b = (*ℕ-≤-l 1≤c a≤b) ≤-◾ (≤-≡ (*ℕ-comm {b = b}))

*ℕ-≤-mono-l : {a b c : ℕ} → a ≤ b → c *ℕ a ≤ c *ℕ b
*ℕ-≤-mono-l {c = O} a≤b = z≤n
*ℕ-≤-mono-l {c = S c} a≤b = +ℕ-≤-mono a≤b (*ℕ-≤-mono-l {c = c} a≤b)

-- ^ℕ properties

^ℕ-≤-mono-r : {a b b' : ℕ} → 1 ≤ a → b ≤ b' → a ^ℕ b ≤ a ^ℕ b'
^ℕ-≤-mono-r {b = O} {b' = O} 1≤a le2 = s≤s z≤n
^ℕ-≤-mono-r {a = a} {b = O} {b' = S b'} 1≤a z≤n = *ℕ-≤-l 1≤a (^ℕ-≤-mono-r {b' = b'} 1≤a z≤n)
^ℕ-≤-mono-r {a = a} {b = S b} {b' = S b'} 1≤a (s≤s b≤b') = *ℕ-≤-mono-l {c = a} (^ℕ-≤-mono-r 1≤a b≤b')

\end{code}
