Egy NatBoolAST-algebra, NatBoolAST függő algebra (dependens algebra), rekurzió, indukció

NatBoolAST-algebra:
Tm : Set
true : Tm
false : Tm
ite : Tm → Tm → Tm → Tm
zero : Tm
suc : Tm → Tm
isZero : Tm → Tm
_+_ : Tm → Tm → Tm

Szintaxis (szintaxisfák): iniciális NatBoolAST-algebra (I-vel jelöljük).

I.Tm : Set
I.true : I.Tm
I.false : I.Tm
I.ite : I.Tm → I.Tm → I.Tm → I.Tm
...

Rekurzor: tetszőleges M algebrára megad egy morfizmust I-ből M-be:
 
M.⟦_⟧ : I.Tm → M.Tm
M.⟦ I.true ⟧ = M.true
M.⟦ I.false ⟧ = M.false
M.⟦ I.ite t t' t'' ⟧ = M.ite M.⟦ t ⟧ M.⟦ t' ⟧ M.⟦ t'' ⟧
...

-----------------------------------------------------------------------

Kitérő:

ℕ : Set
O : ℕ
S : ℕ → ℕ

f : ℕ → A
f O = a
f (S n) = s (f n)

ℕ-algebra: (A : Set, a : A, s : A → A)
(ℕ,O,S) ℕ algebrát alkotnak

tetszőleges (A,a,s) algebrára kapunk egy függvényt ℕ-ból A-ba:
⟦_⟧ : ℕ → A
⟦ O ⟧ = a
⟦ S n ⟧ = s ⟦ n ⟧

semantic brackets ⟦_⟧, szintaxisból megy a szemantikába

rekurzió: n ↦ 2*n+3
O       ↦ 3       S(S(SO))
S O     ↦ 5     SSSSSO ...
S (S O) ↦ 7   SSSSSSSO
3       ↦ 9 SSSSSSSSSO 
...

M := (ℕ, S (S (S O)), λ x → S (S x))
M.⟦_⟧ : ℕ → ℕ ez a n ↦ 2*n+3 függvény

algebra, rekurzió (rekurzor)

függő algebra, indukció

N : I.N → Set                   N I.O : Set,   N (I.S I.O) : Set, N (I.S (I.S I.O)) : Set
O : N I.O                       N 0 , N 1, N 2, N 3, ... : Set
S : (n : I.N) → N n → N (I.S n)    \__/^     \___/^ ...

indukció: tetszőleges D függő algebrára:

D.⟦_⟧ : (n : I.N) → D.N n
D.⟦ I.O ⟧ = D.O
D.⟦ I.S n ⟧ = D.S n D.⟦ n ⟧

Természetes számokra megbeszéltük az alábbi fogalmakat: algebra,
iniciális algebra (szintaxis), rekurzió, függő algebra, indukció

zero + n = n                       n
suc m + n = suc (m + n)            suc

-------------------------------------------------------------------------

Programozási nyelvekre: algebra, függő algebra, rekurzió, indukció

H : Algebra
H.Tm = ℕ
H.true = 0
H.false = 0
H.ite t u v = 1 +ℕ max (t,u,v)
...

height t := H.⟦ t ⟧ : ℕ

szintaktikus term = szabad algebra (= szabad modell)

rekurzor (⟦_⟧, eliminátor, fold, iterátor, kiértékelés)

indukció (dependens/függő eliminátor)

T : Algebra, megadta a term-ben a true operátorok száma

H, T algebrák

Egy termben a true-k maximális száma 3^(term magassága).

           ite
          / | \
         /  |  \
     true true  true

    3 ≤ 3 = 3^1

           ite
          / | \
         /  |  \
       ite ite  ite
       /|\ /|\  /|\       

    9 ≤ 9 = 3^2
    
Agda-ban: (t : I.Tm) → T.⟦ t ⟧ ≤ 3 ^ (H.⟦ t ⟧)

Ez egy tétel/állítás szintaktikus termekről. Ezt dependens algebrával
és indukcióval tudjuk bizonyítani. Indukció termeken: strukturális
indukció.

D : DepAlgebra
D.Tm : I.Tm → Set
D.true : D.Tm I.true
D.false : D.Tm I.false
D.ite   : D.Tm t → D.Tm u → D.Tm v → D.Tm (I.ite t u v)
D.zero  : D.Tm I.zero
D.suc   : D.Tm t → D.Tm (I.suc t)
D.isZero : D.Tm t → D.Tm (I.isZero t)
D._+_    : D.Tm u → D.Tm v → D.Tm (u I.+ v)

indukció: tetszőleges D DepAlgerbrára

D.⟦_⟧ : (t : I.Tm) → D.Tm t

                          D.Tm I.true, D.Tm I.false, D.Tm (I.ite I.true I.false I.zero), ...


D.N : I.N → Set               D.N 0, D.N 1, D.N 2, ...

D : DepAlgebra
D.Tm t := T.⟦ t ⟧ ≤ 3 ^ (H.⟦ t ⟧)
D.true : 1 ≤ 1 = 3 ^ 0
D.false : 0 ≤ 1 = 3 ^ 0
D.ite : D.Tm t → D.Tm u → D.Tm v → D.Tm (I.ite t u v)
tudjuk:  (t): T.⟦ t ⟧ ≤ 3 ^ (H.⟦ t ⟧)
         (u): T.⟦ u ⟧ ≤ 3 ^ (H.⟦ u ⟧)
         (v): T.⟦ v ⟧ ≤ 3 ^ (H.⟦ v ⟧)
kell:    T.⟦ t ⟧ + T.⟦ u ⟧ + T.⟦ v ⟧ ≤
        3 ^ (H.⟦ t ⟧) + 3 ^ (H.⟦ u ⟧) + 3 ^ (H.⟦ v ⟧) ≤

        3 ^ x         + 3 ^ x         + 3 ^ x =
                                                   max {H.⟦ t ⟧, H.⟦ u ⟧, H.⟦ v ⟧} =: x
        3 * 3 ^ x =
        3 ^ (1 + x) =
        3 ^ (1 + max {H.⟦ t ⟧, H.⟦ u ⟧, H.⟦ v ⟧})
D.zero : 
D.suc
D.isZero
D._+_ 

a ≤ a' & b ≤ b', akkor a + b ≤ a' + b'

...

D.⟦_⟧ : (t : I.Tm) → T.⟦ t ⟧ ≤ 3 ^ (H.⟦ t ⟧)

Ezt hívják strukturális indukciónak.

Eddig a NatBoolAST nyelvvel foglalkoztunk.

Szintaxisról: tetszőleges algebrába vezet egy függvény a szintaxisból (iniális algebrából).
Szintaxis elemei végesek.
Szintaxisban a különböző operátorok ténylegesen különbözőek.

I.true ≠ I.false

(I.true ≡ I.false) → 𝟘

(𝟙 ≡ 𝟘) → 𝟘

TF : Algebra
TF.Tm := Set
TF.true := 𝟙
TF.false := 𝟘
TF... mindegy

TF.⟦_⟧ alkalmazom a I.true ≡ I.false egyenloseg ket oldalara:

(TF.⟦ I.true ⟧ ≡ TF.⟦ I.false ⟧) =

(TF.true ≡ TF.false) =

(𝟙 ≡ 𝟘) → 𝟘

* : 𝟙
transport (e : 𝟙 ≡ 𝟘) * : 𝟘

---------------------------------------------------------------------------------

NatBool nyelv (Hutton's Razor)

sztring                              <- túl sok, túl sok különböző
lexikális elemek sorozata            <- (true) ≠ true,    (true értelmetlen
AST                                  <- 3 + 0 ≠ 3,        3 + true, suc true értelmetlen

---------------------------------------------------------------------------------

well-typed syntax jól típusozott leírás

NatBoolWT-algebra:
Ty : Set
Tm : Ty → Set
Bool : Ty
Nat  : Ty           Tm Bool : Set, Tm Nat : Set
zero : Tm Nat
suc  : Tm Nat → Tm Nat
isZero : Tm Nat → Tm Bool
_+_    : Tm Nat → Tm Nat → Tm Nat                          "suc true" le se tudom írni
true  : Tm Bool                                            "(true" le se tudom írni
false : Tm Bool
ite   : Tm Bool → Tm A → Tm A → Tm A                       "ite false true zero" nem tudom leirni

sztring                  |
                         v lexikális elemezés
lex.elemek sorozata      |
                         v parzolás (szintaktikus elemzés)
AST                      |
                         v típuskikövetkeztetés / típusellenőrzés
WT                       |
                         v semmi
WT egyenlőségekkel

1. típuskikövetkeztetés

2. standard interpretáció
